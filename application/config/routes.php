<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
 */
$route['default_controller'] = 'AdminController/index';

//AdminController Routes
$route['checkuser'] = 'login/checkuser';
$route['updateCoreUser'] = 'login/update_core_user';
$route['register'] = 'login/register';
$route['login'] = 'login/index';
$route['index'] = 'AdminController/index';
$route['logout'] = 'AdminController/logout';

$route['addCoreUser'] = 'core_users/addUser';
$route['editCoreuser/(:num)'] = 'core_users/update_user/$1';
$route['deleteCoreuser/(:num)'] = 'core_users/deleteuser/$1';
$route['viewCoreUsers'] = 'core_users/viewUsers';

$route['addUser'] = 'AdminController/addUser';
$route['addUserData'] = 'AdminController/addUserData';
$route['updateUserData'] = 'AdminController/updateUserData';
$route['edituser/(:num)'] = 'AdminController/edituser/$1';
$route['deleteuser/(:num)'] = 'AdminController/deleteuser/$1';
$route['viewUsers'] = 'AdminController/viewUsers';

$route['editemployee/(:num)'] = 'AdminController/editemployee/$1';
$route['deleteemployee/(:num)'] = 'AdminController/deleteemployee/$1';
$route['updateemployeeData'] = 'AdminController/updateemployeeData';

$route['addeditcategory'] = 'AdminController/addeditcategory';
$route['addnewcategory'] = 'AdminController/addnewcategory';
$route['editcategory/(:num)'] = 'AdminController/editcategory/$1';
$route['updatecategory'] = 'AdminController/updatecategory';
$route['deletecategory/(:num)'] = 'AdminController/deletecategory/$1';

$route['addproduct'] = 'AdminController/addproduct';
$route['addProductData'] = 'AdminController/addProductData';
$route['viewproducts'] = 'AdminController/viewproducts';
$route['editprod/(:num)'] = 'AdminController/editprod/$1';
$route['viewspecificprod/(:num)'] = 'AdminController/viewspecificprod/$1';
$route['deleteprod/(:num)'] = 'AdminController/deleteprod/$1';
$route['editProductData'] = 'AdminController/editProductData';

$route['addclient'] = 'AdminController/addclient';
$route['addClientData'] = 'AdminController/addClientData';
$route['viewclients'] = 'AdminController/viewclients';
$route['viewclient/(:num)'] = 'AdminController/viewclient/$1';
$route['editclient/(:num)'] = 'AdminController/editclient/$1';
$route['deleteclient/(:num)'] = 'AdminController/deleteclient/$1';
$route['updateClientData'] = 'AdminController/updateClientData';

//EmployeeController Routes
$route['empList'] = 'EmployeeController/empList';
$route['addEmployee'] = 'EmployeeController/addEmployee';
$route['addData'] = 'EmployeeController/addData';
$route['fullprofile/(:num)'] = 'EmployeeController/fullprofile/$1';

//CompanyController Routes
$route['addCompany'] = 'MyCompanyController/addCompany';
$route['addCompData'] = 'MyCompanyController/addcompanyinformation';
$route['viewCompany'] = 'MyCompanyController/viewCompany';
$route['editcomp/(:num)'] = 'MyCompanyController/editcomp/$1';
$route['updateCompData'] = 'MyCompanyController/updateCompData';

//Admin Uploads
$route['importcsvcontent'] = 'AdminUploads/uploadClientExcel';
$route['importcsvclient'] = 'AdminUploads/importcsvclient';
$route['importempdata'] = 'AdminUploads/uploadEmployeeExcel';
$route['csvemp'] = 'AdminUploads/csvemp';
$route['importproddata'] = 'AdminUploads/uploadProductExcel';
$route['csvprod'] = 'AdminUploads/csvprod';

//Barcode Routes
$route['barcodeForm'] = 'AdminController/loadBarcodePage';
$route['createBarcodeImage'] = 'barcode_file/Barcode/createbarcode';
$route['deleteLabel/(:num)'] = 'AdminController/deleteLabel/$1';

//Barcode Single
$route['createBarcodeImage2'] = 'barcode_file/Barcode2/createbarcode';
$route['barcodeSingleForm'] = 'AdminController/loadBarcodeSingleForm';
$route['deleteBarcode/(:num)'] = 'AdminController/deleteBarcode/$1';

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
