<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminController extends CI_Controller {
	static private $login = "";
	function __construct() {
		parent::__construct();
		// code change by @abdulmanan7
		if (!is_login()) {
			redirect('login');
		}
	}

	//load login page
	public function index() {
		$this->load->view('core/dashboard');

	}
	//logout from application
	public function logout() {
		$this->session->sess_destroy();
		//This code prevents the user from pressing the back button after logging out
		//of the application.
		redirect('login', 'refresh');
	}

	//load add users
	public function adduser() {
		$this->load->view('core/adduser');
	}

	//load all users
	public function viewUsers() {
		$this->load->view('core/viewusers');
	}

	//add user
	public function addUserData() {
		$config['upload_path'] = './upload/e_kdjhawkdhawjkhd';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size'] = '15000';
		$config['max_width'] = '3000';
		$config['max_height'] = '3000';

		$this->upload->initialize($config);

		if (!$this->upload->do_upload()) //if there was an error with file upload
		{
			$error = array('error' => $this->upload->display_errors());
			$profilephoto1 = "blank.png";
		} else //no error with file upload
		{
			/*
			[file_name]    => mypic.jpg
			[file_type]    => image/jpeg
			[file_path]    => /path/to/your/upload/
			[full_path]    => /path/to/your/upload/jpg.jpg
			[raw_name]     => mypic
			[orig_name]    => mypic.jpg
			[client_name]  => mypic.jpg
			[file_ext]     => .jpg
			[file_size]    => 22.2
			[is_image]     => 1
			[image_width]  => 800
			[image_height] => 600
			[image_type]   => jpeg
			[image_size_str] => width="800" height="200"
			 */

			$data = $this->upload->data();
			$profilephoto1 = $data['file_name'];
		}

		$this->load->model('AdminModel');
		$this->AdminModel->store_info($profilephoto1);

		$msg['message'] = "New user added successfully!";
		$this->load->view('core/adduser', $msg);
	}

	//edit user
	public function edituser() {
		$id = $this->uri->segment(2);
		$data['id'] = $id;
		$data['security_code'] = false;
		$this->load->view('core/edituser', $data);
	}

	//delete user
	public function deleteuser() {
		$id = $this->uri->segment(2);
		$fieldname = 'userid';
		$tablename = 'appuser';

		$this->load->model('AdminModel');
		$this->AdminModel->deletedata($id, $fieldname, $tablename);

		$msg['message'] = "User has been deleted successfully!";
		$this->load->view('core/viewusers', $msg);
	}

	//edit employee
	public function editemployee() {
		$id = $this->uri->segment(2);
		$data['id'] = $id;
		$this->load->view('core/editemployee', $data);
	}

	//delete employee
	public function deleteemployee() {
		$id = $this->uri->segment(2);
		$fieldname = 'eid';
		$tablename = 'edata';

		$this->load->model('AdminModel');
		$this->AdminModel->deletedata($id, $fieldname, $tablename);

		$msg['message'] = "Employee has been deleted successfully!";
		$this->load->view('core/employeelistsimple', $msg);
	}

	//edit user data
	public function updateUserData() {
		$config['upload_path'] = './upload/e_kdjhawkdhawjkhd';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size'] = '15000';
		$config['max_width'] = '3000';
		$config['max_height'] = '3000';

		$this->upload->initialize($config);

		if (!$this->upload->do_upload()) //if there was an error with file upload
		{
			$error = array('error' => $this->upload->display_errors());
			$profilephoto1 = "none";
		} else //no error with file upload
		{
			/*
			[file_name]    => mypic.jpg
			[file_type]    => image/jpeg
			[file_path]    => /path/to/your/upload/
			[full_path]    => /path/to/your/upload/jpg.jpg
			[raw_name]     => mypic
			[orig_name]    => mypic.jpg
			[client_name]  => mypic.jpg
			[file_ext]     => .jpg
			[file_size]    => 22.2
			[is_image]     => 1
			[image_width]  => 800
			[image_height] => 600
			[image_type]   => jpeg
			[image_size_str] => width="800" height="200"
			 */

			$data = $this->upload->data();
			$profilephoto1 = $data['file_name'];
		}

		$this->load->model('AdminModel');
		$this->AdminModel->edit_user_info($profilephoto1);

		$msg['message'] = "User edited successfully!";
		$this->load->view('core/viewusers', $msg);
	}

	//edit employee data
	public function updateemployeeData() {
		$config['upload_path'] = './upload/e_kdjhawkdhawjkhd';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size'] = '15000';
		$config['max_width'] = '3000';
		$config['max_height'] = '3000';

		$this->upload->initialize($config);

		if (!$this->upload->do_upload()) //if there was an error with file upload
		{
			$error = array('error' => $this->upload->display_errors());
			$profilephoto1 = "none";
		} else //no error with file upload
		{
			/*
			[file_name]    => mypic.jpg
			[file_type]    => image/jpeg
			[file_path]    => /path/to/your/upload/
			[full_path]    => /path/to/your/upload/jpg.jpg
			[raw_name]     => mypic
			[orig_name]    => mypic.jpg
			[client_name]  => mypic.jpg
			[file_ext]     => .jpg
			[file_size]    => 22.2
			[is_image]     => 1
			[image_width]  => 800
			[image_height] => 600
			[image_type]   => jpeg
			[image_size_str] => width="800" height="200"
			 */

			$data = $this->upload->data();
			$profilephoto1 = $data['file_name'];
		}

		$this->load->model('AdminModel');
		$this->AdminModel->edit_employee_info($profilephoto1);

		$msg['message'] = "Employee edited successfully!";
		$this->load->view('core/employeelistsimple', $msg);
	}

/*
Product functions
 */

	//Load view product page
	public function viewproducts() {
		$this->load->view('core/viewproducts');
	}

	//Load add product page
	public function addproduct() {
		$this->load->view('core/addproduct');
	}

	//add product to db
	public function addProductData() {
		$config['upload_path'] = './upload/p_dfgdfgdfgjkhd';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size'] = '15000';
		$config['max_width'] = '3000';
		$config['max_height'] = '3000';

		$this->upload->initialize($config);

		$flag = 0;
		$i = 0;
		$files = array();
		$is_file_error = FALSE;

		$this->load->model('AdminModel');

		//if the user did not select a photo
		if ($_FILES['userfile1']['size'] <= 0 && $_FILES['userfile2']['size'] <= 0 && $_FILES['userfile3']['size'] <= 0) {
			$flag = 2;
			$this->AdminModel->add_product_info($flag, $files);
		} else //the user uploaded a photo
		{
			foreach ($_FILES as $key => $value) {
				if (!empty($value['name'])) {
					$this->load->library('upload', $config);
					if (!$this->upload->do_upload($key)) {
						//error
						$flag = 3;
						$is_file_error = TRUE;
					} else {
						$files[$i] = $this->upload->data(); //stores all photo data in an array location
						++$i;
					}
				}
			}

			if (!$is_file_error && $files) {
				$flag = 1;
				$this->AdminModel->add_product_info($flag, $files);
			} else {
				$flag = 3;
			}
		}

		if ($flag != 3) {
			$msg['message'] = "Success! Product added.";
			$this->load->view('core/addproduct', $msg);
		} else if ($flag == 3 || $flag == 0) {
			$msg['message'] = "There was an error adding your data. Please contact support.";
			$this->load->view('core/addproduct', $msg);
		}
	}

	//load edit product
	public function editprod() {
		$id = $this->uri->segment(2);
		$data['id'] = $id;
		$this->load->view('core/editproduct', $data);
	}

	//edit product then save to db
	public function editProductData() {
		$config['upload_path'] = './upload/p_dfgdfgdfgjkhd';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size'] = '15000';
		$config['max_width'] = '3000';
		$config['max_height'] = '3000';

		$this->upload->initialize($config);

		$flag = 0;
		$i = 0;
		$files = array();
		$is_file_error = FALSE;

		$this->load->model('AdminModel');

		//if the user did not select a photo
		if ($_FILES['userfile1']['size'] <= 0 && $_FILES['userfile2']['size'] <= 0 && $_FILES['userfile3']['size'] <= 0) {
			$flag = 2;
			$this->AdminModel->edit_product_info($flag, $files);
		} else //the user uploaded a photo
		{
			foreach ($_FILES as $key => $value) {
				if (!empty($value['name'])) {
					$this->load->library('upload', $config);
					if (!$this->upload->do_upload($key)) {
						//error
						$flag = 3;
						$is_file_error = TRUE;
					} else {
						$files[$i] = $this->upload->data(); //stores all photo data in an array location
						++$i;
					}
				}
			}

			if (!$is_file_error && $files) {
				$flag = 1;
				$this->AdminModel->edit_product_info($flag, $files);
			} else {
				$flag = 3;
			}
		}

		if ($flag != 3) {
			$msg['message'] = "Success! Product updated.";
			$this->load->view('core/addproduct', $msg);
		} else if ($flag == 3 || $flag == 0) {
			$msg['message'] = "There was an error editing your data. Please contact support.";
			$this->load->view('core/addproduct', $msg);
		}
	}

	//load view specific product
	public function viewspecificprod() {
		$id = $this->uri->segment(2);
		$data['id'] = $id;
		$this->load->view('core/viewspecificprod', $data);
	}

	//delete category
	public function deleteprod() {
		$id = $this->uri->segment(2);
		$fieldname = 'prodId';
		$tablename = 'pdata';

		$this->load->model('AdminModel');
		$this->AdminModel->deletedata($id, $fieldname, $tablename);

		$msg['message'] = "Product has been deleted successfully!";
		$this->load->view('core/viewproducts', $msg);
	}

	//load add product categories
	public function addeditcategory() {
		$this->load->model('AdminModel');
		$str['data'] = $this->AdminModel->load_inputbox_category();

		//var_dump($str);
		$this->load->view('core/addcategory', $str);
	}

	//add product categories
	public function addnewcategory() {
		$this->load->model('AdminModel');
		$str = $this->AdminModel->add_category();

		$msg['message'] = $str;
		$this->load->view('core/addcategory', $msg);
	}

	//load edit category
	public function editcategory() {
		$id = $this->uri->segment(2);
		$data['id'] = $id;
		$this->load->view('core/editcategory', $data);
	}

	//update product categories
	public function updatecategory() {
		$this->load->model('AdminModel');
		$str = $this->AdminModel->update_category();

		$msg['message'] = $str;
		$this->load->view('core/addcategory', $msg);
	}

	//delete category
	public function deletecategory() {
		$id = $this->uri->segment(2);
		$fieldname = 'catid';
		$tablename = 'pcategories';

		$this->load->model('AdminModel');
		$this->AdminModel->deletedata($id, $fieldname, $tablename);

		$msg['message'] = "Category has been deleted successfully!";
		$this->load->view('core/addcategory', $msg);
	}

/*
Client functions
 */

	//Load add product page
	public function viewclients() {
		$this->load->view('core/viewclients');
	}

	//Load add product page
	public function addclient() {
		$this->load->view('core/addclient');
	}

	//add new client
	public function addClientData() {
		$config['upload_path'] = './upload/cl_skeuhdflefjslfjseflksjef';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size'] = '15000';
		$config['max_width'] = '3000';
		$config['max_height'] = '3000';

		$this->upload->initialize($config);

		if (!$this->upload->do_upload()) //if there was an error with file upload
		{
			$error = array('error' => $this->upload->display_errors());
			$clientphoto1 = "blank.png";
		} else //no error with file upload
		{
			$data = $this->upload->data();
			$clientphoto1 = $data['file_name'];
		}

		$this->load->model('AdminModel');
		$this->AdminModel->store_client_info($clientphoto1);

		$msg['message'] = "New client added successfully!";
		$this->load->view('core/viewclients', $msg);
	}

	//load view specific client
	public function viewclient() {
		$id = $this->uri->segment(2);
		$data['id'] = $id;
		$this->load->view('core/viewspecificclient', $data);
	}

	//load edit specific client
	public function editclient() {
		$id = $this->uri->segment(2);
		$data['id'] = $id;
		$this->load->view('core/editclient', $data);
	}

	//delete client
	public function deleteclient() {
		$id = $this->uri->segment(2);
		$fieldname = 'clId';
		$tablename = 'cldata';

		$this->load->model('AdminModel');
		$this->AdminModel->deletedata($id, $fieldname, $tablename);

		$msg['message'] = "Client has been deleted successfully!";
		$this->load->view('core/viewclients', $msg);
	}

	//update client data
	public function updateClientData() {
		$config['upload_path'] = './upload/cl_skeuhdflefjslfjseflksjef';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size'] = '15000';
		$config['max_width'] = '30000';
		$config['max_height'] = '30000';

		$this->upload->initialize($config);

		if (!$this->upload->do_upload()) //if there was an error with file upload
		{
			$error = array('error' => $this->upload->display_errors());
			$clientphoto1 = "none";
		} else //no error with file upload
		{
			$data = $this->upload->data();
			$clientphoto1 = $data['file_name'];
		}

		$this->load->model('AdminModel');
		$this->AdminModel->update_client_info($clientphoto1);

		$msg['message'] = "New client edited successfully!";
		$this->load->view('core/viewclients', $msg);
	}

	//Load create barcode page
	public function loadBarcodePage() {
		$this->load->view('core/createBarcode');
	}

	//delete label
	public function deleteLabel() {
		$id = $this->uri->segment(2);
		$fieldname = 'barcodeId';
		$tablename = 'barcode';

		$this->load->model('AdminModel');
		$this->AdminModel->deletedata($id, $fieldname, $tablename);

		$msg['message'] = "Label has been deleted successfully!";
		$this->load->view('core/createBarcode', $msg);
	}

//Load create barcode single page
	public function loadBarcodeSingleForm() {
		$this->load->view('core/createBarcodeSingle');
	}

//delete barcode
	public function deleteBarcode() {
		$id = $this->uri->segment(2);
		$fieldname = 'barcodeId';
		$tablename = 'barcode2';

		$this->load->model('AdminModel');
		$this->AdminModel->deletedata($id, $fieldname, $tablename);

		$msg['message'] = "Barcode has been deleted successfully!";
		$this->load->view('core/createBarcodeSingle', $msg);
	}

	//set up captcha and return image
	private function getimage() {
		$vals = array(
			'img_path' => './captcha/',
			'img_url' => base_url() . 'captcha/',
			'font_path' => './path/to/fonts/texb.ttf',
			'img_width' => '150',
			'img_height' => 30,
			'expiration' => 200,
			'word_length' => 4,
			'font_size' => 16,
			'img_id' => 'Imageid',
			'pool' => '123456789abcdefghijklmnpqrstuvwxyzABCDEFGHIJKLMNPQRSTUVWXYZ',

			// White background and border, black text and red grid
			'colors' => array(
				'background' => array(255, 255, 255),
				'border' => array(255, 255, 255),
				'text' => array(0, 0, 0),
				'grid' => array(0, 0, 0),
			),
		);

		$cap = create_captcha($vals);

		$data2 = array(
			'captcha_time' => $cap['time'],
			'ip_address' => $this->input->ip_address(),
			'word' => $cap['word'],
		);

		$query = $this->db->insert_string('captcha', $data2);
		$this->db->query($query);

		$data['cap_img'] = $cap['image'];

		return $data;
	}
}
