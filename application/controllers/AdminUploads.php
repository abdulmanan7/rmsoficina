<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminUploads extends CI_Controller {

	function __construct() {
		parent::__construct();
		// code change by @abdulmanan7
		if (!is_login()) {
			redirect('login');
		}
	}

	//load import employees
	public function csvprod() {
		$this->load->view('core/importcsvproduct');
	}
	//load import employees
	public function csvemp() {
		$this->load->view('core/importcsvemployee');
	}

	//load import clients
	public function importcsvclient() {
		$this->load->view('core/importcsvclient');
	}

//Upload Product Excel Data
	public function uploadProductExcel() {

		$config['upload_path'] = './upload/excel_files/'; //upload folder path
		$config['allowed_types'] = 'xlsx|csv'; //allowed extensions
		$this->upload->initialize($config); //initialize the config items

		//if upload error
		if (!$this->upload->do_upload('userfile')) {
			$msg['message'] = "There was an error uploading your file.<br>The file should be of type: xlsx or csv.<br>Contact support if you are still having problems.";
			$this->load->view('core/importcsvproduct', $msg);
		} else //successfully uploaded the file
		{
			$upload_data = $this->upload->data(); //upload data
			$file_name = $upload_data['file_name']; //save file name

			//load the phpExcel library
			$this->load->library("Excel");

			$objReader = PHPExcel_IOFactory::createReader('Excel2007'); //excel 2007 reader

			//$objReader->setReadDataOnly(true); //set to read only

			//load excel file
			$objPHPExcel = $objReader->load('upload/excel_files/' . $file_name);
			$sheetnumber = 0;

			//as long as we are on sheet 0 then loop
			foreach ($objPHPExcel->getWorksheetIterator() as $sheet) {
				$s = $sheet->getTitle(); //get the sheet name
				$sheet = str_replace(' ', '', $s); //remove the spaces in sheet name
				$sheet = strtolower($sheet); //convert sheet name to lowercase
				$objWorksheet = $objPHPExcel->getSheetByName($s); //load the sheet

				$lastRow = $objPHPExcel->setActiveSheetIndex($sheetnumber)->getHighestRow(); //get the last row in the sheet

				if ($sheet == 'products') // if sheet name is products
				{
					//loop from first row until last row
					for ($j = 2; $j <= $lastRow; $j++) {
						$sku = $objWorksheet->getCellByColumnAndRow(0, $j)->getValue();
						$prodname = $objWorksheet->getCellByColumnAndRow(1, $j)->getValue();
						$currency = $objWorksheet->getCellByColumnAndRow(2, $j)->getValue();
						$price = $objWorksheet->getCellByColumnAndRow(3, $j)->getValue();
						$saleprice = $objWorksheet->getCellByColumnAndRow(4, $j)->getValue();
						$stock = $objWorksheet->getCellByColumnAndRow(5, $j)->getValue();
						$stkqty = $objWorksheet->getCellByColumnAndRow(6, $j)->getValue();
						$cat = $objWorksheet->getCellByColumnAndRow(7, $j)->getValue();
						$sub1 = $objWorksheet->getCellByColumnAndRow(8, $j)->getValue();
						$sub2 = $objWorksheet->getCellByColumnAndRow(9, $j)->getValue();
						$sub3 = $objWorksheet->getCellByColumnAndRow(10, $j)->getValue();
						$sub4 = $objWorksheet->getCellByColumnAndRow(11, $j)->getValue();
						$editorEmp = $objWorksheet->getCellByColumnAndRow(12, $j)->getValue();

						$excel = array(
							'sku' => $sku,
							'prodname' => $prodname,
							'currency' => $currency,
							'price' => $price,
							'saleprice' => $saleprice,
							'stock' => $stock,
							'stkqty' => $stkqty,
							'cat' => $cat,
							'sub1' => $sub1,
							'sub2' => $sub2,
							'sub3' => $sub3,
							'sub4' => $sub4,
							'editorEmp' => $editorEmp,
						);

						$this->db->insert('pdata', $excel);
						$result = ($this->db->affected_rows() != 1) ? false : true;
					} //for loop ends
				} //end if clients
			} //end foreach

			//data has been written then send confirmation message
			if ($result == true) {
				$msg['message'] = "Product data uploaded successfully.";
				$this->load->view('core/importcsvproduct', $msg);
			} else {
				$msg['message'] = "There was an error uploading your file data.<br>Please be sure the header names are the same as the example \"Excel formatting details.\".<br>Contact support if you are still having problems.";
				$this->load->view('core/importcsvproduct', $msg);
			}
		} //end successfully uploaded file
	} //end product upload excel function

//Upload Employee Excel Data
	public function uploadEmployeeExcel() {
		$config['upload_path'] = './upload/excel_files/'; //upload folder path
		$config['allowed_types'] = 'xlsx|csv'; //allowed extensions
		$this->upload->initialize($config); //initialize the config items

		//if upload error
		if (!$this->upload->do_upload('userfile')) {
			$msg['message'] = "There was an error uploading your file.<br>The file should be of type: xlsx or csv.<br>Contact support if you are still having problems.";
			$this->load->view('core/importcsvemployee', $msg);
		} else //successfully uploaded the file
		{
			$upload_data = $this->upload->data(); //upload data
			$file_name = $upload_data['file_name']; //save file name

			//load the phpExcel library
			$this->load->library("Excel");

			$objReader = PHPExcel_IOFactory::createReader('Excel2007'); //excel 2007 reader

			//$objReader->setReadDataOnly(true); //set to read only

			//load excel file
			$objPHPExcel = $objReader->load('upload/excel_files/' . $file_name);
			$sheetnumber = 0;

			//as long as we are on sheet 0 then loop
			foreach ($objPHPExcel->getWorksheetIterator() as $sheet) {
				$s = $sheet->getTitle(); //get the sheet name
				$sheet = str_replace(' ', '', $s); //remove the spaces in sheet name
				$sheet = strtolower($sheet); //convert sheet name to lowercase
				$objWorksheet = $objPHPExcel->getSheetByName($s); //load the sheet

				$lastRow = $objPHPExcel->setActiveSheetIndex($sheetnumber)->getHighestRow(); //get the last row in the sheet

				if ($sheet == 'employees') // if sheet name is employees
				{
					//loop from first row until last row
					for ($j = 2; $j <= $lastRow; $j++) {
						$salutation = $objWorksheet->getCellByColumnAndRow(0, $j)->getValue();
						$fullname = $objWorksheet->getCellByColumnAndRow(1, $j)->getValue();
						$fname = $objWorksheet->getCellByColumnAndRow(2, $j)->getValue();
						$lname = $objWorksheet->getCellByColumnAndRow(3, $j)->getValue();
						$sex = $objWorksheet->getCellByColumnAndRow(4, $j)->getValue();
						$dob = $objWorksheet->getCellByColumnAndRow(5, $j)->getValue();
						$email = $objWorksheet->getCellByColumnAndRow(6, $j)->getValue();
						$add11 = $objWorksheet->getCellByColumnAndRow(7, $j)->getValue();
						$add12 = $objWorksheet->getCellByColumnAndRow(8, $j)->getValue();
						$city = $objWorksheet->getCellByColumnAndRow(9, $j)->getValue();
						$srp = $objWorksheet->getCellByColumnAndRow(10, $j)->getValue();
						$zip = $objWorksheet->getCellByColumnAndRow(11, $j)->getValue();
						$cntry = $objWorksheet->getCellByColumnAndRow(12, $j)->getValue();
						$mobile = $objWorksheet->getCellByColumnAndRow(13, $j)->getValue();
						$home = $objWorksheet->getCellByColumnAndRow(14, $j)->getValue();
						$work = $objWorksheet->getCellByColumnAndRow(15, $j)->getValue();
						$pob = $objWorksheet->getCellByColumnAndRow(16, $j)->getValue();
						$ssn = $objWorksheet->getCellByColumnAndRow(17, $j)->getValue();
						$bir = $objWorksheet->getCellByColumnAndRow(18, $j)->getValue();
						$tos = $objWorksheet->getCellByColumnAndRow(19, $j)->getValue();
						$otherdetails = $objWorksheet->getCellByColumnAndRow(20, $j)->getValue();
						$payrate = $objWorksheet->getCellByColumnAndRow(21, $j)->getValue();
						$jobtitle = $objWorksheet->getCellByColumnAndRow(22, $j)->getValue();
						$tertiaryschoolname = $objWorksheet->getCellByColumnAndRow(23, $j)->getValue();
						$secondaryschoolname = $objWorksheet->getCellByColumnAndRow(24, $j)->getValue();
						$tertiarylevel = $objWorksheet->getCellByColumnAndRow(25, $j)->getValue();
						$secondarylevel = $objWorksheet->getCellByColumnAndRow(26, $j)->getValue();
						$fullnameec = $objWorksheet->getCellByColumnAndRow(27, $j)->getValue();
						$mobileec = $objWorksheet->getCellByColumnAndRow(28, $j)->getValue();
						$homeec = $objWorksheet->getCellByColumnAndRow(29, $j)->getValue();
						$workec = $objWorksheet->getCellByColumnAndRow(30, $j)->getValue();
						$addec = $objWorksheet->getCellByColumnAndRow(31, $j)->getValue();
						$addec2 = $objWorksheet->getCellByColumnAndRow(32, $j)->getValue();
						$relationec = $objWorksheet->getCellByColumnAndRow(33, $j)->getValue();
						$fullnameec2 = $objWorksheet->getCellByColumnAndRow(34, $j)->getValue();
						$mobileec2 = $objWorksheet->getCellByColumnAndRow(35, $j)->getValue();
						$homeec2 = $objWorksheet->getCellByColumnAndRow(36, $j)->getValue();
						$workec2 = $objWorksheet->getCellByColumnAndRow(37, $j)->getValue();
						$addec22 = $objWorksheet->getCellByColumnAndRow(38, $j)->getValue();
						$addec222 = $objWorksheet->getCellByColumnAndRow(39, $j)->getValue();
						$relationec2 = $objWorksheet->getCellByColumnAndRow(40, $j)->getValue();
						$editorEmp = $objWorksheet->getCellByColumnAndRow(41, $j)->getValue();

						$excel = array(
							'salutation' => $salutation,
							'fullname' => $fullname,
							'fname' => $fname,
							'lname' => $lname,
							'sex' => $sex,
							'dob' => $dob,
							'email' => $email,
							'addl1' => $add11,
							'addl2' => $add12,
							'city' => $city,
							'srp' => $srp,
							'zip' => $zip,
							'cntry' => $cntry,
							'mobile' => $mobile,
							'home' => $home,
							'work' => $work,
							'pob' => $pob,
							'ssn-nis' => $ssn,
							'bir' => $bir,
							'tos' => $tos,
							'otherdetails' => $otherdetails,
							'payrate' => $payrate,
							'jobtitle' => $jobtitle,
							'tertiaryschoolname' => $tertiaryschoolname,
							'secondaryschoolname' => $secondaryschoolname,
							'tertiarylevel' => $tertiarylevel,
							'secondarylevel' => $secondarylevel,
							'fullnameec' => $fullnameec,
							'mobileec' => $mobileec,
							'homeec' => $homeec,
							'workec' => $workec,
							'addec' => $addec,
							'addec2' => $addec2,
							'relationec' => $relationec,
							'fullnameec2' => $fullnameec2,
							'mobileec2' => $mobileec2,
							'homeec2' => $homeec2,
							'workec2' => $workec2,
							'addec22' => $addec22,
							'addec222' => $addec222,
							'relationec2' => $relationec2,
							'editorEmp' => $editorEmp,
						);

						$this->db->insert('edata', $excel);
						$result = ($this->db->affected_rows() != 1) ? false : true;
					} //for loop ends
				} //end if clients
			} //end foreach

			//data has been written then send confirmation message
			if ($result == true) {
				$msg['message'] = "Employee data uploaded successfully.";
				$this->load->view('core/importcsvemployee', $msg);
			} else {
				$msg['message'] = "There was an error uploading your file data.<br>Please be sure the header names are the same as the example \"Excel formatting details.\".<br>Contact support if you are still having problems.";
				$this->load->view('core/importcsvemployee', $msg);
			}
		} //end successfully uploaded file
	} //end employee upload excel function

	//Client Excel
	public function uploadClientExcel() {
		$config['upload_path'] = './upload/excel_files/'; //upload folder path
		$config['allowed_types'] = 'xlsx|csv'; //allowed extensions
		$this->upload->initialize($config); //initialize the config items

		//if upload error
		if (!$this->upload->do_upload('userfile')) {
			$msg['message'] = "There was an error uploading your file.<br>The file should be of type: xlsx or csv.<br>Contact support if you are still having problems.";
			$this->load->view('core/importcsvclient', $msg);
		} else //successfully uploaded the file
		{
			$upload_data = $this->upload->data(); //upload data
			$file_name = $upload_data['file_name']; //save file name

			//load the phpExcel library
			$this->load->library("Excel");

			$objReader = PHPExcel_IOFactory::createReader('Excel2007'); //excel 2007 reader

			//$objReader->setReadDataOnly(true); //set to read only

			//load excel file
			$objPHPExcel = $objReader->load('upload/excel_files/' . $file_name);
			$sheetnumber = 0;

			//as long as we are on sheet 0 then loop
			foreach ($objPHPExcel->getWorksheetIterator() as $sheet) {
				$s = $sheet->getTitle(); //get the sheet name
				$sheet = str_replace(' ', '', $s); //remove the spaces in sheet name
				$sheet = strtolower($sheet); //convert sheet name to lowercase
				$objWorksheet = $objPHPExcel->getSheetByName($s); //load the sheet

				$lastRow = $objPHPExcel->setActiveSheetIndex($sheetnumber)->getHighestRow(); //get the last row in the sheet

				if ($sheet == 'clients') // if sheet name is clients
				{
					//loop from first row until last row
					for ($j = 2; $j <= $lastRow; $j++) {
						$fullname = $objWorksheet->getCellByColumnAndRow(0, $j)->getValue();
						$occupation = $objWorksheet->getCellByColumnAndRow(1, $j)->getValue();
						$companyname = $objWorksheet->getCellByColumnAndRow(2, $j)->getValue();
						$email = $objWorksheet->getCellByColumnAndRow(3, $j)->getValue();
						$add1 = $objWorksheet->getCellByColumnAndRow(4, $j)->getValue();
						$add2 = $objWorksheet->getCellByColumnAndRow(5, $j)->getValue();
						$work1 = $objWorksheet->getCellByColumnAndRow(6, $j)->getValue();
						$work2 = $objWorksheet->getCellByColumnAndRow(7, $j)->getValue();
						$otherdetails = $objWorksheet->getCellByColumnAndRow(8, $j)->getValue();

						$excel = array(
							'fullname' => $fullname,
							'occupation' => $occupation,
							'employer' => $companyname,
							'email' => $email,
							'add1' => $add1,
							'add2' => $add2,
							'worknum' => $work1,
							'worknum2' => $work2,
							'editorEmp' => $otherdetails,
						);

						$this->db->insert('cldata', $excel);
						$result = ($this->db->affected_rows() != 1) ? false : true;
					} //for loop ends
				} //end if clients
			} //end foreach

			//data has been written then send confirmation message
			if ($result == true) {
				$msg['message'] = "Client data uploaded successfully.";
				$this->load->view('core/importcsvclient', $msg);
			} else {
				$msg['message'] = "There was an error uploading your file data.<br>Please be sure the header names are the same as the example \"Excel formatting details.\".<br>Contact support if you are still having problems.";
				$this->load->view('core/importcsvclient', $msg);
			}
		} //end successfully uploaded file
	} //end client upload excel function

	//set up captcha and return image
	private function getimage() {
		$vals = array(
			'img_path' => './captcha/',
			'img_url' => base_url() . 'captcha/',
			'font_path' => './path/to/fonts/texb.ttf',
			'img_width' => '150',
			'img_height' => 30,
			'expiration' => 200,
			'word_length' => 4,
			'font_size' => 16,
			'img_id' => 'Imageid',
			'pool' => '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',

			// White background and border, black text and red grid
			'colors' => array(
				'background' => array(255, 255, 255),
				'border' => array(255, 255, 255),
				'text' => array(0, 0, 0),
				'grid' => array(0, 0, 0),
			),
		);

		$cap = create_captcha($vals);

		$data2 = array(
			'captcha_time' => $cap['time'],
			'ip_address' => $this->input->ip_address(),
			'word' => $cap['word'],
		);

		$query = $this->db->insert_string('captcha', $data2);
		$this->db->query($query);

		$data['cap_img'] = $cap['image'];

		return $data;
	}

}
