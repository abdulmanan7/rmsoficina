<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Core_users extends CI_Controller {
	static private $login = "";
	function __construct() {
		parent::__construct();
		// code change by @abdulmanan7
		if (!is_login()) {
			redirect('login');
		} else {
			$this->load->model('core_user_model', "coreuser");
		}
	}

	//load login page
	public function index() {
		$data['title'] = 'CoreUser';
		$data['sub_title'] = 'viewCoreUser';
		$data['users'] = $this->coreuser->get();
		$data['page'] = "core/core_users/viewusers";
		$this->load->view('core/template.php', $data);
	}

	//load add users
	public function adduser() {
		// echo "<pre>";
		// print_r($this->input->post());
		// die();
		if ($this->input->post()) {
			$this->form_validation->set_rules('usrnm', 'Username', 'trim|required|xss_clean');
			$this->form_validation->set_rules('pastech', 'Password', 'trim|required|xss_clean');
			$this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean|is_unique[coreuser.email]');
			if ($this->form_validation->run() == FALSE) {

				$data['message'] = "<p class=\"text-red login-box-msg\">Incorrect Data provide!</p>";
				$data['title'] = 'CoreUser';
				$data['sub_title'] = 'adduser';
				$data['page'] = 'core/core_users/adduser';
				$this->load->view('core/template.php', $data);
			} else {
				$config['upload_path'] = './upload/e_kdjhawkdhawjkhd';
				$config['allowed_types'] = 'gif|jpg|png';
				$config['max_size'] = '15000';
				$config['max_width'] = '3000';
				$config['max_height'] = '3000';

				$this->upload->initialize($config);

				if (!$this->upload->do_upload()) //if there was an error with file upload
				{
					$error = array('error' => $this->upload->display_errors());
					$usrnm_avatar = "blank.png";
				} else //no error with file upload
				{

					$data = $this->upload->data();
					$usrnm_avatar = $data['file_name'];
				}

				$username = $this->input->post('usrnm', true);
				$password = $this->input->post('pastech', true);
				$email = $this->input->post('email', true);
				$number = $this->input->post('number', true);
				$ut = $this->input->post('ut', true);

				$pdata = array(
					'email' => $email,
					'nombre' => $username,
					'pass' => $password,
					'foto' => $usrnm_avatar,
					'ut' => $this->input->post('ut'),
					'status' => 0,
				);
				$this->coreuser->add_user($pdata);
				$message = "<p class='alert-success'>New user added successfully!</p>";
				$this->session->set_flashdata('msg', $message);
				redirect('core_users', 'refresh');
			}
		} else {
			$data['title'] = 'CoreUser';
			$data['sub_title'] = 'adduser';
			$data['page'] = 'core/core_users/adduser';
			$this->load->view('core/template.php', $data);
		}
	}

	//load all users
	public function viewUsers() {
		$data['title'] = 'CoreUser';
		$data['sub_title'] = 'viewCoreUser';
		$data['users'] = $this->coreuser->get();
		$data['page'] = "core/core_users/viewusers";
		$this->load->view('core/template.php', $data);
	}

	//edit user
	public function edituser() {
		$id = $this->uri->segment(2);

		$data['user'] = $this->coreuser->get($id);
		$data['page'] = 'core/core_users/edituser';
		$data['title'] = 'Coreuser';
		$data['sub_title'] = 'editCoreUser';
		$data['id'] = $id;
		$data['security_code'] = false;
		$this->load->view('core/template.php', $data);
	}

	//delete user
	public function deleteuser() {
		$id = $this->uri->segment(2);
		$fieldname = 'id';
		$tablename = 'coreuser';

		$this->coreuser->delete_user($id, $fieldname, $tablename);

		$msg['message'] = "User has been deleted successfully!";
		$data['title'] = 'CoreUser';
		$data['sub_title'] = 'viewCoreUser';
		$data['users'] = $this->coreuser->get();
		$data['page'] = "core/core_users/viewusers";
		$this->load->view('core/template.php', $data);
	}

	//edit user data
	public function updateUserData() {
		$config['upload_path'] = './upload/e_kdjhawkdhawjkhd';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size'] = '15000';
		$config['max_width'] = '3000';
		$config['max_height'] = '3000';

		$this->upload->initialize($config);

		if (!$this->upload->do_upload()) //if there was an error with file upload
		{
			$error = array('error' => $this->upload->display_errors());
			$profilephoto1 = "none";
		} else //no error with file upload
		{

			$data = $this->upload->data();
			$profilephoto1 = $data['file_name'];
		}

		$this->load->model('AdminModel');
		$this->AdminModel->edit_user_info($profilephoto1);

		$msg['message'] = "User edited successfully!";
		$this->load->view('core/app_users/viewusers', $msg);
	}

	//edit employee data
	//Load add product page
	public function addclient() {
		$this->load->view('core/addclient');
	}

	//add new client
	public function addClientData() {
		$config['upload_path'] = './upload/cl_skeuhdflefjslfjseflksjef';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size'] = '15000';
		$config['max_width'] = '3000';
		$config['max_height'] = '3000';

		$this->upload->initialize($config);

		if (!$this->upload->do_upload()) //if there was an error with file upload
		{
			$error = array('error' => $this->upload->display_errors());
			$clientphoto1 = "blank.png";
		} else //no error with file upload
		{
			$data = $this->upload->data();
			$clientphoto1 = $data['file_name'];
		}

		$this->load->model('AdminModel');
		$this->AdminModel->store_client_info($clientphoto1);

		$msg['message'] = "New client added successfully!";
		$this->load->view('core/viewclients', $msg);
	}

	//load view specific client
	public function viewclient() {
		$id = $this->uri->segment(2);
		$data['id'] = $id;
		$this->load->view('core/viewspecificclient', $data);
	}

	//load edit specific client
	public function editclient() {
		$id = $this->uri->segment(2);
		$data['id'] = $id;
		$this->load->view('core/editclient', $data);
	}

	//delete client
	public function deleteclient() {
		$id = $this->uri->segment(2);
		$fieldname = 'clId';
		$tablename = 'cldata';

		$this->load->model('AdminModel');
		$this->AdminModel->deletedata($id, $fieldname, $tablename);

		$msg['message'] = "Client has been deleted successfully!";
		$this->load->view('core/viewclients', $msg);
	}

	//update client data

	//delete label
	public function deleteLabel() {
		$id = $this->uri->segment(2);
		$fieldname = 'barcodeId';
		$tablename = 'barcode';

		$this->load->model('AdminModel');
		$this->AdminModel->deletedata($id, $fieldname, $tablename);

		$msg['message'] = "Label has been deleted successfully!";
		$this->load->view('core/createBarcode', $msg);
	}
	function update_user() {
		$id = $this->uri->segment(2);
		if ($this->input->post()) {
			$config['upload_path'] = './upload/e_kdjhawkdhawjkhd';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size'] = '15000';
			$config['max_width'] = '3000';
			$config['max_height'] = '3000';

			$this->upload->initialize($config);

			if (!$this->upload->do_upload()) //if there was an error with file upload
			{
				$error = array('error' => $this->upload->display_errors());
				$profilephoto1 = "none";
			} else //no error with file upload
			{

				$data = $this->upload->data();
				$profilephoto1 = $data['file_name'];
			}
			$this->coreuser->update_user($profilephoto1, $id);
			$msg['message'] = "User edited successfully!";
			$data['users'] = $this->coreuser->get();
			$data['page'] = "core/core_users/viewusers";
			$this->load->view('core/template.php', $data);
		} else {
			$id = $this->uri->segment(2);
			$data['user'] = $this->coreuser->get($id);
			$data['page'] = 'core/core_users/edituser';
			$data['title'] = 'Coreuser';
			$data['sub_title'] = 'editCoreUser';
			$data['id'] = $id;
			$data['security_code'] = false;
			$this->load->view('core/template.php', $data);
		}
	}
//Load create barcode single page
	public function loadBarcodeSingleForm() {
		$this->load->view('core/createBarcodeSingle');
	}

//delete barcode
	public function deleteBarcode() {
		$id = $this->uri->segment(2);
		$fieldname = 'barcodeId';
		$tablename = 'barcode2';

		$this->load->model('AdminModel');
		$this->AdminModel->deletedata($id, $fieldname, $tablename);

		$msg['message'] = "Barcode has been deleted successfully!";
		$this->load->view('core/createBarcodeSingle', $msg);
	}

	//set up captcha and return image
	private function getimage() {
		$vals = array(
			'img_path' => './captcha/',
			'img_url' => base_url() . 'captcha/',
			'font_path' => './path/to/fonts/texb.ttf',
			'img_width' => '150',
			'img_height' => 30,
			'expiration' => 200,
			'word_length' => 4,
			'font_size' => 16,
			'img_id' => 'Imageid',
			'pool' => '123456789abcdefghijklmnpqrstuvwxyzABCDEFGHIJKLMNPQRSTUVWXYZ',

			// White background and border, black text and red grid
			'colors' => array(
				'background' => array(255, 255, 255),
				'border' => array(255, 255, 255),
				'text' => array(0, 0, 0),
				'grid' => array(0, 0, 0),
			),
		);

		$cap = create_captcha($vals);

		$data2 = array(
			'captcha_time' => $cap['time'],
			'ip_address' => $this->input->ip_address(),
			'word' => $cap['word'],
		);

		$query = $this->db->insert_string('captcha', $data2);
		$this->db->query($query);

		$data['cap_img'] = $cap['image'];

		return $data;
	}
}
