<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class EmployeeController extends CI_Controller {

	function __construct() {
		parent::__construct();
		// code change by @abdulmanan7
		if (!is_login()) {
			redirect('login');
		}
	}

	public function index() {
		$this->load->view('core/dashboard');
	}

	//load add employee view
	public function addEmployee() {
		$this->load->view('core/addemployee');
	}

	//load employee list
	public function empList() {
		$this->load->view('core/employeelistsimple');
	}

	public function addData() {
		$config['upload_path'] = './upload/e_kdjhawkdhawjkhd';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size'] = '15000';
		$config['max_width'] = '3000';
		$config['max_height'] = '3000';

		$this->upload->initialize($config);

		if (!$this->upload->do_upload()) //if there was an error with file upload
		{
			$error = array('error' => $this->upload->display_errors());
			$profilephoto1 = "blank.png";
		} else //no error with file upload
		{
			/*
			[file_name]    => mypic.jpg
			[file_type]    => image/jpeg
			[file_path]    => /path/to/your/upload/
			[full_path]    => /path/to/your/upload/jpg.jpg
			[raw_name]     => mypic
			[orig_name]    => mypic.jpg
			[client_name]  => mypic.jpg
			[file_ext]     => .jpg
			[file_size]    => 22.2
			[is_image]     => 1
			[image_width]  => 800
			[image_height] => 600
			[image_type]   => jpeg
			[image_size_str] => width="800" height="200"
			 */

			$data = $this->upload->data();
			$profilephoto1 = $data['file_name'];
		}

		$this->load->model('EmployeeModel');
		$this->EmployeeModel->store_info($profilephoto1);

		$msg['message'] = "New employee added successfully!";
		$this->load->view('core/addemployee', $msg);
	}

	public function fullprofile() {
		$id['empid'] = $this->uri->segment(2);
		$this->load->view('core/employeelistfull', $id);
	}
	//set up captcha and return image
	private function getimage() {
		$vals = array(
			'img_path' => './captcha/',
			'img_url' => base_url() . 'captcha/',
			'font_path' => './path/to/fonts/texb.ttf',
			'img_width' => '150',
			'img_height' => 30,
			'expiration' => 200,
			'word_length' => 4,
			'font_size' => 16,
			'img_id' => 'Imageid',
			'pool' => '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',

			// White background and border, black text and red grid
			'colors' => array(
				'background' => array(255, 255, 255),
				'border' => array(255, 255, 255),
				'text' => array(0, 0, 0),
				'grid' => array(0, 0, 0),
			),
		);

		$cap = create_captcha($vals);

		$data2 = array(
			'captcha_time' => $cap['time'],
			'ip_address' => $this->input->ip_address(),
			'word' => $cap['word'],
		);

		$query = $this->db->insert_string('captcha', $data2);
		$this->db->query($query);

		$data['cap_img'] = $cap['image'];

		return $data;
	}
}