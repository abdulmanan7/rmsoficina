<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->helper('captcha');
		$this->load->model('login_model', "auth");
		//Do your magic here
	}
	public function index() {
		//This code prevents the user from pressing the back button after logging out
		//of the application.
		$this->output->set_header('Last-Modified:' . gmdate('D, d M Y H:i:s') . 'GMT');
		$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
		$this->output->set_header('Cache-Control: post-check=0, pre-check=0', false);
		$this->output->set_header('Pragma: no-cache');

		// $data = $this->getimage();
		$data['security_code'] = false;
		$this->load->view('core/loginpage', $data);
	}
	function rolekey_exists($key) {
		$this->load->model('AdminModel');
		$this->auth->mail_exists($key);
	}
	public function edituser() {
		$id = $this->uri->segment(2);
		$data['id'] = $id;
		$data['security_code'] = false;
		$this->load->view('core/edituser', $data);
	}
	function update_core_user($value = '') {
		$config['upload_path'] = './upload/e_kdjhawkdhawjkhd';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size'] = '15000';
		$config['max_width'] = '3000';
		$config['max_height'] = '3000';

		$this->upload->initialize($config);

		if (!$this->upload->do_upload()) //if there was an error with file upload
		{
			$error = array('error' => $this->upload->display_errors());
			$profilephoto1 = "none";
		} else //no error with file upload
		{

			$data = $this->upload->data();
			$profilephoto1 = $data['file_name'];
		}

		$this->auth->edit_user_info($profilephoto1);

		$msg['message'] = "User edited successfully!";
		$this->load->view('core/viewusers', $msg);
	}
	function register() {
		if ($this->input->post()) {
			$this->form_validation->set_rules('pastech', 'Password', 'trim|required|xss_clean');
			$this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean|is_unique[coreuser.email]');
			if ($this->form_validation->run() == FALSE) {

				$data = $this->getimage();

				$data['message'] = "<p class=\"text-red login-box-msg\">Incorrect Data provide!</p>";
				$this->load->view('core/register', $data);
			} else {
				$password = $this->input->post('pastech', true);
				$email = $this->input->post('email', true);

				$pdata = array(
					'email' => strtolower($email),
					'pass' => $password,
					'foto' => "blank.png",
					'ut' => "Customer",
					'status' => 0,
				);
				$this->auth->register_user($pdata);
				$message = "<p class='alert-success'>New user added successfully!</p>";
				$this->session->set_flashdata('msg', $message);
				redirect('login', 'refresh');
			}
		} else {
			$this->load->view('core/register');
		}
	}
	//check if user is valid
	public function checkuser() {
		$this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean');
		$this->form_validation->set_rules('pastech', 'Password', 'trim|required|xss_clean');
		// $this->form_validation->set_rules('captcha', 'Security Code', 'trim|required|xss_clean');

		if ($this->form_validation->run() == FALSE) {
			$data = $this->getimage();

			$data['message'] = "<p class=\"text-red login-box-msg\">Incorrect Information Entered!</p>";
			$data['security_code'] = $this->loginAttemps();
			$this->load->view('core/loginpage', $data);
			//$this->index();
		} else {
			$username = $this->input->post('email', true);
			$password = $this->input->post('pastech', true);

			$this->load->model('AdminModel');
			$is_valid = $this->auth->validate($username, $password);
			if (isset($_POST['captcha'])) {
				// First, delete old captchas
				$expiration = time() - 200; // Two hour limit
				$this->db->where('captcha_time < ', $expiration)
					->delete('captcha');

				// Then see if a captcha exists:
				$sql = 'SELECT COUNT(*) AS count FROM captcha WHERE word = ? AND ip_address = ? AND captcha_time > ?';
				$binds = array($_POST['captcha'], $this->input->ip_address(), $expiration);
				$query = $this->db->query($sql, $binds);
				$row = $query->row();

				if ($row->count == 0) {

					$data = $this->getimage();
					$data['message'] = "<p class=\"text-red login-box-msg\">captcha Dosenot match!</p>";
					$data['security_code'] = $this->loginAttemps();
					$this->load->view('core/loginpage', $data);
				}
			}
			if ($is_valid) {
				$get_data = $this->auth->get_data($username);
				foreach ($get_data as $val) {
					$id = $val->id;
					$ut = $val->ut;
					$email = $val->email;
					$usrnm = $val->nombre;
					$photo = $val->foto;
				}

				$data = array(
					'userid' => $id,
					'ut' => $ut,
					'email' => $email,
					'photo' => $photo,
					'usrnm' => $usrnm,
					'is_logged_in' => true,
				);

				$this->session->set_userdata($data);
				unset($_SESSION['attemp']);
				$this->load->view('core/dashboard');
			} else {
				$data = $this->getimage();
				$data['message'] = "<p class=\"text-red login-box-msg\">Incorrect Information Entered!</p>";
				$data['security_code'] = $this->loginAttemps();
				$this->load->view('core/loginpage', $data);
			}
		}
	}
	private function getimage() {
		$vals = array(
			'img_path' => './captcha/',
			'img_url' => base_url() . 'captcha/',
			'font_path' => './path/to/fonts/texb.ttf',
			'img_width' => '150',
			'img_height' => 30,
			'expiration' => 200,
			'word_length' => 4,
			'font_size' => 16,
			'img_id' => 'Imageid',
			'pool' => '123456789abcdefghijklmnpqrstuvwxyzABCDEFGHIJKLMNPQRSTUVWXYZ',

			// White background and border, black text and red grid
			'colors' => array(
				'background' => array(255, 255, 255),
				'border' => array(255, 255, 255),
				'text' => array(0, 0, 0),
				'grid' => array(0, 0, 0),
			),
		);

		$cap = create_captcha($vals);

		$data2 = array(
			'captcha_time' => $cap['time'],
			'ip_address' => $this->input->ip_address(),
			'word' => $cap['word'],
		);

		$query = $this->db->insert_string('captcha', $data2);
		$this->db->query($query);

		$data['cap_img'] = $cap['image'];

		return $data;
	}
	public function loginAttemps() {
		if (isset($_SESSION['attemp'])) {
			$count = $this->session->userdata('attemp');
		} else {
			$_SESSION['attemp'] = 1;
			$count = 0;
		}
		if ($count == 5) {
			return TRUE;
		} else {

			$_SESSION['attemp'] += 1;
			return FALSE;
		}
	}
}

/* End of file login.php */
/* Location: ./application/controllers/login.php */