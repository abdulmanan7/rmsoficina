<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

	function __construct() {
		parent::__construct();
		// code change by @abdulmanan7
		if (!is_login()) {
			redirect('login');
		}
	}

	public function index() {
		$this->load->view('core/dashboard');
	}

	//set up captcha and return image
	private function getimage() {
		$vals = array(
			'img_path' => './captcha/',
			'img_url' => base_url() . 'captcha/',
			'font_path' => './path/to/fonts/texb.ttf',
			'img_width' => '150',
			'img_height' => 30,
			'expiration' => 200,
			'word_length' => 4,
			'font_size' => 16,
			'img_id' => 'Imageid',
			'pool' => '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',

			// White background and border, black text and red grid
			'colors' => array(
				'background' => array(255, 255, 255),
				'border' => array(255, 255, 255),
				'text' => array(0, 0, 0),
				'grid' => array(0, 0, 0),
			),
		);

		$cap = create_captcha($vals);

		$data2 = array(
			'captcha_time' => $cap['time'],
			'ip_address' => $this->input->ip_address(),
			'word' => $cap['word'],
		);

		$query = $this->db->insert_string('captcha', $data2);
		$this->db->query($query);

		$data['cap_img'] = $cap['image'];

		return $data;
	}
}
