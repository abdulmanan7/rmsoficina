<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MyCompanyController extends CI_Controller {

	function __construct() {
		parent::__construct();
		// code change by @abdulmanan7
		if (!is_login()) {
			redirect('login');
		}
	}

	//load add company details
	public function addCompany() {
		$this->load->view('core/addcompany');
	}

	//add company data
	public function addcompanyinformation() {
		$config['upload_path'] = './upload/comp_awhdlawhdal';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size'] = '15000';
		$config['max_width'] = '3000';
		$config['max_height'] = '3000';

		$this->upload->initialize($config);

		if (!$this->upload->do_upload()) //if there was an error with file upload
		{
			$error = array('error' => $this->upload->display_errors());
			$photo = "blank.png";
		} else //no error with file upload
		{
			$data = $this->upload->data();
			$photo = $data['file_name'];
		}

		$this->load->model('MyCompanyModel');
		$this->MyCompanyModel->store_comp_info($photo);

		$msg['message'] = "Company data added successfully!";
		$this->load->view('core/addcompany', $msg);
	}

	//load view company details
	public function viewCompany() {
		$this->load->view('core/viewcompany');
	}

	//load edit company data
	public function editcomp() {
		$id = 1;
		$data['id'] = $id;
		$this->load->view('core/editcompany', $data);
	}

	//update company data
	public function updateCompData() {
		$config['upload_path'] = './upload/comp_awhdlawhdal';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size'] = '15000';
		$config['max_width'] = '3000';
		$config['max_height'] = '3000';

		$this->upload->initialize($config);

		if (!$this->upload->do_upload()) //if there was an error with file upload
		{
			$error = array('error' => $this->upload->display_errors());
			$photo = "none";
		} else //no error with file upload
		{
			$data = $this->upload->data();
			$photo = $data['file_name'];
		}

		$this->load->model('MyCompanyModel');
		$this->MyCompanyModel->update_comp_info($photo);

		$msg['message'] = "Company Information Updated Successfully!";
		$this->load->view('core/viewcompany', $msg);
	}

	//set up captcha and return image
	private function getimage() {
		$vals = array(
			'img_path' => './captcha/',
			'img_url' => base_url() . 'captcha/',
			'font_path' => './path/to/fonts/texb.ttf',
			'img_width' => '150',
			'img_height' => 30,
			'expiration' => 200,
			'word_length' => 4,
			'font_size' => 16,
			'img_id' => 'Imageid',
			'pool' => '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',

			// White background and border, black text and red grid
			'colors' => array(
				'background' => array(255, 255, 255),
				'border' => array(255, 255, 255),
				'text' => array(0, 0, 0),
				'grid' => array(0, 0, 0),
			),
		);

		$cap = create_captcha($vals);

		$data2 = array(
			'captcha_time' => $cap['time'],
			'ip_address' => $this->input->ip_address(),
			'word' => $cap['word'],
		);

		$query = $this->db->insert_string('captcha', $data2);
		$this->db->query($query);

		$data['cap_img'] = $cap['image'];

		return $data;
	}
}