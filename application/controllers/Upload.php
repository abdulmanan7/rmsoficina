<?php

class Upload extends CI_Controller {
	protected $final_upload_path;
	protected $root_path;
	protected $dir_path;
	public function __construct() {

		parent::__construct();
		// Load the helpers
		$this->load->helper(array('form', 'url'));
		$this->load->model('upload_model', 'upload_db');
		$this->dir_path = $this->get_dir_path();
		$this->final_upload_path = realpath($this->get_path($this->dir_path));
		// echo $this->dir_path;die;
	}

	public function form() {

		// Load the form
		$this->load->view('upload_form', array('error' => ' '));
	}

	/**
	 * Multiple upload functionality will fallback to CodeIgniters default do_upload()
	 * method so configuration is backwards compatible between do_upload() and the new do_multi_upload()
	 * method provided by Multi File Upload extension.
	 *
	 */
	public function do_upload() {
		if (count($_FILES['uploadfile']['name']) > 4) {
			echo "Files exceded from allowed files you can upload 4 files at a time.";
			die;
		};
		// Detect form submission.
		if ($this->input->post('submit')) {

			$path = './upload/';
			$this->load->library('upload');

			// Define file rules
			$this->upload->initialize(array(
				"upload_path" => $path,
				"allowed_types" => "gif|jpg|png|jpeg|pdf",
				"max_size" => '2400',
			));

			if ($this->upload->do_multi_upload("uploadfile")) {

				$data['upload_data'] = $this->upload->get_multi_upload_data();
				$res = $this->scale_image($data['upload_data']);
				$res_html = "<ul>";
				foreach ($res['path'] as $key => $val) {
					$res_html .= "<li><img class='img-responsive avatar' src='" . base_url($val) . "'></li>";
				}
				echo $res_html . "</ul>";
				die;
				echo json_encode($res);
				// echo "<pre>";
				// print_r($res);
				die;
				echo '<p class = "bg-success">' . count($data['upload_data']) . 'File(s) successfully uploaded.</p>';

			} else {
				// Output the errors
				$errors = array('error' => $this->upload->display_errors('<p class = "bg-danger">', '</p>'));

				foreach ($errors as $k => $error) {
					echo $error;
				}

			}

		} else {
			echo '<p class = "bg-danger">An error occured, please try again later.</p>';

		}
		// Exit to avoid further execution
		exit();
	}
	function scale_image($files) {
		$this->load->library('image_lib');
		$response = array();
		/** $files is array of image just upload through form **/
		foreach ($files as $file) {
			$file_name = $file['file_name'];
			/** there is possiblity that the files array may contain pdf as well
			so let take care of that **/
			if ($file['file_ext'] != ".pdf") {
				/** if image width is greater then 1024 reize it to 1024x768 **/
				if ($file['image_width'] > 1024) {
					$configer = array(
						'image_library' => 'gd2',
						'source_image' => $file['full_path'],
						'maintain_ratio' => TRUE,
						'width' => 1024,
						'height' => 768,
						'new_image' => $this->final_upload_path . "/" . $file_name,
					);
					$this->image_lib->clear();
					$this->image_lib->initialize($configer);
					$this->image_lib->resize();
					/** finally unlink the cache file **/
					unlink($file['full_path']);
				} else {
					rename("./upload/" . $file_name, $this->final_upload_path . "/" . $file_name);
				}
			} else {
				/** if not image file just move the file from temp directory **/
				rename("./upload/" . $file_name, $this->final_upload_path . "/" . $file_name);
			}
			$response['path'][] = $this->dir_path . "/" . $this->root_path . "/" . $file_name;
			$pdata = array(
				'modulo' => 'test',
				'idmodulo' => 1,
				'nomarchivo' => $file_name,
				'ruta' => $this->root_path,
				'date_created' => date("Y-m-d H:i:s"),
				'status' => 0,
			);
			$response['id'][] = $this->upload_db->save_uploaded_file($pdata);
		}
		return $response;
	}
	function get_path($root = NULL, $just_root = false) {
		$date = new DateTime();
		$year = $date->format("Y");
		$month = $date->format("m");
		$week = $date->format("W");
		$this->root_path = $year . '/1/' . $month . '/' . $week;
		$new_path = FCPATH . $root . '/' . $this->root_path;
		if (!file_exists($new_path)) {
			mkdir($new_path, 0777, true);
		}
		return $new_path;
	}
	function get_dir_path($param = NULL) {
		/** if param is set use it otherwise give it a default group **/
		$param = $param ? $param : "DGROUP";
		$dir = $this->upload_db->get_root($param);
		return str_replace("\\", "/", $dir['deslarga']);
	}
}