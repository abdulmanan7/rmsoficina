<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/********
 **
|| author : @abdulmanan7
|| developer : precisetech
|| purpose common helper provide repid common function
 **
 *********/
if (!function_exists("is_login")) {
	function is_login($value = '') {
		$ci = &get_instance();
		if (!$ci->session->userdata('is_logged_in')) {
			return FALSE;
		}
		return TRUE;
	}
}

/* End of file common_helper.php */
/* Location: ./application/helpers/common_helper.php */