<?php

class AdminModel extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	//Validate user login
	function validate($username, $password) {
		$this->db->select('*');
		$this->db->from('coreuser');
		$this->db->where('nombre', $username);
		$this->db->limit(1);

		$query = $this->db->get();

		$data = $query->result_array(); //create an array

		foreach ($data as $field) {
			$pass = $field['pass'];
		}

		if ($query->num_rows() == 1) {

			if (password_verify($password, $pass)) {
				return $query->result();
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	//Get Session values
	function get_data($username) {
		$this->db->select('*');
		$this->db->from('appuser');
		$this->db->where('usrnm', $username);
		$this->db->limit(1);
		$query = $this->db->get();
		return $query->result();
	}

	//Store user data
	function store_info($profilephoto1) {
		$encryptedpass = password_hash($this->input->post('pastech', TRUE), PASSWORD_BCRYPT, array("cost" => 10));

		$data = array(
			'fullname' => $this->input->post('fullname', TRUE),
			'ut' => $this->input->post('ut', TRUE),
			'email' => $this->input->post('email', TRUE),
			'usrnm' => $this->input->post('usrnm', TRUE),
			'pastech' => $encryptedpass,
			'photo' => $profilephoto1,
		);

		$this->db->insert('appuser', $data);
	}
	function register_user($data) {
		$encryptedpass = password_hash($data['pass'], PASSWORD_BCRYPT, array("cost" => 10));
		$data['pass'] = $encryptedpass;
		$this->db->insert('coreuser', $data);
	}
	function mail_exists($key) {
		$this->db->where('email', $key);
		$query = $this->db->get('coreuser');
		if ($query->num_rows() > 0) {
			return true;
		} else {
			return false;
		}
	}
	//Edit user data
	function edit_user_info($profilephoto1) {
		if (strcmp($profilephoto1, "none") == 0) {
//if no new photo
			if (strcmp($this->input->post('pastech'), "nochange") == 0) {
//if no new password
				$data = array(
					'fullname' => $this->input->post('fullname', TRUE),
					'ut' => $this->input->post('ut', TRUE),
					'email' => $this->input->post('email', TRUE),
					'usrnm' => $this->input->post('usrnm', TRUE),
				);

				$this->db->set($data);
				$this->db->where('userid', $this->input->post('id', TRUE));
				$this->db->update('appuser');
			} //end if no new password
			else {
//if a new password
				$encryptedpass = password_hash($this->input->post('pastech', TRUE), PASSWORD_BCRYPT, array("cost" => 10));

				$data = array(
					'fullname' => $this->input->post('fullname', TRUE),
					'ut' => $this->input->post('ut', TRUE),
					'email' => $this->input->post('email', TRUE),
					'usrnm' => $this->input->post('usrnm', TRUE),
					'pastech' => $encryptedpass,
				);

				$this->db->set($data);
				$this->db->where('userid', $this->input->post('id', TRUE));
				$this->db->update('appuser');
			} //end if new password
		} //end if no new photo
		else {
//new photo
			if (strcmp($this->input->post('pastech'), "nochange") == 0) {
//if no new password
				$data = array(
					'fullname' => $this->input->post('fullname', TRUE),
					'ut' => $this->input->post('ut', TRUE),
					'email' => $this->input->post('email', TRUE),
					'usrnm' => $this->input->post('usrnm', TRUE),
					'photo' => $profilephoto1,
				);

				$this->db->set($data);
				$this->db->where('userid', $this->input->post('id', TRUE));
				$this->db->update('appuser');
			} //end if no new password
			else {
//if a new password
				$encryptedpass = password_hash($this->input->post('pastech', TRUE), PASSWORD_BCRYPT, array("cost" => 10));

				$data = array(
					'fullname' => $this->input->post('fullname', TRUE),
					'ut' => $this->input->post('ut', TRUE),
					'email' => $this->input->post('email', TRUE),
					'usrnm' => $this->input->post('usrnm', TRUE),
					'photo' => $profilephoto1,
					'pastech' => $encryptedpass,
				);

				$this->db->set($data);
				$this->db->where('userid', $this->input->post('id', TRUE));
				$this->db->update('appuser');
			} //end if new password
		} //end if new photo
	}

	//Edit employee data
	function edit_employee_info($profilephoto1) {
		if (strcmp($profilephoto1, "none") == 0) {
//if no new photo
			$data = array(
				'fullname' => $this->input->post('fullname', TRUE),
				'fname' => $this->input->post('fname', TRUE),
				'lname' => $this->input->post('lname', TRUE),
				'email' => $this->input->post('email', TRUE),
				'addl1' => $this->input->post('addl1', TRUE),
				'addl2' => $this->input->post('addl2', TRUE),
				'city' => $this->input->post('city', TRUE),
				'srp' => $this->input->post('srp', TRUE),
				'zip' => $this->input->post('zip', TRUE),
				'mobile' => $this->input->post('mobile', TRUE),
				'home' => $this->input->post('home', TRUE),
				'work' => $this->input->post('work', TRUE),
				'ssn-nis' => $this->input->post('ssn-nis', TRUE),
				'bir' => $this->input->post('bir', TRUE),
				'tos' => $this->input->post('tos', TRUE),
				'otherdetails' => $this->input->post('otherdetails', TRUE),
				'payrate' => $this->input->post('payrate', TRUE),
				'jobtitle' => $this->input->post('jobtitle', TRUE),
				'tertiaryschoolname' => $this->input->post('tertiaryschoolname', TRUE),
				'secondaryschoolname' => $this->input->post('secondaryschoolname', TRUE),
				'tertiarylevel' => $this->input->post('tertiarylevel', TRUE),
				'secondarylevel' => $this->input->post('secondarylevel', TRUE),
				'fullnameec' => $this->input->post('fullnameec', TRUE),
				'mobileec' => $this->input->post('mobileec', TRUE),
				'homeec' => $this->input->post('homeec', TRUE),
				'workec' => $this->input->post('workec', TRUE),
				'addec' => $this->input->post('addec', TRUE),
				'addec2' => $this->input->post('addec2', TRUE),
				'relationec' => $this->input->post('relationec', TRUE),
				'fullnameec2' => $this->input->post('fullnameec2', TRUE),
				'mobileec2' => $this->input->post('mobileec2', TRUE),
				'homeec2' => $this->input->post('homeec2', TRUE),
				'workec2' => $this->input->post('workec2', TRUE),
				'addec22' => $this->input->post('addec22', TRUE),
				'addec222' => $this->input->post('addec222', TRUE),
				'relationec2' => $this->input->post('relationec2', TRUE),
				'editorEmp' => $this->input->post('editorEmp', TRUE),
			);

			$this->db->set($data);
			$this->db->where('eid', $this->input->post('id', TRUE));
			$this->db->update('edata');
		} //end if no new photo
		else {
//new photo
			$data = array(
				'fullname' => $this->input->post('fullname', TRUE),
				'fname' => $this->input->post('fname', TRUE),
				'lname' => $this->input->post('lname', TRUE),
				'profilephoto' => $profilephoto1,
				'email' => $this->input->post('email', TRUE),
				'addl1' => $this->input->post('addl1', TRUE),
				'addl2' => $this->input->post('addl2', TRUE),
				'city' => $this->input->post('city', TRUE),
				'srp' => $this->input->post('srp', TRUE),
				'zip' => $this->input->post('zip', TRUE),
				'mobile' => $this->input->post('mobile', TRUE),
				'home' => $this->input->post('home', TRUE),
				'work' => $this->input->post('work', TRUE),
				'ssn-nis' => $this->input->post('ssn-nis', TRUE),
				'bir' => $this->input->post('bir', TRUE),
				'tos' => $this->input->post('tos', TRUE),
				'otherdetails' => $this->input->post('otherdetails', TRUE),
				'payrate' => $this->input->post('payrate', TRUE),
				'jobtitle' => $this->input->post('jobtitle', TRUE),
				'tertiaryschoolname' => $this->input->post('tertiaryschoolname', TRUE),
				'secondaryschoolname' => $this->input->post('secondaryschoolname', TRUE),
				'tertiarylevel' => $this->input->post('tertiarylevel', TRUE),
				'secondarylevel' => $this->input->post('secondarylevel', TRUE),
				'fullnameec' => $this->input->post('fullnameec', TRUE),
				'mobileec' => $this->input->post('mobileec', TRUE),
				'homeec' => $this->input->post('homeec', TRUE),
				'workec' => $this->input->post('workec', TRUE),
				'addec' => $this->input->post('addec', TRUE),
				'addec2' => $this->input->post('addec2', TRUE),
				'relationec' => $this->input->post('relationec', TRUE),
				'fullnameec2' => $this->input->post('fullnameec2', TRUE),
				'mobileec2' => $this->input->post('mobileec2', TRUE),
				'homeec2' => $this->input->post('homeec2', TRUE),
				'workec2' => $this->input->post('workec2', TRUE),
				'addec22' => $this->input->post('addec22', TRUE),
				'addec222' => $this->input->post('addec222', TRUE),
				'relationec2' => $this->input->post('relationec2', TRUE),
				'editorEmp' => $this->input->post('editorEmp', TRUE),
			);

			$this->db->set($data);
			$this->db->where('eid', $this->input->post('id', TRUE));
			$this->db->update('edata');
		} //end if new photo
	}

	//Delete user data
	function deletedata($id, $fieldname, $tablename) {

		$this->db->delete($tablename, array($fieldname => $id));
	}

/*
Product functions
 */

	//Add product category
	function add_category() {
		/*$this -> db -> select('*');
		$this -> db -> from('pcategories');
		$this -> db -> where('cat',$this->input->post('maincategory', TRUE));

		$result = $this->db->get();

		if($result->num_rows() >=1)
		{
		$str = "Sorry, that main category exists. Please add a different category name.";
		return $str;
		}
		else
		{*/
		$data = array(
			'cat' => $this->input->post('maincategory', TRUE),
			'sub1' => $this->input->post('subcategory1', TRUE),
			'sub2' => $this->input->post('subcategory2', TRUE),
			'sub3' => $this->input->post('subcategory3', TRUE),
			'sub4' => $this->input->post('subcategory4', TRUE),
		);

		$this->db->insert('pcategories', $data);

		$str = "Success! Category/Categories Added.";
		return $str;
		//}
	}

	//Add product category
	function update_category() {
		/*$this -> db -> select('*');
		$this -> db -> from('pcategories');
		$this -> db -> where('cat',$this->input->post('maincategory', TRUE));

		$result = $this->db->get();

		if($result->num_rows() >=1)
		{
		$str = "Sorry, that main category exists. Please add a different category name.";
		return $str;
		}
		else
		{*/
		$data = array(
			'cat' => $this->input->post('maincategory', TRUE),
			'sub1' => $this->input->post('subcategory1', TRUE),
			'sub2' => $this->input->post('subcategory2', TRUE),
			'sub3' => $this->input->post('subcategory3', TRUE),
			'sub4' => $this->input->post('subcategory4', TRUE),
		);

		$this->db->set($data);
		$this->db->where('catid', $this->input->post('catid', TRUE));
		$this->db->update('pcategories');

		$str = "Success! Category/Categories Edited.";
		return $str;
		//}
	}

	//retrieve categories for main category input box
	function load_inputbox_category() {
		$this->db->select('cat');
		$this->db->from('pcategories');
		$query = $this->db->get();

		if ($query->num_rows() >= 1) {
			foreach ($query->result_array() as $row) {
				$new_row[] = stripslashes($row['cat']);
			}
			return $new_row;
		} else {
			return false;
		}
	}

	//Add product data
	function add_product_info($flag, $files) {
		/* Adding photodata */
		if ($flag == 1) {
			$file_data = array();
			$count = 1;
			foreach ($files as $file) {
				$field = 'file_name' . $count;

				$file_data[$field] = $file['file_name'];

				$count++;
			}

			$count = 0;

			$file_data2 = array();
			$file_data2 = array(
				'sku' => $this->input->post('sku', TRUE),
				'prodname' => $this->input->post('prodname', TRUE),
				'currency' => $this->input->post('currency', TRUE),
				'price' => $this->input->post('price', TRUE),
				'saleprice' => $this->input->post('saleprice', TRUE),
				'stock' => $this->input->post('stock', TRUE),
				'stkqty' => $this->input->post('stkqty', TRUE),
				'cat' => $this->input->post('cat', TRUE),
				'sub1' => $this->input->post('sub1', TRUE),
				'sub2' => $this->input->post('sub2', TRUE),
				'sub3' => $this->input->post('sub3', TRUE),
				'sub4' => $this->input->post('sub4', TRUE),
				'editorEmp' => $this->input->post('editorEmp', TRUE),
			);

			$file_data = array_merge($file_data, $file_data2);
			$this->db->insert('pdata', $file_data);

		} else if ($flag == 2) {
			$file_data = array(
				'sku' => $this->input->post('sku', TRUE),
				'prodname' => $this->input->post('prodname', TRUE),
				'currency' => $this->input->post('currency', TRUE),
				'price' => $this->input->post('price', TRUE),
				'saleprice' => $this->input->post('saleprice', TRUE),
				'stock' => $this->input->post('stock', TRUE),
				'stkqty' => $this->input->post('stkqty', TRUE),
				'cat' => $this->input->post('cat', TRUE),
				'sub1' => $this->input->post('sub1', TRUE),
				'sub2' => $this->input->post('sub2', TRUE),
				'sub3' => $this->input->post('sub3', TRUE),
				'sub4' => $this->input->post('sub4', TRUE),
				'editorEmp' => $this->input->post('editorEmp', TRUE),
			);

			$this->db->insert('pdata', $file_data);
		}
	}

	//Edit and update product data
	function edit_product_info($flag, $files) {
		/* Adding photodata */
		if ($flag == 1) {
			$file_data = array();
			$count = 1;
			foreach ($files as $file) {
				$field = 'file_name' . $count;

				$file_data[$field] = $file['file_name'];

				$count++;
			}

			$count = 0;

			$file_data2 = array();
			$file_data2 = array(
				'sku' => $this->input->post('sku', TRUE),
				'prodname' => $this->input->post('prodname', TRUE),
				'currency' => $this->input->post('currency', TRUE),
				'price' => $this->input->post('price', TRUE),
				'saleprice' => $this->input->post('saleprice', TRUE),
				'stock' => $this->input->post('stock', TRUE),
				'stkqty' => $this->input->post('stkqty', TRUE),
				'cat' => $this->input->post('cat', TRUE),
				'sub1' => $this->input->post('sub1', TRUE),
				'sub2' => $this->input->post('sub2', TRUE),
				'sub3' => $this->input->post('sub3', TRUE),
				'sub4' => $this->input->post('sub4', TRUE),
				'editorEmp' => $this->input->post('editorEmp', TRUE),
			);

			$file_data = array_merge($file_data, $file_data2);

			$this->db->set($file_data);
			$this->db->where('prodId', $this->input->post('id', TRUE));
			$this->db->update('pdata');

		} else if ($flag == 2) {
			$file_data = array(
				'sku' => $this->input->post('sku', TRUE),
				'prodname' => $this->input->post('prodname', TRUE),
				'currency' => $this->input->post('currency', TRUE),
				'price' => $this->input->post('price', TRUE),
				'saleprice' => $this->input->post('saleprice', TRUE),
				'stock' => $this->input->post('stock', TRUE),
				'stkqty' => $this->input->post('stkqty', TRUE),
				'cat' => $this->input->post('cat', TRUE),
				'sub1' => $this->input->post('sub1', TRUE),
				'sub2' => $this->input->post('sub2', TRUE),
				'sub3' => $this->input->post('sub3', TRUE),
				'sub4' => $this->input->post('sub4', TRUE),
				'editorEmp' => $this->input->post('editorEmp', TRUE),
			);

			$this->db->set($file_data);
			$this->db->where('prodId', $this->input->post('id', TRUE));
			$this->db->update('pdata');
		}
	}

	/*
	Client Fucntions
	 */
	//Store client data
	function store_client_info($clientphoto1) {
		$data = array(
			'fullname' => $this->input->post('fullname', TRUE),
			'occupation' => $this->input->post('occupation', TRUE),
			'employer' => $this->input->post('employer', TRUE),
			'email' => $this->input->post('email', TRUE),
			'add1' => $this->input->post('add1', TRUE),
			'add2' => $this->input->post('add2', TRUE),
			'worknum' => $this->input->post('worknum', TRUE),
			'worknum2' => $this->input->post('worknum2', TRUE),
			'editorEmp' => $this->input->post('editorEmp', TRUE),
			'photo' => $clientphoto1,
		);

		$this->db->insert('cldata', $data);
	}

	//Update client data
	function update_client_info($clientphoto1) {
		if (strcmp($clientphoto1, "none") != 0) //a new photo
		{
			$data = array(
				'fullname' => $this->input->post('fullname', TRUE),
				'occupation' => $this->input->post('occupation', TRUE),
				'employer' => $this->input->post('employer', TRUE),
				'email' => $this->input->post('email', TRUE),
				'add1' => $this->input->post('add1', TRUE),
				'add2' => $this->input->post('add2', TRUE),
				'worknum' => $this->input->post('worknum', TRUE),
				'worknum2' => $this->input->post('worknum2', TRUE),
				'editorEmp' => $this->input->post('editorEmp', TRUE),
				'photo' => $clientphoto1,
			);

			$this->db->set($data);
			$this->db->where('clId', $this->input->post('clId', TRUE));
			$this->db->update('cldata');
		} else {
			$data = array(
				'fullname' => $this->input->post('fullname', TRUE),
				'occupation' => $this->input->post('occupation', TRUE),
				'employer' => $this->input->post('employer', TRUE),
				'email' => $this->input->post('email', TRUE),
				'add1' => $this->input->post('add1', TRUE),
				'add2' => $this->input->post('add2', TRUE),
				'worknum' => $this->input->post('worknum', TRUE),
				'worknum2' => $this->input->post('worknum2', TRUE),
				'editorEmp' => $this->input->post('editorEmp', TRUE),
			);

			$this->db->set($data);
			$this->db->where('clId', $this->input->post('clId', TRUE));
			$this->db->update('cldata');
		}
	}

	//Get comp name
	function get_comp_name() {
		$query = $this->db->get('compdata');

		foreach ($query->result() as $row) {
			$compname = $row->compname;
		}

		return $compname;
	}

	//Get comp work number
	function get_comp_phone() {
		$query = $this->db->get('compdata');

		foreach ($query->result() as $row) {
			$work = $row->work;
		}

		return $work;
	}

	//Create image name
	function image_name() {
		$query = $this->db->get('barcode');
		$rows = $query->num_rows();

		if ($rows == 0) {
			$barcodeName = "image_0";
		} else {
			$rows = $rows + 1;
			$barcodeName = "image_" . $rows;
		}

		return $barcodeName;
	}

	//Get store barcode data
	function store_barcode_data($text, $barcodeName) {
		$data = array(
			'text' => $text,
			'imageName' => $barcodeName,
		);

		$this->db->insert('barcode', $data);
	}

	//Create image name2
	function image_name2() {
		$query = $this->db->get('barcode2');
		$rows = $query->num_rows();

		if ($rows == 0) {
			$barcodeName = "image_0";
		} else {
			$rows = $rows + 1;
			$barcodeName = "image_" . $rows;
		}

		return $barcodeName;
	}

	//Get store barcode data
	function store_barcode_data2($text, $barcodeName) {
		$data = array(
			'text' => $text,
			'imageName' => $barcodeName,
		);

		$this->db->insert('barcode2', $data);
	}

	//Get prod data
	function get_prod_data($prodId) {
		$this->db->select('*');
		$this->db->from('pdata');
		$this->db->where('prodId', $prodId);
		$query = $this->db->get();

		return $query->result_array();
	}

}
