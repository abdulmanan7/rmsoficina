<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Core_user_model extends CI_Model {
	function get($id = '') {
		$this->db->select();
		$this->db->from('coreuser');
		if ($id) {
			$this->db->where('id', $id);
			$data = $this->db->get();
			$data = $data->row_array();
		} else {
			$data = $this->db->get();
			$data = $data->result_array();
		}
		return $data;
	}
	function update_user($profilephoto1, $id) {
		//if a new password
		if ($profilephoto1 != "none") {
			$this->db->set('foto', $profilephoto1);
		}
		if ($this->input->post('pastech') != "password") {
			$encryptedpass = password_hash($this->input->post('pastech', TRUE), PASSWORD_BCRYPT, array("cost" => 10));
			$this->db->set('pass', $encryptedpass);
		}
		$this->db->set('nombre', $this->input->post('nombre'));
		$this->db->where('id', $id);
		$this->db->update('coreuser');
	} //end if new photo
	function add_user($data) {
		$encryptedpass = password_hash($data['pass'], PASSWORD_BCRYPT, array("cost" => 10));
		$data['pass'] = $encryptedpass;
		$this->db->insert('coreuser', $data);
		$userid = $this->db->insert_id();
		if ($userid > 0) {
			return $this->insert_cormecra($userid, $data);
		} else {
			return 0;
		}
	}
	function insert_cormecra($userid, $user_data) {
		$pdata = array(
			'email' => $user_data['email'],
			'rut' => '11111111-1',
			'nombre' => 'Rms Oficina Demo',
			'fonooficina1' => "",
			'fonooficina2' => "",
			'fonocelular' => "",
			'foto1' => $user_data['foto'],
			'dirline1' => "",
			'dirline2' => "",
			'comuna' => "",
			'ciudad' => "",
			'twitter' => "",
			'facebook' => "",
			'linkedin' => "",
			'date_created' => date('Y-m-d H:i:s'),
			'status' => 0,
		);
		$this->db->insert('coremarca', $pdata);
		$id_marca = $this->db->insert_id();
		if ($id_marca > 0) {
			$this->db->insert('corexusrmarca', array(
				'iduser' => $userid,
				'idmarca' => $id_marca,
			));
			$email_data = array(
				"fromemail" => 'notifica@barahonapropiedades.cl',
				"fromnombre" => 'Notificacion',
				"subject" => 'Bienvenido',
				"toemail" => $user_data['email'],
				"toname" => $user_data['nombre'],
				"Message" => 'Bienvenido a rms, ya puedes utilizar nuestra aplicación',
				"date_created" => date('Y-m-d H:i:s'),
			);
			$this->db->insert('emailout', $email_data);
			return $this->db->insert_id();
		} else {
			return 0;
		}
	}
	function delete_user($id, $fieldname, $tablename) {
		$this->db->delete($tablename, array($fieldname => $id));
	}
	function mail_exists($key) {
		$this->db->where('email', $key);
		$query = $this->db->get('coreuser');
		if ($query->num_rows() > 0) {
			return true;
		} else {
			return false;
		}
	}
}

/* End of file Core_user_model.php */
/* Location: ./application/models/App_user_model.php */