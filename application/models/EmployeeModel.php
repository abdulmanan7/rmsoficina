<?php

class EmployeeModel extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }
    
    function store_info($profilephoto1)
	{
		$data = array(
			'salutation' => $this->input->post('salutation', TRUE),
			'fullname' => $this->input->post('fullname', TRUE),
			'fname' => $this->input->post('fname', TRUE),
			'lname' => $this->input->post('lname', TRUE),
			'sex' => $this->input->post('sex', TRUE),
			'dob' => $this->input->post('dob', TRUE),
			'email' => $this->input->post('email', TRUE),
			'profilephoto' => $profilephoto1,
			'addl1' => $this->input->post('addl1', TRUE),
			'addl2' => $this->input->post('addl2', TRUE),
			'city' => $this->input->post('city', TRUE),
			'srp' => $this->input->post('srp', TRUE),
			'zip' => $this->input->post('zip', TRUE),
			'cntry' => $this->input->post('cntry', TRUE),
			'mobile' => $this->input->post('mobile', TRUE),
			'home' => $this->input->post('home', TRUE),
			'work' => $this->input->post('work', TRUE),
			'pob' => $this->input->post('pob', TRUE),
			'ssn-nis' => $this->input->post('ssn-nis', TRUE),
			'bir' => $this->input->post('bir', TRUE),
			'tos' => $this->input->post('tos', TRUE),
			'otherdetails' => $this->input->post('otherdetails', TRUE),
			'payrate' => $this->input->post('payrate', TRUE),
			'jobtitle' => $this->input->post('jobtitle', TRUE),
			'tertiaryschoolname' => $this->input->post('tertiaryschoolname', TRUE),
			'secondaryschoolname' => $this->input->post('secondaryschoolname', TRUE),
			'tertiarylevel' => $this->input->post('tertiarylevel', TRUE),
			'secondarylevel' => $this->input->post('secondarylevel', TRUE),
			'fullnameec' => $this->input->post('fullnameec', TRUE),
			'mobileec' => $this->input->post('mobileec', TRUE),
			'homeec' => $this->input->post('homeec', TRUE),
			'workec' => $this->input->post('workec', TRUE),
			'addec' => $this->input->post('addec', TRUE),
			'addec2' => $this->input->post('addec2', TRUE),
			'relationec' => $this->input->post('relationec', TRUE),
			'fullnameec2' => $this->input->post('fullnameec2', TRUE),
			'mobileec2' => $this->input->post('mobileec2', TRUE),
			'homeec2' => $this->input->post('homeec2', TRUE),
			'workec2' => $this->input->post('workec2', TRUE),
			'addec22' => $this->input->post('addec22', TRUE),
			'addec222' => $this->input->post('addec222', TRUE),
			'relationec2' => $this->input->post('relationec2', TRUE),
			'editorEmp' => $this->input->post('editorEmp', TRUE)
		);
    	
    	$this->db->insert('edata', $data);
    }
        
}