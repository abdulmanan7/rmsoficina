<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_model extends CI_Model {
	function get_data($username) {
		$this->db->select('*');
		$this->db->from('coreuser');
		$this->db->where('email', $username);
		$this->db->limit(1);
		$query = $this->db->get();
		return $query->result();
	}
	function register_user($data) {
		$encryptedpass = password_hash($data['pass'], PASSWORD_BCRYPT, array("cost" => 10));
		$data['pass'] = $encryptedpass;
		$this->db->insert('coreuser', $data);
		$userid = $this->db->insert_id();
		if ($userid > 0) {
			return $this->insert_cormecra($userid, $data);
		} else {
			return 0;
		}
	}
	function insert_cormecra($userid, $user_data) {
		$pdata = array(

			'email' => $user_data['email'],
			'rut' => '11111111-1',
			'nombre' => 'Rms Oficina Demo',
			'fonooficina1' => "",
			'fonooficina2' => "",
			'fonocelular' => "",
			'foto1' => "",
			'dirline1' => "",
			'dirline2' => "",
			'comuna' => "",
			'ciudad' => "",
			'twitter' => "",
			'facebook' => "",
			'linkedin' => "",
			'date_created' => date('Y-m-d H:i:s'),
			'status' => 0,
		);
		$this->db->insert('coremarca', $pdata);
		$id_marca = $this->db->insert_id();
		if ($id_marca > 0) {
			$this->db->insert('corexusrmarca', array(
				'iduser' => $userid,
				'idmarca' => $id_marca,
			));
			$email_data = array(
				"fromemail" => 'notifica@barahonapropiedades.cl',
				"fromnombre" => 'Notificacion',
				"subject" => 'Bienvenido',
				"toemail" => $user_data['email'],
				"toname" => "demo",
				"Message" => 'Bienvenido a rms, ya puedes utilizar nuestra aplicación',
				"date_created" => date('Y-m-d H:i:s'),
			);
			$this->db->insert('emailout', $email_data);
			return $this->db->insert_id();
		} else {
			return 0;
		}
	}
	function mail_exists($key) {
		$this->db->where('email', $key);
		$query = $this->db->get('coreuser');
		if ($query->num_rows() > 0) {
			return true;
		} else {
			return false;
		}
	}
	function validate($username, $password) {
		$this->db->select('*');
		$this->db->from('coreuser');
		$this->db->where('email', $username);
		$this->db->limit(1);

		$query = $this->db->get();

		$data = $query->result_array(); //create an array

		foreach ($data as $field) {
			$pass = $field['pass'];
		}

		if ($query->num_rows() == 1) {

			if (password_verify($password, $pass)) {
				return $query->result();
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	function edit_user_info($profilephoto1) {
		if (strcmp($profilephoto1, "none") == 0) {
//if no new photo
			if (strcmp($this->input->post('pass'), "nochange") != 0) {
//if no new password
				$encryptedpass = password_hash($this->input->post('pass', TRUE), PASSWORD_BCRYPT, array("cost" => 10));

				$data = array(
					'pass' => $encryptedpass,
				);

				$this->db->set($data);
				$this->db->where('id', $this->input->post('id', TRUE));
				$this->db->update('coreuser');
			} //end if no new password
		} //end if no new photo
		else {
//new photo
			if (strcmp($this->input->post('pass'), "nochange") == 0) {
//if no new password
				$data = array(
					'foto' => $profilephoto1,
				);

				$this->db->set($data);
				$this->db->where('id', $this->input->post('id', TRUE));
				$this->db->update('coreuser');
			} //end if no new password
			else {
//if a new password
				$encryptedpass = password_hash($this->input->post('pass', TRUE), PASSWORD_BCRYPT, array("cost" => 10));

				$data = array(
					'foto' => $profilephoto1,
					'pass' => $encryptedpass,
				);

				$this->db->set($data);
				$this->db->where('id', $this->input->post('id', TRUE));
				$this->db->update('coreuser');
			} //end if new password
		} //end if new photo
	}
}

/* End of file Login_model.php */
/* Location: ./application/models/Login_model.php */