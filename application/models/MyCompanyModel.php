<?php

class MyCompanyModel extends CI_Model {

	function __construct()
	{
		parent::__construct();
	}


	//Store company data
	function store_comp_info($photo)
	{		
		$pdata = array(
			'email' => $_SESSION['email'],
			'rut' => $this->input->post('rut'),
			'nombre' => $this->input->post('nombre'),
			'fonooficina1' => $this->input->post('fonooficina1'),
			'fonooficina2' => $this->input->post('fonooficina2'),
			'fonocelular' => $this->input->post('fonocelular'),
			'foto1' => strcmp($photo,"none")==0?"blank.png":$photo,
			'dirline1' => $this->input->post('dirline1'),
			'dirline2' => $this->input->post('dirline2'),
			'comuna' => $this->input->post('comuna'),
			'ciudad' => $this->input->post('ciudad'),
			'twitter' => $this->input->post('twitter'),
			'facebook' => $this->input->post('facebook'),
			'linkedin' => $this->input->post('linkedin'),
			'date_created' => date('Y-m-d H:i:s'),
			'status' => 0,
			);

		$sql = "SELECT * FROM coremarca where email = '".$_SESSION['email']."'"; 
		$result = $this->db->query($sql);
		
		if($result->num_rows() == 0)//if no data exists then create a new row
		{
			$this->db->insert('coremarca', $pdata);
			$id_marca = $this->db->insert_id();
			if ($id_marca>0) {
				$this->db->insert('corexusrmarca', array(
				'iduser' => $_SESSION['userid'],
				'idmarca' => $id_marca,
			));
			}
		}
		else //if data exists then update the first row
		{

			$this->db->where('email', $_SESSION['email']);
			$this->db->update('coremarca',$pdata);
   			// $this->db->delete('coremarca'); 
			// $this->db->insert('coremarca', $data);
		}
	}

	//Update company data
	function update_comp_info($photo)
	{

		if(strcmp($photo,"none")==0){//if no new photo
			$pdata = array(
				'email' => $_SESSION['email'],
				'rut' => $this->input->post('rut'),
				'nombre' => $this->input->post('nombre'),
				'fonooficina1' => $this->input->post('fonooficina1'),
				'fonooficina2' => $this->input->post('fonooficina2'),
				'fonocelular' => $this->input->post('fonocelular'),
				'dirline1' => $this->input->post('dirline1'),
				'dirline2' => $this->input->post('dirline2'),
				'comuna' => $this->input->post('comuna'),
				'ciudad' => $this->input->post('ciudad'),
				'twitter' => $this->input->post('twitter'),
				'facebook' => $this->input->post('facebook'),
				'linkedin' => $this->input->post('linkedin'),
				'date_created' => date('Y-m-d H:i:s'),
				'status' => 0,
				);

			$sql = "SELECT * FROM coremarca where email = '".$_SESSION['email']."'"; 
			$result = $this->db->query($sql);

		if($result->num_rows() == 0)//if no data exists then create a new row
		{
			$this->db->insert('coremarca', $pdata);
			$id_marca = $this->db->insert_id();
			if ($id_marca>0) {
				$this->db->insert('corexusrmarca', array(
				'iduser' => $_SESSION['userid'],
				'idmarca' => $id_marca,
			));
			}
		}
		else //if data exists then update the first row
		{
			$this->db->where('email', $_SESSION['email']);
			$this->db->update('coremarca', $pdata);
		}//end if no new photo
	}
	else
		{//new photo
			$pdata = array(
				'email' => $_SESSION['email'],
				'rut' => $this->input->post('rut'),
				'nombre' => $this->input->post('nombre'),
				'fonooficina1' => $this->input->post('fonooficina1'),
				'fonooficina2' => $this->input->post('fonooficina2'),
				'fonocelular' => $this->input->post('fonocelular'),
				'foto1' => $photo,
				'dirline1' => $this->input->post('dirline1'),
				'dirline2' => $this->input->post('dirline2'),
				'comuna' => $this->input->post('comuna'),
				'ciudad' => $this->input->post('ciudad'),
				'twitter' => $this->input->post('twitter'),
				'facebook' => $this->input->post('facebook'),
				'linkedin' => $this->input->post('linkedin'),
				'date_created' => date('Y-m-d H:i:s'),
				'status' => 0,
				);

			$sql = "SELECT * FROM coremarca where email = '".$_SESSION['email']."'"; 
			$result = $this->db->query($sql);

		if($result->num_rows() == 0)//if no data exists then create a new row
		{
			$this->db->insert('coremarca', $pdata);
			$id_marca = $this->db->insert_id();
			if ($id_marca>0) {
				$this->db->insert('corexusrmarca', array(
				'iduser' => $_SESSION['userid'],
				'idmarca' => $id_marca,
			));
			}
		}
		else //if data exists then update the first row
		{
			$this->db->where('email', $_SESSION['email']);
			$this->db->update('coremarca', $pdata);
		}
	}
}

}