<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Upload_model extends CI_Model {
	protected $table = "corefile";
	function get_id_marca() {
		return $this->db->get($this->table, 1, 0)->row_array();
	}
	public function save_uploaded_file($data) {
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}
	public function get_root($term) {
		$this->db->select('deslarga')->from('corezmant');
		$this->db->where('cod', $term);
		return $this->db->get()->row_array();
	}

}

/* End of file Upload_model.php */
/* Location: ./application/models/Upload_model.php */