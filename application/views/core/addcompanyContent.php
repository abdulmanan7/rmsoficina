<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
        <?php 
			if ( !isset($message)){}
			else{ echo "<h3 style=\"color:green; text-align: center;\">".$message."</h3>"; }
	  	?>
          <h1>
            Add Company Data
            <small>Please fill out the form carefully.</small>
          </h1>
        </section>
        <!-- Main content -->
        <section class="content">
        <?php echo form_open_multipart('addCompData');?>
        
        <!-- Basic Information -->
         <div class="row">
            <div class="col-md-12">
              <div class="box box-info">
                <div class="box-header">
                  <h3 class="box-title">Información Basica</h3>
                  <!-- tools box -->
                  <div class="pull-right box-tools">
                    <button class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                  </div><!-- /. tools -->
                </div><!-- /.box-header -->
                <div class="box-body pad">
                        <div class="col-md-5">
                                
                                <div class="form-group">
                                  <input type="text" class="form-control" name="nombre" id="nombre" value="" placeholder="Nombre">
                                </div>
                                <div class="form-group">
                                  <input type="text" class="form-control" disabled="disabled" name="email" id="email1" value="<?=$_SESSION['email']?>" placeholder="Email">
                                </div>
                                <div class="form-group">
                                  <input type="text" class="form-control" name="rut" id="rut" value="" placeholder="RUT">
                                </div>
                        </div>
                        <div class="col-sm-offset-1 col-md-5">
                                <div class="form-group">
                                  <label for="upload_file1">Upload Oficina Foto</label>
                                  <input type="file" name="userfile" id="userfile1" readonly="true" />
                                  <p class="help-block"></p>
                                </div>
                        </div>
                </div>
              </div><!-- /.box -->
            </div><!-- /.col-->
          </div><!-- ./row -->

     <!-- Address and Contact Information -->
           <div class="row">
            <div class="col-md-12">
              <div class="box box-success">
                <div class="box-header">
                  <h3 class="box-title">Dirección e información de contacto</h3>
                  <!-- tools box -->
                  <div class="pull-right box-tools">
                    <button class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                  </div><!-- /. tools -->
                </div><!-- /.box-header -->
                    <div class="box-body pad">
                        <div class="col-md-5">
                            <div class="form-group">
                              <input type="text" class="form-control" name="dirline1" id="add1" value="" placeholder="dir Line 1">
                            </div>
                            <div class="form-group">
                              <input type="text" class="form-control" name="dirline2" id="add2" value="" placeholder="dir Line 2">
                            </div>
                            <div class="form-group">
                              <input type="text" class="form-control" name="comuna" id="cumuna" value="" placeholder="comuna">
                            </div>
                            <div class="form-group">
                              <input type="text" class="form-control" name="ciudad" id="ciudad" value="" placeholder="Ciudad">
                            </div>
                        </div>
                        <div class="col-sm-offset-1 col-md-5">
                        <h3>Información de Contacto</h3>
                            <div class="form-group">
                                    <div class="input-group">
                                      <div class="input-group-addon">
                                          <i class="fa fa-mobile"></i>
                                      </div>
                                      <input type="text" class="form-control" name="fonocelular" id="mobile1" value="" placeholder="Mobile Number">
                                    </div><!-- /.input group -->
                            </div><!-- /.form group -->
                            <div class="form-group">
                                    <div class="input-group">
                                      <div class="input-group-addon">
                                          <i class="fa fa-home"></i>
                                      </div>
                                      <input type="text" class="form-control" name="fonooficina1" id="fonooficina1" value="" placeholder="Home Number">
                                    </div><!-- /.input group -->
                            </div><!-- /.form group -->
                            <div class="form-group">
                                    <div class="input-group">
                                      <div class="input-group-addon">
                                        <i class="fa fa-suitcase"></i>
                                      </div>
                                      <input type="text" class="form-control" name="fonooficina2" id="fonooficina2" value="" placeholder="Work Number">
                                    </div><!-- /.input group -->
                            </div><!-- /.form group -->
                            <div class="form-group">
                                    <div class="input-group">
                                      <div class="input-group-addon">
                                        <i class="fa fa-facebook"></i>
                                      </div>
                                       <input type="text" class="form-control" name="facebook" id="facebook" value="" placeholder="facebook">
                                    </div><!-- /.input group -->
                            </div><!-- /.form group -->
                             <div class="form-group">
                                    <div class="input-group">
                                      <div class="input-group-addon">
                                        <i class="fa fa-twitter"></i>
                                      </div>
                                      <input type="text" class="form-control" name="twitter" id="twitter" value="" placeholder="Twitter">
                                    </div><!-- /.input group -->
                            </div><!-- /.form group -->
                            <div class="form-group">
                                    <div class="input-group">
                                      <div class="input-group-addon">
                                          <i class="fa fa-linkedin"></i>
                                      </div>
                                      <input type="text" class="form-control" name="linkedin" id="linkedin" value="" placeholder="Linked in">
                                    </div><!-- /.input group -->
                            </div><!-- /.form group -->
                        </div>
                </div>
              </div><!-- /.box -->
            </div><!-- /.col-->
          </div><!-- ./row -->
          
          
          <div class="row">
            <div class="col-md-12">
              <div class="box box-alert">
                <div class="box-header">
                  <h3 class="box-title">Otros Detalles</h3>
                  <!-- tools box -->
                  <div class="pull-right box-tools">
                    <button class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                  </div><!-- /. tools -->
                </div><!-- /.box-header -->
                <!-- <div class="box-body pad">
                    <div class="form-group">
                        <textarea id="editorEmp1" name="editorEmp" class="form-control" rows="10">
                           <?php //echo $comp['editorEmp']; ?>
                        </textarea>
                    </div>
                </div> -->
              </div><!-- /.box -->
            </div><!-- /.col-->
          </div><!-- ./row -->
        <button type="submit"  value="upload" class="btn btn-block btn-primary btn-lg">Grabar Datos Oficina</button>
        </form>
        </section><!-- /.content -->
      </div>