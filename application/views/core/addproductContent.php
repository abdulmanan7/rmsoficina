<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
        <?php
			if ( !isset($message)){}
			else{ echo "<h3 style=\"color:green; text-align: center;\">".$message."</h3>"; }
	  	?>
          <h1>
            Add Product
            <small>Please fill out the form carefully.</small>
          </h1>
        </section>
        <!-- Main content -->
        <section class="content">
        <?php echo form_open_multipart('addProductData');?>
        
        <!-- Basic Information -->
        <div class="row">
            <div class="col-md-12">
              <div class="box box-success">
                <div class="box-header">
                  <h3 class="box-title">Basic Product Information</h3>
                  <!-- tools box -->
                  <div class="pull-right box-tools">
                    <button class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                  </div><!-- /. tools -->
                </div><!-- /.box-header -->
                <div class="box-body pad">
                        <div class="col-md-5">
                                <div class="form-group">
                                  <input type="text" class="form-control" name="sku" id="sku1" title="SKU - A unique idetifier for each product." placeholder="SKU">
                                </div>
                                <div class="form-group">
                                  <input type="text" class="form-control" name="prodname" id="prodname1" placeholder="Product Name">
                                </div>
                                <div class="form-group">
                                <label for="currency">Currency</label>
                                  <select class="form-control" name="currency">
                                    <option value="USD$">United States, Dollar USD$</option>
                                    <option value="TTD$">Trinidad and Tobago, Dollar TTD$</option>
                                    <option value="CAD$">Canada, Dollar CAD$</option>
									<option value="€">Euro Member, Dollar €</option>
                                  </select>
                                </div>
                                <div class="form-group">
                                  <input type="text" class="form-control" name="price" id="price1" placeholder="Regular Price (exclude the currency symbol)">
                                </div>
                                <div class="form-group">
                                  <input type="text" class="form-control" name="saleprice" id="saleprice1" placeholder="Sale Price (exclude the currency symbol)">
                                </div>
                                <div class="form-group">
                                  <select class="form-control" name="stock">
                                    <option value="In Stock">In Stock</option>
                                    <option value="Out of Stock">Out of Stock</option>
                                  </select>
                                </div>
                                <div class="form-group">
                                <label for="qty">Stock Quantity</label>
                                  <input type="text" class="form-control" name="stkqty" id="stkqty1" value="0" title="0: None, -1: Unlimited">
                                </div> 
                        </div>
                        <div class="col-sm-offset-1 col-md-5">                                   
                               	<div class="form-group">
                                <label for="cat">Main Category</label>
                                  <select class="form-control" name="cat">
                                  <?php
								  	//Retrieve categories
										$sql = "SELECT cat FROM pcategories"; 
										$qry = $this->db->query($sql);
											
										foreach ($qry->result() as $row)
										{
										   echo '<option value = "'.$row->cat.'">'.$row->cat.'</option>';
										}
									?>
                                  </select>
                                </div>
                                <div class="form-group">
                                <label for="sub1">Sub Category 1</label>
                                  <select class="form-control" name="sub1">
                                  <option value="None">None</option>
                                  <?php
								  	//Retrieve categories
										$sql = "SELECT sub1 FROM pcategories"; 
										$qry = $this->db->query($sql);
											
										foreach ($qry->result() as $row)
										{
										   if($row->sub1 != "") echo '<option value = "'.$row->sub1.'">'.$row->sub1.'</option>';
										}
									?>
                                  </select>
                                </div>
                                <div class="form-group">
                                <label for="sub2">Sub Category 2</label>
                                  <select class="form-control" name="sub2">
                                  <option value="None">None</option>
                                  <?php
								  	//Retrieve categories
										$sql = "SELECT sub2 FROM pcategories"; 
										$qry = $this->db->query($sql);
											
										foreach ($qry->result() as $row)
										{
										   if($row->sub2 != "") echo '<option value = "'.$row->sub2.'">'.$row->sub2.'</option>';
										}
									?>
                                  </select>
                                </div>
                                <div class="form-group">
                                <label for="sub3">Sub Category 3</label>
                                  <select class="form-control" name="sub3">
                                  <option value="None">None</option>
                                  <?php
								  	//Retrieve categories
										$sql = "SELECT sub3 FROM pcategories"; 
										$qry = $this->db->query($sql);
											
										foreach ($qry->result() as $row)
										{
										   if($row->sub3 != "") echo '<option value = "'.$row->sub3.'">'.$row->sub3.'</option>';
										}
									?>
                                  </select>
                                </div>
                                <div class="form-group">
                                <label for="sub4">Sub Category 4</label>
                                  <select class="form-control" name="sub4">
                                  <option value="None">None</option>
                                  <?php
								  	//Retrieve categories
										$sql = "SELECT sub4 FROM pcategories"; 
										$qry = $this->db->query($sql);
											
										foreach ($qry->result() as $row)
										{
										   if($row->sub4 != "") echo '<option value = "'.$row->sub4.'">'.$row->sub4.'</option>';
										}
									?>
                                  </select>
                                </div>
                        </div>
                </div>
              </div><!-- /.box -->
            </div><!-- /.col-->
          </div><!-- ./row -->          
          
          <div class="row">
            <div class="col-md-12">
              <div class="box box-info">
                <div class="box-header">
                  <h3 class="box-title">Product Images</h3>
                  <!-- tools box -->
                  <div class="pull-right box-tools">
                    <button class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                  </div><!-- /. tools -->
                </div><!-- /.box-header -->
                <div class="box-body pad">
                    <div class="form-group">
                      <label for="upload_file1">Upload product photo 1</label>
                      <input type="file" name="userfile1" id="userfile1" readonly="true" />
                      <p class="help-block"></p>
                    </div>
                    <div class="form-group">
                      <label for="upload_file1">Upload product photo 2</label>
                      <input type="file" name="userfile2" id="userfile2" readonly="true" />
                      <p class="help-block"></p>
                    </div>
                    <div class="form-group">
                      <label for="upload_file1">Upload product photo 3</label>
                      <input type="file" name="userfile3" id="userfile3" readonly="true" />
                      <p class="help-block"></p>
                    </div>
                </div>
              </div><!-- /.box -->
            </div><!-- /.col-->
          </div><!-- ./row -->
          
          <div class="row">
            <div class="col-md-12">
              <div class="box box-alert">
                <div class="box-header">
                  <h3 class="box-title">Product Description</h3>
                  <!-- tools box -->
                  <div class="pull-right box-tools">
                    <button class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                  </div><!-- /. tools -->
                </div><!-- /.box-header -->
                <div class="box-body pad">
                    <div class="form-group">
                        <textarea id="editorEmp1" name="editorEmp" class="form-control" rows="10">
                            <details>
                               <summary>Product Features</summary>
                                  <ul>
                                    <li>8 mega pixel camera with full 1080p video recording</li>
                                    <li>Siri voice assitant</li>
                                    <li>iCloud</li>
                                    <li>Air Print</li>
                                    <li>Retina display</li>
                                    <li>Photo and video geotagging</li>
                                  </ul>
                             </details>
                        </textarea>
                    </div>
                </div>
              </div><!-- /.box -->
            </div><!-- /.col-->
          </div><!-- ./row -->
                  
        <button type="submit"  value="upload" class="btn btn-block btn-primary btn-lg">Add Product</button>
        
        </form>
        </section><!-- /.content -->
      </div>