<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <?php
if (!isset($message)) {} else {echo "<h3 style=\"color:green; text-align: center;\">" . $message . "</h3>";}
?>
    <h1>
      Add New User
      <small>Please fill out the form carefully.</small>
    </h1>
  </section>
  <!-- Main content -->
  <section class="content">
    <?php echo form_open_multipart('addCoreUser');?>

    <!-- Basic Information -->
    <div class="row">
      <div class="col-md-12">
        <div class="box box-info">
          <div class="box-header">
            <h3 class="box-title">User Form</h3>
            <!-- tools box -->
            <div class="pull-right box-tools">
              <button class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
            </div><!-- /. tools -->
          </div><!-- /.box-header -->
          <div class="box-body pad">
            <div class="col-md-5">
              <div class="form-group">
                <label for="upload_file1">Upload Staff Photo</label>
                <input type="file" name="userfile" id="userfile1" readonly="true" />
                <p class="help-block"></p>
              </div>
            </div>
            <div class="col-sm-offset-1 col-md-5">
              <div class="form-group">
                <label for="usrnm">Username</label>
                <input type="text" class="form-control" name="usrnm" id="usrnm1" placeholder="Username: myName123" required>
              </div>
              <div class="form-group">
                <label for="ut">User Type (Permission Type)</label>
                <select class="form-control" name="ut">
                  <option value="Customer">Customer</option>
                  <option value="Administrator">Administrator</option>
                  <option value="Staff">Staff</option>
                  <option value="Product Manager">Product Manager</option>
                  <option value="HR Manager">HR Manager</option>
                </select>
              </div>

              <div class="form-group">
                <label for="email">Email</label>
                <input type="email" class="form-control" name="email" id="email" placeholder="Email" required>
              </div>
              <div class="form-group">
                <label for="pastech">Password (Password used to log into the application)</label>
                <input type="password" class="form-control" name="pastech" id="pastech1" placeholder="Password: myPass123" required>
              </div>

            </div>
          </div>
        </div><!-- /.box -->
      </div><!-- /.col-->
    </div><!-- ./row -->

    <button type="submit"  value="upload" class="btn btn-block btn-primary btn-lg">Add New User</button>

  </form>
</section><!-- /.content -->
</div>