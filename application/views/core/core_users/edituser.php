<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Editar Usuario
            <small>Please only change fields that you want to change.</small>
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <?php echo form_open_multipart('editCoreuser/' . $id);?>
        <!-- Basic Information -->
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                    <div class="box-header">
                        <h3 class="box-title">Editar Formulario Usuario</h3>
                        <!-- tools box -->
                        <div class="pull-right box-tools">
                            <button class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        </div><!-- /. tools -->
                    </div><!-- /.box-header -->
                    <div class="box-body pad">
                        <div class="col-md-5">
                            <div class="form-group">
                                <input class="hidden" name="id" value="<?php echo $id;?>" />
                                <label for="upload_file1">Subir Foto</label>
                                <input type="file" name="userfile" id="userfile1" readonly="true" />
                                <p class="help-block"></p>
                            </div>
                        </div>
                        <div class="col-sm-offset-1 col-md-5">
                            <div class="form-group">
                                <label for="nombre">Nombre</label>
                                <input type="text" class="form-control" name="nombre" value="<?=$user['nombre']?>">
                            </div>
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input disabled="disabled" type="text" class="form-control" name="email" value="<?=$user['email']?>">
                            </div>
                            <div class="form-group">
                                <label for="pastech">Password</label>
                                <input type="password" class="form-control" name="pastech" value="none" required>
                            </div>
                        </div>
                    </div>
                </div><!-- /.box -->
                <div class="box box-info">
                    <div class="box-header">
                        <h3 class="box-title">Detalles Oficina</h3>
                        <!-- tools box -->
                        <div class="pull-right box-tools">
                            <button class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        </div><!-- /. tools -->
                    </div><!-- /.box-header -->
                    <div class="box-body pad">
                        <div class="box box-widget widget-user">
                         <?php
$sql = "SELECT * FROM coremarca where id = $id";
$this->db->select();
$this->db->from('coremarca cm');
$this->db->join('corexusrmarca cx', 'cm.id = cx.idmarca');
$this->db->where('cx.iduser', $id);
$data = $this->db->get();
$comp_data = $data->row_array();
// print_r($this->db->last_query());
// die;
?>
          <?php if ($comp_data || (isset($comp_data['email']) && strlen($comp_data['email']) > 4)): ?>
                            <!-- Add the bg color to the header using any of the bg-* classes -->
                            <div class="widget-user-header bg-aqua-active">
                              <h3 class="widget-user-username"><?php echo $comp_data['nombre'];?></h3>
                              <h5 class="widget-user-desc"><?php echo $comp_data['rut'];?></h5>
                              <h5 class="widget-user-desc"><?php echo $comp_data['email'];?></h5>
                              <h5 class="widget-user-desc"><?php echo $comp_data['comuna'];?></h5>
                          </div>
                          <div class="widget-user-image">
                              <?php
$img_avatar = ($comp_data['foto1'] == "" || $comp_data['foto1'] == NULL) ? "blank.png" : $comp_data['foto1'];
echo img(array('src' => "upload/e_kdjhawkdhawjkhd/" . $img_avatar, 'class' => 'img-circle img-responsive'));
?>
                          </div>
                          <div class="box-footer">
                              <div class="row">
                                <div class="col-sm-4 border-right">
                                  <div class="description-block">
                                    <h5 class="description-header">Dirección</h5>
                                    <span class="description-text"><?php echo $comp_data['dirline1'];?></span><br>
                                    <span class="description-text"><?php echo $comp_data['dirline2'];?></span><br><br>

                                    <h5 class="description-header">Ciudad</h5>
                                    <span class="description-text"><?php echo $comp_data['ciudad'];?></span><br><br>

                                </div><!-- /.description-block -->
                            </div><!-- /.col -->
                            <div class="col-sm-4 border-right">
                              <div class="description-block">
                                <h5 class="description-header">Contact</h5>
                                <span class="description-text"><i class="fa fa-mobile"></i> <?php echo $comp_data['fonocelular'];?></span><br>
                                <span class="description-text"><i class="fa fa-home"></i> <?php echo $comp_data['fonooficina1'];?></span><br>
                                <span class="description-text"><i class="fa fa-suitcase"></i> <?php echo $comp_data['fonooficina2'];?></span><br>
                            </div><!-- /.description-block -->
                        </div><!-- /.col -->
                        <div class="col-sm-4">
                          <div class="description-block">
                            <h5 class="description-header">Facebook</h5>
                            <span class="description-text"><i class="fa fa-facebook"></i> <?php echo $comp_data['facebook'];?></span><br><br>

                            <h5 class="description-header">Twitter</h5>
                            <span class="description-text"><i class="fa fa-twitter"></i> <?php echo $comp_data['twitter'];?></span><br>
                            <h5 class="description-header">LinkedIn</h5>
                            <span class="description-text"><i class="fa fa-linkedin"></i> <?php echo $comp_data['linkedin'];?></span><br>
                        </div><!-- /.description-block -->
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div>
        </div><!-- /.widget-user -->
        <?=anchor('editcomp/' . $comp_data['id'], '<b>Editar</b>', array('class' => 'btn btn-info btn-flat'));?>
        <?php else: ?>
  <div class="alert alert-info">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <strong>No Record found !</strong> please click add new to create company info....
  </div>
    <?=anchor('addCompany', '<b>Add New</b>', array('class' => 'btn btn-info btn-flat'));?>
<?php endif;?>
    </div>
</div><!-- /.box -->
</div><!-- /.col-->
</div><!-- ./row -->
<button type="submit"  value="upload" class="btn btn-block btn-primary btn-lg">Grabar Cambios</button>
</form>
</section><!-- /.content -->
</div>