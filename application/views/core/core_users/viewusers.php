<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Users
            <small>Below are users who can access the application.</small>
          </h1>
        </section>
        <!-- Main content -->
        <section class="content">
        <?php
if (!isset($message)) {} else {echo "<h3 style=\"color:green; text-align: center;\">" . $message . "</h3>";}
?>
        <!-- Basic Information -->
        <div class="row">
            <div class="col-md-12">
               <div class="box">
                <div class="box-body table-responsive no-padding">
                  <table class="table table-hover">

                    <tr>
                      <th>Username</th>
                      <th>User Type</th>
                      <th>Email</th>
                      <th>Option</th>
                    </tr>
                    <?php

foreach ($users as $field) {
	?>
                    <tr>
                      <td><?php echo $field['nombre'];?></td>
                      <td><?php echo $field['ut'];?></td>
                      <td><?php echo $field['email'];?></td>
                      <td><?php
echo anchor('editCoreuser/' . $field['id'] . '', '<b>Edit</b>', array('class' => 'btn btn-info btn-flat'));?>
                      <?php
if (strcmp($field['nombre'], 'superadmin') != 0) {
		?>
                      <a class="btn btn-danger btn-flat" data-toggle="modal" data-target="#myModal_<?php echo $field['id'];?>">Delete</a>
                      <!-- Modal -->
                        <div class="modal fade" id="myModal_<?php echo $field['id'];?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                          <div class="modal-dialog" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Deleting a user.</h4>
                              </div>
                              <div class="modal-body">
                                Are you sure you want to delete this user? Once deleted it cannot be undone.
                              </div>
                              <div class="modal-footer">
                               <?php echo anchor('deleteCoreuser/' . $field['id'] . '', '<b>Delete</b>', array('class' => 'btn btn-danger btn-flat'));?>
                               <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                              </div>
                            </div>
                          </div>
                        </div><!-- End modal -->
                        <?php
}
	?>
                      </td>
                    </tr>
                    <?php
}
?>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col-->
          </div><!-- ./row -->
        </section><!-- /.content -->
      </div>