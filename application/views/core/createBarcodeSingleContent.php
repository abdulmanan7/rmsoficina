<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
        <?php
			if ( !isset($message)){}
			else{ echo "<h3 style=\"color:green; text-align: center;\">".$message."</h3>"; }
	  	?>
          <h1>
            Create Barcodes
            <small>Please follow all guidelines.</small>
          </h1>
        </section>
        <!-- Main content -->
        <section class="content">
		    <!-- Basic Information -->
        <div class="row">
            <div class="col-md-12">
              <div class="box box-success">
                <div class="box-header">
                  <h3 class="box-title">Barcode Generator</h3>
                  <!-- tools box -->
                  <div class="pull-right box-tools">
                    <button class="btn btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                  </div><!-- /. tools -->
                </div><!-- /.box-header -->
                <div class="box-body pad">
                  <div class="col-md-12">

                    <form action="<?=base_url('createBarcodeImage2')?>" method="GET" name="barcode-form">
                        <div class="col-md-4">
                          <div class="form-group">
            							  <label for="text">Enter text to be converted to a barcode below (code128):</label><br>
                            <input type="text" class="form-control" id="text" name="text" placeholder="mybarcode3456" />
            							</div>
                          <div class="form-group">
                            <label for="orientation">Orientation:</label><br>
                            <select class="form-control" name="orientation">
                                <option value="horizontal">Horizontal</option>
                                <option value="vertical">Vertical</option>
                            </select>
                          </div>
                        </div>
                      <button type="submit" class="btn btn-block btn-success btn-lg">Create Barcode</button>
                    </form>
                  </div>
                </div>
				<hr>
				<div class="box-body pad">
						<div class="col-md-12">
              <div class="box-header2">
  						  <h3 class="box-title">All Saved Barcodes</h3>
              </div>
  						<p>You can print or delete a barcode below.</p>
              <br>
            <table id="example1" class="table table-hover" cellspacing="0" width="100%">
              <thead><tr><th>Barcode</th><th>Options</th></tr></thead>
              <tbody>
                <?php
                $sql = "SELECT * FROM barcode2";
                $data = $this->db->query($sql);
                $data = $data->result_array();
                foreach ($data as $field)
                {
                ?>

                <tr>
                  <td><div id="printableArea_<?php echo $field['barcodeId']; ?>">
                      <?php
                      echo img(array('src' => "upload/barcodes/single/".$field['imageName'].".png",'class'=>'img-responsive'));
                      ?>
                      </div>
                  </td>
                  <td>
                    <button type="button" onclick="printDiv('printableArea_<?php echo $field['barcodeId']; ?>')" class="btn btn-info" ><i class="fa fa-print"></i></button>
                    <a class="btn btn-danger btn-flat" data-toggle="modal" data-target="#myModal_<?php echo $field['barcodeId']; ?>"><i class="fa fa-times"></i></a>
        					  <!-- Modal -->
                    <div class="modal fade" id="myModal_<?php echo $field['barcodeId']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Deleting Label</h4>
                          </div>
                          <div class="modal-body">
                            <i class="fa fa-exclamation-triangle"  style="color:red"></i> Are you sure you want to delete this label? Once deleted it cannot be undone.
                          </div>
                          <div class="modal-footer">
                           <?php echo anchor( 'deleteBarcode/'.$field['barcodeId'].'', '<b>Delete</b>', array( 'class'=>'btn btn-danger btn-flat')); ?>
                           <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                          </div>
                        </div>
                      </div>
                    </div><!-- End modal -->
                  </td>
                </tr>

                <?php
                }
                ?>
                  </tbody>
                </table>
						</div>
          </div>



          <script>
            function printDiv(divName)
            {
                 var printContents = document.getElementById(divName).innerHTML;
                 var originalContents = document.body.innerHTML;
                 document.body.innerHTML = printContents;
                 window.print();
                 document.body.innerHTML = originalContents;
            }
          </script>

<br>
              </div><!-- /.box -->
            </div><!-- /.col-->
          </div><!-- ./row -->
        </section><!-- /.content -->
      </div>
