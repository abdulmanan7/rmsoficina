<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Dashboard
           <?php echo 'sesion ut' .$_SESSION['ut']; ?>
          </h1>
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-green">
                <div class="inner">
                <?php
                  	$sql = "SELECT * FROM edata";
          					$data = $this->db->query($sql);

          					if($data->num_rows() == 0){	$empNum = $data->num_rows(); }
          					else {	$empNum = $data->num_rows(); }
          				?>
                  <h3><?php echo $empNum ?></h3>
                  <p><?php if($empNum==1) echo "Employee"; else echo "Employees"; ?></p>
                </div>
                <div class="icon">
                  <i class="ion ion-ios-people"></i>
                </div>
                <!--<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>-->
              </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-purple">
                <div class="inner">
                <?php
                  	$sql = "SELECT * FROM pdata";
					$data = $this->db->query($sql);

					if($data->num_rows() == 0){	$prodnum = $data->num_rows(); }
					else {	$prodnum = $data->num_rows(); }
				?>
                  <h3><?php echo $prodnum ?></h3>
                  <p><?php if($prodnum==1) echo "Product"; else echo "Products"; ?></p>
                </div>
                <div class="icon">
                  <i class="ion ion-android-cart"></i>
                </div>
                <!--<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>-->
              </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-orange">
                <div class="inner">
                <?php
                  	$sql = "SELECT * FROM cldata";
					$data = $this->db->query($sql);

					if($data->num_rows() == 0){	$clnum = $data->num_rows(); }
					else {	$clnum = $data->num_rows(); }
				?>
                  <h3><?php echo $clnum ?></h3>
                  <p><?php if($clnum==1) echo "Client"; else echo "Clients"; ?></p>
                </div>
                <div class="icon">
                  <i class="ion ion-person-add"></i>
                </div>
                <!--<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>-->
              </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-aqua">
                <div class="inner">
				<?php
                  	$sql = "SELECT * FROM appuser";
					$data = $this->db->query($sql);

					if($data->num_rows() == 0){	$appnum = $data->num_rows(); }
					else {	$appnum = $data->num_rows(); }
				?>
                  <h3><?php echo $appnum ?></h3>
                  <p><?php if($appnum==1) echo "User"; else echo "Users"; ?></p>
                </div>
                <div class="icon">
                  <i class="ion ion-pie-graph"></i>
                </div>
                <!--<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>-->
              </div>
            </div><!-- ./col -->
          </div><!-- /.row -->
          <!-- Main row -->
          <div class="row">
            <!-- Left col -->
            <section class="col-lg-12">

            <?php
			if(strcmp($_SESSION['ut'],'Product Manager')==0 || strcmp($_SESSION['ut'],'Administrator')==0 || strcmp($_SESSION['ut'],'Staff') ==0)
			{
			?>
              <!-- PRODUCT LIST -->
              <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title">Recently Added Products</h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <ul class="products-list product-list-in-box">
                    <?php
						$sql = "SELECT * FROM pdata ORDER BY prodId DESC LIMIT 4";
						$data = $this->db->query($sql);

						if($data->num_rows() == 0){	$prodnum2 = $data->num_rows(); }
						else {	$prodnum2 = $data->num_rows(); }

						$data = $data->result_array(); //create an array
					?>
                    <?php
                    foreach ($data as $field)
					{

					?>

                    <li class="item">
                      <div class="product-img">
                        <?php
					  			if($field['file_name1'] != NULL) echo img(array('src' => "upload/p_dfgdfgdfgjkhd/".$field['file_name1'],'class'=>''));
					  			else if($field['file_name2'] != NULL) echo img(array('src' => "upload/p_dfgdfgdfgjkhd/".$field['file_name2'],'class'=>''));
					  			else if($field['file_name3'] != NULL) echo img(array('src' => "upload/p_dfgdfgdfgjkhd/".$field['file_name3'],'class'=>''));
								else echo img(array('src' => 'assets/images/blank.png','class'=>''));
					  ?>
                      </div>
                      <div class="product-info">
                      <?php echo anchor( 'viewspecificprod/'.$field['prodId'].'', ''.$field['prodname'].' <span class="label label-info pull-right">'.$field['currency'].$field['price'].'</span>', array( 'class'=>'product-title')); ?>
                        <span class="product-description">
                          <?php echo 'Status: '.$field['stock'].'<br>Category: '.$field['cat']; ?>
                        </span>
                      </div>
                    </li><!-- /.item -->
                    <?php
					}
					?>
                  </ul>
                </div><!-- /.box-body -->
                <div class="box-footer text-center">
                <?php echo anchor( 'viewproducts', 'View All Products', array( 'class'=>'uppercase')); ?>
                </div><!-- /.box-footer -->
              </div><!-- /.box -->
            <?php
			}
			?>

              <!-- USERS LIST -->
              <div class="box box-danger">
                    <div class="box-header with-border">
                      <h3 class="box-title">Clients</h3>
                      <div class="box-tools pull-right">
                        <span class="label label-danger">4 New Clients</span>
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                      </div>
                    </div><!-- /.box-header -->
                    <div class="box-body no-padding">
                      <ul class="users-list clearfix">
                       <?php
						$sql = "SELECT * FROM cldata ORDER BY clId DESC LIMIT 4";
						$data = $this->db->query($sql);

						if($data->num_rows() == 0){	$prodnum2 = $data->num_rows(); }
						else {	$prodnum2 = $data->num_rows(); }

						$data = $data->result_array(); //create an array
					?>
                    <?php
                    foreach ($data as $field)
					{

					?>
                        <li>
                          <?php
					  		echo img(array('src' => "upload/cl_skeuhdflefjslfjseflksjef/".$field['photo'],'class'=>'', 'width' => '128','height' => '128'));
					  ?>
                       <?php echo anchor( 'viewclient/'.$field['clId'].'', ''.$field['fullname'].'', array( 'class'=>'users-list-name')); ?>
                        </li>
                    <?php
					}
					?>
                      </ul><!-- /.users-list -->
                    </div><!-- /.box-body -->
                    <div class="box-footer text-center">
                      <?php echo anchor( 'viewclients', 'View All Clients', array( 'class'=>'')); ?>
                    </div><!-- /.box-footer -->
                  </div>
              <!--/.box -->

            </section><!-- /.Left col -->
			</div>
			<div class="row">
            <!-- right col (We are only adding the ID to make the widgets sortable)-->
            <section class="col-lg-12">

              <!-- Calendar -->
              <div class="box box-solid bg-light-blue-gradient">
                <div class="box-header">
                  <i class="fa fa-calendar"></i>
                  <h3 class="box-title">Calendar</h3>
                  <!-- tools box -->
                  <div class="pull-right box-tools">
                    <button class="btn btn-primary btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  </div><!-- /. tools -->
                </div><!-- /.box-header -->
                <div class="box-body no-padding">
                  <!--The calendar -->
                  <div id="calendar" style="width: 100%"></div>
                </div><!-- /.box-body -->

              </div><!-- /.box -->

            </section><!-- right col -->

			<!--<section class="col-lg-6">
			 <-- quick email widget --
              <div class="box box-info">
                <div class="box-header">
                  <i class="fa fa-envelope"></i>
                  <h3 class="box-title">Quick Email</h3>
                  <-- tools box --
                  <div class="pull-right box-tools">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  </div><!-- /. tools --
                </div>
                <div class="box-body">
                  <form action="#" method="post">
                    <div class="form-group">
                      <input type="email" class="form-control" name="emailto" placeholder="Email to:">
                    </div>
                    <div class="form-group">
                      <input type="text" class="form-control" name="subject" placeholder="Subject">
                    </div>
                    <div>
                      <textarea class="textarea" placeholder="Message" style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                    </div>
                  </form>
                </div>
                <div class="box-footer clearfix">
                  <button class="pull-right btn btn-default" id="sendEmail">Send <i class="fa fa-arrow-circle-right"></i></button>
                </div>
              </div>
			  </section>-->
          </div><!-- /.row (main row) -->

        </section><!-- /.content -->
      </div>
