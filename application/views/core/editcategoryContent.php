<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
        <?php 
			if ( !isset($message)){}
			else{ echo "<h3 style=\"color:green; text-align: center;\">".$message."</h3>"; }
	  	?>
          <h1>
            Edit Product Category
            <small>Please fill out the form carefully.</small>
          </h1>
        </section>
        <!-- Main content -->
        <section class="content">
        <?php echo form_open_multipart('updatecategory');?>
        
        <!-- Basic Information -->
        <div class="row">
            <div class="col-md-12">
              <div class="box box-info">
                <div class="box-header">
                  <h3 class="box-title">Edit Product Category</h3>
                  <?php
					$sql = "SELECT * FROM pcategories Where catid=".$id; 
					$data = $this->db->query($sql);
						
					$data = $data->result_array(); //create an array
					?>
                  <!-- tools box -->
                  <div class="pull-right box-tools">
                    <button class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                  </div><!-- /. tools -->
                </div><!-- /.box-header -->
                <div class="box-body pad">
                        <div class="col-md-5">
                        <?php
						foreach ($data as $field)
						{
							?>
                            <input type="text" class="form-control hidden" name="catid" id="maincategory1" value="<?php echo $field['catid']; ?>">
                                <div class="form-group">
                                  <input type="text" class="form-control" name="maincategory" id="maincategory1" value="<?php echo $field['cat']; ?>">
                                </div>
                                <div class="form-group col-sm-offset-1">
                                  <input type="text" class="form-control" name="subcategory1" id="subcategory1" value="<?php echo $field['sub1']; ?>">
                                </div>
                                <div class="form-group col-sm-offset-2">
                                  <input type="text" class="form-control" name="subcategory2" id="subcategory2" value="<?php echo $field['sub2']; ?>">
                                </div>
                                <div class="form-group col-sm-offset-3">
                                  <input type="text" class="form-control" name="subcategory3" id="subcategory4" value="<?php echo $field['sub3']; ?>">
                                </div>
                                <div class="form-group col-sm-offset-4">
                                  <input type="text" class="form-control" name="subcategory4" id="subcategory4" value="<?php echo $field['sub4']; ?>">
                                </div>
							<?php
                            }//end of foreach
                            ?>
                        </div>
                <button type="submit"  value="upload" class="btn btn-block btn-primary btn-lg">Submit</button>
        
        		</form>
                </div>
              </div><!-- /.box -->
            </div><!-- /.col-->
          </div><!-- ./row -->
          
        </section><!-- /.content -->
      </div>