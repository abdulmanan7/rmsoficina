<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
        <?php 
			if ( !isset($message)){}
			else{ echo "<h3 style=\"color:green; text-align: center;\">".$message."</h3>"; }
	  	?>
          <h1>
            Edit Client
            <small>Please fill out the form carefully.</small>
          </h1>
        </section>
         <?php
			$sql = "SELECT * FROM cldata Where clId=".$id; 
			$data = $this->db->query($sql);
			
			$data = $data->result_array();
		?>
        <!-- Main content -->
        <section class="content">
        <?php echo form_open_multipart('updateClientData');?>
        <?php
		foreach($data as $field)
		{
		?>
        <!-- Basic Information -->
        <div class="row">
            <div class="col-md-12">
              <div class="box box-success">
                <div class="box-header">
                  <h3 class="box-title">Update Client Form</h3>
                  <!-- tools box -->
                  <div class="pull-right box-tools">
                    <button class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                  </div><!-- /. tools -->
                </div><!-- /.box-header -->
                <div class="box-body pad">
                        <div class="col-md-4">
                        <input class="hidden" name="clId" value="<?php echo $id; ?>" />
                                <div class="form-group">
                                  <label for="upload_file1">Upload Client Photo</label>
                                  <input type="file" name="userfile" id="userfile1" readonly="true" />
                                  <p class="help-block"></p>
                                </div>
                                <div class="form-group">
                                  <input type="text" class="form-control" name="fullname" id="fullname1" value="<?php echo $field['fullname']; ?>" placeholder="Full Name" required>
                                </div>
                                <div class="form-group">
                                  <input type="text" class="form-control" name="occupation" id="occupation1" value="<?php echo $field['occupation']; ?>" placeholder="Occupation" required>
                                </div>
                                <div class="form-group">
                                  <input type="text" class="form-control" name="employer" id="employer1" value="<?php echo $field['employer']; ?>" placeholder="Company Name" required>
                                </div>                                  
                               	<div class="form-group">
                                  <input type="email" class="form-control" name="email" id="email" value="<?php echo $field['email']; ?>" placeholder="Email" required>
                                </div>
                                <div class="form-group">
                                  <input type="text" class="form-control" name="add1" id="add11" value="<?php echo $field['add1']; ?>" placeholder="Address Line 1" required>
                                </div>
                                <div class="form-group">
                                  <input type="text" class="form-control" name="add2" id="add12" value="<?php echo $field['add2']; ?>" placeholder="Address Line 2" required>
                                </div> 
                        </div>
                        <div class="col-sm-offset-1 col-md-7">
                        <div class="form-group">
                                 <label for="worknum">Work Number 1/ext</label>
                                  <input type="text" class="form-control" name="worknum" id="worknum1" value="<?php echo $field['worknum']; ?>" placeholder="+1-765-8747/ext: 44567" required>
                        </div> 
                        <div class="form-group">
                         <label for="worknum2">Work Number 2/ext</label>
                          <input type="text" class="form-control" name="worknum2" id="worknum12" value="<?php echo $field['worknum2']; ?>" placeholder="+1-765-8747/ext: 44567" required>
                        </div> 
                        <div class="form-group">
                        <label for="editorEmp">Other Details</label>
                        <textarea id="editorEmp1" name="editorEmp" class="form-control" rows="10">
                        <?php echo $field['editorEmp']; ?>"
                        </textarea>
                    </div>
                                
                        </div>
                </div>
              </div><!-- /.box -->
            </div><!-- /.col-->
          </div><!-- ./row -->
        <?php
		}
		?>
        <button type="submit"  value="upload" class="btn btn-block btn-primary btn-lg">Update Client</button>
        
        </form>
        </section><!-- /.content -->
      </div>