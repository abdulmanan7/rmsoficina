<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Add Employee Dataaaaa
            <small>Please fill out the form carefully.</small>
          </h1>
        </section>
        
        <?php
			$sql = "SELECT * FROM edata Where eid=".$id; 
			$data = $this->db->query($sql);
			
			$data = $data->result_array();
		?>
        <!-- Main content -->
        <section class="content">
        <?php echo form_open_multipart('updateemployeeData'); ?>
        
        <?php
		foreach($data as $field)
		{
		?>
        <!-- Basic Information -->
        <div class="row">
            <div class="col-md-12">
              <div class="box box-info">
                <div class="box-header">
                  <h3 class="box-title">Basic Information</h3>
                  <!-- tools box -->
                  <div class="pull-right box-tools">
                    <button class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                  </div><!-- /. tools -->
                </div><!-- /.box-header -->
                <div class="box-body pad">
                        <div class="col-md-5">
                        <input class="hidden" name="id" value="<?php echo $id; ?>" />
                                <div class="form-group">
                                  <input type="text" class="form-control" name="fullname" id="fullname1" value="<?php echo $field['fullname']; ?>">
                                </div>
                                <div class="form-group">
                                  <input type="text" class="form-control" name="fname" id="fname1" value="<?php echo $field['fname']; ?>">
                                </div>
                                <div class="form-group">
                                  <input type="text" class="form-control" name="lname" id="lname1" value="<?php echo $field['lname']; ?>">
                                </div>
                        </div>
                        <div class="col-sm-offset-1 col-md-5">
                               	<div class="form-group">
                                  <input type="email" class="form-control" name="email" id="email" value="<?php echo $field['email']; ?>">
                                </div>
                                <div class="form-group">
                                  <label for="upload_file1">Upload employee photo</label>
                                  <input type="file" name="userfile" id="userfile1" readonly="true" />
                                  <p class="help-block"></p>
                                </div>
                        </div>
                </div>
              </div><!-- /.box -->
            </div><!-- /.col-->
          </div><!-- ./row -->

     <!-- Address and Contact Information -->
           <div class="row">
            <div class="col-md-12">
              <div class="box box-success">
                <div class="box-header">
                  <h3 class="box-title">Address and Contact Information</h3>
                  <!-- tools box -->
                  <div class="pull-right box-tools">
                    <button class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                  </div><!-- /. tools -->
                </div><!-- /.box-header -->
                	<div class="box-body pad">
                        <div class="col-md-5">
                        	<div class="form-group">
                              <input type="text" class="form-control" name="addl1" id="add1" value="<?php echo $field['addl1']; ?>">
                            </div>
                            <div class="form-group">
                              <input type="text" class="form-control" name="addl2" id="add2" value="<?php echo $field['addl2']; ?>">
                            </div>
                            <div class="form-group">
                              <input type="text" class="form-control" name="city" id="city1" value="<?php echo $field['city']; ?>">
                            </div>
                            <div class="form-group">
                              <input type="text" class="form-control" name="srp" id="srp1" value="<?php echo $field['srp']; ?>">
                            </div>
                            <div class="form-group">
                              <input type="text" class="form-control" name="zip" id="zip1" value="<?php echo $field['zip']; ?>">
                            </div>                                
                        </div>
                        <div class="col-sm-offset-1 col-md-5">
                        <h3>Contact Information</h3>
                        	<div class="form-group">
                                    <div class="input-group">
                                      <div class="input-group-addon">
	                                      <i class="fa fa-mobile"></i>
                                      </div>
                                      <input type="text" class="form-control" name="mobile" id="mobile1" value="<?php echo $field['mobile']; ?>">
                                    </div><!-- /.input group -->
                            </div><!-- /.form group -->
                            <div class="form-group">
                                    <div class="input-group">
                                      <div class="input-group-addon">
	                                      <i class="fa fa-home"></i>
                                      </div>
                                      <input type="text" class="form-control" name="home" id="home1" value="<?php echo $field['home']; ?>">
                                    </div><!-- /.input group -->
                            </div><!-- /.form group -->
                            <div class="form-group">
                                    <div class="input-group">
                                      <div class="input-group-addon">
	                                      <i class="fa fa-suitcase"></i>
                                      </div>
                                      <input type="text" class="form-control" name="work" id="work1" value="<?php echo $field['work']; ?>">
                                    </div><!-- /.input group -->
                            </div><!-- /.form group -->
                        </div>
                </div>
              </div><!-- /.box -->
            </div><!-- /.col-->
          </div><!-- ./row -->
          
<!-- Personal Information -->
		<div class="row">
            <div class="col-md-12">
              <div class="box box-warning">
                <div class="box-header">
                  <h3 class="box-title">Personal Information</h3>
                  <!-- tools box -->
                  <div class="pull-right box-tools">
                    <button class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                  </div><!-- /. tools -->
                </div><!-- /.box-header -->
                	<div class="box-body pad">
                        <div class="col-md-5">
                        	<div class="form-group">
                              <input type="text" class="form-control" name="ssn-nis" id="ssn-nis1" value="<?php echo $field['ssn-nis']; ?>">
                            </div>
                            <div class="form-group">
                              <input type="text" class="form-control" name="bir" id="bir2" value="<?php echo $field['bir']; ?>">
                            </div>
                            <div class="form-group">
                              <label>Type of Salary</label>
                              <select class="form-control" name="tos">
                                <option value="monthly">Monthly</option>
                                <option value="weekly">Weekly</option>
                                <option value="hourly">Hourly</option>
                                <option value="other">Other</option>
                              </select>
                            </div>
                            <div class="form-group">
                              <input type="text" class="form-control" name="otherdetails" id="otherdetails1" value="<?php echo $field['otherdetails']; ?>">
                            </div>
                            <div class="form-group">
                              <input type="text" class="form-control" name="payrate" id="payrate1" value="<?php echo $field['payrate']; ?>">
                            </div>
                            <div class="form-group">
                              <input type="text" class="form-control" name="jobtitle" id="jobtitle1" value="<?php echo $field['jobtitle']; ?>">
                            </div>                                
                        </div>
                        <div class="col-sm-offset-1 col-md-5">
                        <h3>Education</h3>
                            <div class="form-group">
                              <input type="text" class="form-control" name="tertiaryschoolname" id="tertiaryschoolname1" value="<?php echo $field['tertiaryschoolname']; ?>">
                            </div>
                            <div class="form-group">
                              <input type="text" class="form-control" name="secondaryschoolname" id="secondaryschoolname1" value="<?php echo $field['secondaryschoolname']; ?>">
                            </div>
                            <h3>Qualifications</h3>
                            <div class="form-group">
                              <input type="text" class="form-control" name="tertiarylevel" id="tertiaryschoolname1" value="<?php echo $field['tertiarylevel']; ?>">
                            </div>
                            <div class="form-group">
                              <input type="text" class="form-control" name="secondarylevel" id="tertiaryschoolname1" value="<?php echo $field['secondarylevel']; ?>">
                            </div>
                        </div>
                    </div>
                  </div><!-- /.box -->
                </div><!-- /.col-->
              </div><!-- ./row -->
                    
    <!-- Emergency Contacts -->
            <div class="row">
            <div class="col-md-12">
              <div class="box box-info">
                <div class="box-header">
                  <h3 class="box-title">Emergency Contacts</h3>
                  <!-- tools box -->
                  <div class="pull-right box-tools">
                    <button class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                  </div><!-- /. tools -->
                </div><!-- /.box-header -->
                <div class="box-body pad">
                        <div class="col-md-5">
                                <div class="form-group">
                                  <input type="text" class="form-control" name="fullnameec" id="fullnameec" value="<?php echo $field['fullnameec']; ?>">
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                      <div class="input-group-addon">
	                                      <i class="fa fa-mobile"></i>
                                      </div>
                                      <input type="text" class="form-control" name="mobileec" id="mobileec1" value="<?php echo $field['mobileec']; ?>">
                                    </div><!-- /.input group -->
                                </div><!-- /.form group -->
                                <div class="form-group">
                                        <div class="input-group">
                                          <div class="input-group-addon">
                                              <i class="fa fa-home"></i>
                                          </div>
                                          <input type="text" class="form-control" name="homeec" id="homeec1" value="<?php echo $field['homeec']; ?>">
                                        </div><!-- /.input group -->
                                </div><!-- /.form group -->
                                <div class="form-group">
                                        <div class="input-group">
                                          <div class="input-group-addon">
                                              <i class="fa fa-suitcase"></i>
                                          </div>
                                          <input type="text" class="form-control" name="workec" id="workec1" value="<?php echo $field['workec']; ?>">
                                        </div><!-- /.input group -->
                                </div><!-- /.form group -->
                                <div class="form-group">
                                  <input type="text" class="form-control" name="addec" id="addec1" value="<?php echo $field['addec']; ?>">
                                </div>
                                <div class="form-group">
                                  <input type="text" class="form-control" name="addec2" id="addec2" value="<?php echo $field['addec2']; ?>">
                                </div>
                                <div class="form-group">
                                  <input type="text" class="form-control" name="relationec" id="relation1" value="<?php echo $field['relationec']; ?>">
                                </div>
                        </div>
                        <div class="col-sm-offset-1 col-md-5">
                        		<div class="form-group">
                                  <input type="text" class="form-control" name="fullnameec2" id="fullnameec2" value="<?php echo $field['fullnameec2']; ?>">
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                      <div class="input-group-addon">
	                                      <i class="fa fa-mobile"></i>
                                      </div>
                                      <input type="text" class="form-control" name="mobileec2" id="mobileec1" value="<?php echo $field['mobileec2']; ?>">
                                    </div><!-- /.input group -->
                                </div><!-- /.form group -->
                                <div class="form-group">
                                        <div class="input-group">
                                          <div class="input-group-addon">
                                              <i class="fa fa-home"></i>
                                          </div>
                                          <input type="text" class="form-control" name="homeec2" id="homeec1" value="<?php echo $field['homeec2']; ?>">
                                        </div><!-- /.input group -->
                                </div><!-- /.form group -->
                                <div class="form-group">
                                        <div class="input-group">
                                          <div class="input-group-addon">
                                              <i class="fa fa-suitcase"></i>
                                          </div>
                                          <input type="text" class="form-control" name="workec2" id="workec1" value="<?php echo $field['workec2']; ?>">
                                        </div><!-- /.input group -->
                                </div><!-- /.form group -->
                                <div class="form-group">
                                  <input type="text" class="form-control" name="addec22" id="addec1" value="<?php echo $field['addec22']; ?>">
                                </div>
                                <div class="form-group">
                                  <input type="text" class="form-control" name="addec222" id="addec2" value="<?php echo $field['addec222']; ?>">
                                </div>
                                <div class="form-group">
                                  <input type="text" class="form-control" name="relationec2" id="relation1" value="<?php echo $field['relationec2']; ?>">
                                </div>
                        </div>
                </div>
              </div><!-- /.box -->
            </div><!-- /.col-->
          </div><!-- ./row -->
          
          
          <div class="row">
            <div class="col-md-12">
              <div class="box box-alert">
                <div class="box-header">
                  <h3 class="box-title">Other Details</h3>
                  <!-- tools box -->
                  <div class="pull-right box-tools">
                    <button class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                  </div><!-- /. tools -->
                </div><!-- /.box-header -->
                <div class="box-body pad">
                    <div class="form-group">
                        <textarea id="editorEmp1" name="editorEmp" class="form-control" rows="10">
                            <?php echo $field['editorEmp']; ?>"
                        </textarea>
                    </div>
                </div>
              </div><!-- /.box -->
            </div><!-- /.col-->
          </div><!-- ./row -->
         <?php
		}
		?>
        <button type="submit"  value="upload" class="btn btn-block btn-primary btn-lg">Submit Employee Information</button>
        
        </form>
        </section><!-- /.content -->
      </div>