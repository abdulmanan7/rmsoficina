<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
        <?php
			if ( !isset($message)){}
			else{ echo "<h3 style=\"color:green; text-align: center;\">".$message."</h3>"; }
	  	?>
          <h1>
            Edit Product
            <small>Please fill out the form carefully.</small>
          </h1>
        </section>
        <?php
			//if the id was not sent to the page:
			if ( !isset($id)){ echo "An error has occurred.<br>"; echo anchor( 'viewproducts', '<b>Back to View Products.</b>', array( 'class'=>'btn btn-danger btn-flat'));
			}
			else{//if the id was sent to the page:
			$sql = "SELECT * FROM pdata Where prodId=".$id; 
			$data = $this->db->query($sql);
			if($data->num_rows()!=0)
			{
			$data = $data->result_array();
		?>
        <!-- Main content -->
        <section class="content">
        <?php echo form_open_multipart('editProductData');?>
        <?php
		foreach($data as $field)
		{
		?>	
        <!-- Basic Information -->
        <div class="row">
            <div class="col-md-12">
              <div class="box box-success">
                <div class="box-header">
                  <h3 class="box-title">Basic Product Information</h3>
                  <!-- tools box -->
                  <div class="pull-right box-tools">
                    <button class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                  </div><!-- /. tools -->
                </div><!-- /.box-header -->
                <div class="box-body pad">
                        <div class="col-md-5">
                        <input class="hidden" name="id" value="<?php echo $id; ?>" />
                                <div class="form-group">
                                  <input type="text" class="form-control" name="sku" id="sku1" title="SKU - A unique idetifier for each product." value="<?php echo $field['sku']; ?>" placeholder="SKU">
                                </div>
                                <div class="form-group">
                                  <input type="text" class="form-control" name="prodname" id="prodname1" value="<?php echo $field['prodname']; ?>" placeholder="Product Name">
                                </div>
                                <div class="form-group">
                                  <input type="text" class="form-control" name="price" id="price1" value="<?php echo $field['price']; ?>" placeholder="Regular Price (exclude the currency symbol)">
                                </div>
                                <div class="form-group">
                                  <input type="text" class="form-control" name="saleprice" id="saleprice1" value="<?php echo $field['saleprice']; ?>" placeholder="Sale Price (exclude the currency symbol)">
                                </div>
                                <div class="form-group">
                                  <select class="form-control" name="stock">
                                    <option value="In Stock">In Stock</option>
                                    <option value="Out of Stock">Out of Stock</option>
                                  </select>
                                </div>
                                <div class="form-group">
                                <label for="qty">Stock Quantity</label>
                                  <input type="text" class="form-control" name="stkqty" id="stkqty1" value="<?php echo $field['stkqty']; ?>" title="0: None, -1: Unlimited">
                                </div> 
                        </div>
                        <div class="col-sm-offset-1 col-md-5">                                   
                               	<div class="form-group">
                                <label for="cat">Main Category</label>
                                  <select class="form-control" name="cat">
                                  <?php echo '<option value = "'.$field['cat'].'">'.$field['cat'].'</option>'; ?>
                                  <?php
								  	//Retrieve categories
										$sql = "SELECT cat FROM pcategories"; 
										$qry = $this->db->query($sql);
											
										foreach ($qry->result() as $row)
										{
										   echo '<option value = "'.$row->cat.'">'.$row->cat.'</option>';
										}
									?>
                                  </select>
                                </div>
                                <div class="form-group">
                                <label for="sub1">Sub Category 1</label>
                                  <select class="form-control" name="sub1">
                                  <?php echo '<option value = "'.$field['sub1'].'">'.$field['sub1'].'</option>'; ?>
                                  <option value="None">None</option>
                                  <?php
								  	//Retrieve categories
										$sql = "SELECT sub1 FROM pcategories"; 
										$qry = $this->db->query($sql);
											
										foreach ($qry->result() as $row)
										{
										   if($row->sub1 != "") echo '<option value = "'.$row->sub1.'">'.$row->sub1.'</option>';
										}
									?>
                                  </select>
                                </div>
                                <div class="form-group">
                                <label for="sub2">Sub Category 2</label>
                                  <select class="form-control" name="sub2">
                                  <?php echo '<option value = "'.$field['sub2'].'">'.$field['sub2'].'</option>'; ?>
                                  <option value="None">None</option>
                                  <?php
								  	//Retrieve categories
										$sql = "SELECT sub2 FROM pcategories"; 
										$qry = $this->db->query($sql);
											
										foreach ($qry->result() as $row)
										{
										   if($row->sub2 != "") echo '<option value = "'.$row->sub2.'">'.$row->sub2.'</option>';
										}
									?>
                                  </select>
                                </div>
                                <div class="form-group">
                                <label for="sub3">Sub Category 3</label>
                                  <select class="form-control" name="sub3">
                                  <?php echo '<option value = "'.$field['sub3'].'">'.$field['sub3'].'</option>'; ?>
                                  <option value="None">None</option>
                                  <?php
								  	//Retrieve categories
										$sql = "SELECT sub3 FROM pcategories"; 
										$qry = $this->db->query($sql);
											
										foreach ($qry->result() as $row)
										{
										   if($row->sub3 != "") echo '<option value = "'.$row->sub3.'">'.$row->sub3.'</option>';
										}
									?>
                                  </select>
                                </div>
                                <div class="form-group">
                                <label for="sub4">Sub Category 4</label>
                                  <select class="form-control" name="sub4">
                                  <?php echo '<option value = "'.$field['sub4'].'">'.$field['sub4'].'</option>'; ?>
                                  <option value="None">None</option>
                                  <?php
								  	//Retrieve categories
										$sql = "SELECT sub4 FROM pcategories"; 
										$qry = $this->db->query($sql);
											
										foreach ($qry->result() as $row)
										{
										   if($row->sub4 != "") echo '<option value = "'.$row->sub4.'">'.$row->sub4.'</option>';
										}
									?>
                                  </select>
                                </div>
                        </div>
                </div>
              </div><!-- /.box -->
            </div><!-- /.col-->
          </div><!-- ./row -->          
          
          <div class="row">
            <div class="col-md-12">
              <div class="box box-info">
                <div class="box-header">
                  <h3 class="box-title">Product Images</h3>
                  <!-- tools box -->
                  <div class="pull-right box-tools">
                    <button class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                  </div><!-- /. tools -->
                </div><!-- /.box-header -->
                <div class="box-body pad">
                    <div class="form-group">
                      <label for="upload_file1">Upload product photo 1</label>
                      <input type="file" name="userfile1" id="userfile1" readonly="true" />
                      <p class="help-block"></p>
                    </div>
                    <div class="form-group">
                      <label for="upload_file1">Upload product photo 2</label>
                      <input type="file" name="userfile2" id="userfile2" readonly="true" />
                      <p class="help-block"></p>
                    </div>
                    <div class="form-group">
                      <label for="upload_file1">Upload product photo 3</label>
                      <input type="file" name="userfile3" id="userfile3" readonly="true" />
                      <p class="help-block"></p>
                    </div>
                </div>
              </div><!-- /.box -->
            </div><!-- /.col-->
          </div><!-- ./row -->
          
          <div class="row">
            <div class="col-md-12">
              <div class="box box-alert">
                <div class="box-header">
                  <h3 class="box-title">Product Details</h3>
                  <!-- tools box -->
                  <div class="pull-right box-tools">
                    <button class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                  </div><!-- /. tools -->
                </div><!-- /.box-header -->
                <div class="box-body pad">
                    <div class="form-group">
                        <textarea id="editorEmp1" name="editorEmp" class="form-control" rows="10"> 
						<?php echo $field['editorEmp']; ?>
                        </textarea>
                    </div>
                </div>
              </div><!-- /.box -->
            </div><!-- /.col-->
          </div><!-- ./row -->
          <?php
			}
			?>
        <button type="submit"  value="upload" class="btn btn-block btn-primary btn-lg">Update Product</button>
        
        </form>
        </section><!-- /.content -->
        <?php
			}
			else
			{?>
            <section class="content">
            <?php
				echo "That product does not exist!";
			?>
            </section>
                        <?php
			}
    }
    ?>
      </div>