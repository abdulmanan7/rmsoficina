<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Editar Usuario
            <small>Please only change fields that you want to change.</small>
          </h1>
        </section>
        <?php
//if the id was not sent to the page:
if (!isset($id)) {
	echo "An error has occurred.<br>";
	echo anchor('viewUsers', '<b>Back to View Users.</b>', array('class' => 'btn btn-danger btn-flat'));
} else {
//if the id was sent to the page:
	$sql = "SELECT * FROM coreuser Where id=" . $id;
	$data = $this->db->query($sql);
	if ($data->num_rows() != 0) {
		$data = $data->result_array();
		?>
		<!-- Main content -->
        <section class="content">
        <?php echo form_open_multipart('updateCoreUser');?>
    <?php
foreach ($data as $field) {
			?>
        <!-- Basic Information -->
        <div class="row">
            <div class="col-md-12">
              <div class="box box-info">
                <div class="box-header">
                  <h3 class="box-title">Editar Formulario Usuario</h3>
                  <!-- tools box -->
                  <div class="pull-right box-tools">
                    <button class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                  </div><!-- /. tools -->
                </div><!-- /.box-header -->
                <div class="box-body pad">
                        <div class="col-md-5">
                                <div class="form-group">
                                <input class="hidden" name="id" value="<?php echo $id;?>" />
                                  <label for="upload_file1">Subir Foto</label>
                                  <input type="file" name="userfile" id="userfile1" readonly="true" />
                                  <p class="help-block"></p>
                                </div>

                        </div>
                        <div class="col-sm-offset-1 col-md-5">
                        <?php
if (strcmp($field['nombre'], 'superadmin') != 0) {
				?>
                        		<div class="form-group">
                                 <label for="usrnm">Username (Name used to log into the application)</label>
                                  <input type="text" class="form-control" name="usrnm" id="usrnm1" value="<?php echo $field['nombre'];?>" required>
                                </div>
                                <div class="form-group">
                                 <label for="email">Email</label>
                                  <input type="text" class="form-control" name="email" id="email1" value="<?php echo $field['email'];?>" disabled>
                                  <input type="text" class="hidden" name="email" id="email1" value="<?php echo $field['email'];?>" >
                                </div>
                        <?php
} else {
				?>
                         <?php
}
			?>
                              	<div class="form-group">
                                 <label for="pastech">Password</label>
                                  <input type="password" class="form-control" name="pastech" id="pastech1" value="nochange" required>
                                </div>

                        </div>
                </div>
              </div><!-- /.box -->
            </div><!-- /.col-->
          </div><!-- ./row -->
	<?php
}
		?>
        <button type="submit"  value="upload" class="btn btn-block btn-primary btn-lg">Grabar Cambios</button>

        </form>
        </section><!-- /.content -->
		<?php
} else {
		?>
            <section class="content">
            <?php
echo "That user does not exist!";
		?>
            </section>
                        <?php
}
}
?>
      </div>
