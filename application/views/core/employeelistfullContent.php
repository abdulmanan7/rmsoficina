      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Employee Full Details
          </h1>
        </section>
		<?php
			$sql = "SELECT * FROM edata Where eid=".$empid; 
			$data = $this->db->query($sql);
		
       	 	$data = $data->result_array(); //create an array
		
			foreach ($data as $field)
		{?>
        <!-- Main content -->
        <section class="content">

          <div class="row">
            <div class="col-md-3">

<!-- Profile Image -->
              <div class="box box-primary">
                <div class="box-body box-profile">
                  
                  <?php 
				  if(strcmp($field['profilephoto'], "blank.png") == 0)
				  	echo img(array('src' => 'assets/images/blank.png','class'=>'profile-user-img img-responsive'));
                  else
                  	echo img(array('src' => "upload/e_kdjhawkdhawjkhd/".$field['profilephoto'],'class'=>'profile-user-img img-responsive')); ?>
                  <h3 class="profile-username text-center"><?php echo $field['salutation'].'. '.$field['fullname']; ?></h3>
                  <p class="text-muted text-center"><?php echo $field['jobtitle']; ?></p>

                  <ul class="list-group list-group-unbordered">
                    <li class="list-group-item">
                      <b>Email</b> <a class="pull-right"><?php echo $field['email']; ?></a>
                    </li>
                    <li class="list-group-item">
                      <b>Mobile</b> <a class="pull-right"><?php echo $field['mobile']; ?></a>
                    </li>
                    <li class="list-group-item">
                      <b>Date of Birth</b> <a class="pull-right"><?php echo $field['dob']; ?></a>
                    </li>
                  </ul>
                </div><!-- /.box-body -->
              </div><!-- /.box -->

            </div><!-- /.col -->
            <div class="col-md-9">
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#Details" data-toggle="tab"><i class="fa fa-file-text-o"></i> Details</a></li>
                  <li><a href="#emergency" data-toggle="tab"><i class="fa fa-ambulance"></i> Emergency</a></li>
                  <li><a href="#options" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
                </ul>
                <div class="tab-content">
                  <div class="active tab-pane" id="Details">
			<div class="box box-info collapsed-box">
                <div class="box-header">
                  <h3 class="box-title">Basic Information</h3>
                  <!-- tools box -->
                  <div class="pull-right box-tools">
                    <button class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-plus"></i></button>
                  </div><!-- /. tools -->
                </div><!-- /.box-header -->
                <div class="box-body pad">
                        <div class="col-md-5">
                                <div>
                                  <?php echo '<span class="label label-info">First Name:</span> '.$field['fname'].'<br><span class="label label-info">Last Name:</span> '.$field['lname']; ?>
                                  <?php echo '<br><span class="label label-info">Sex:</span> '.$field['sex']; ?>
                                  <?php echo '<br><span class="label label-info">Date of Birth:</span> '.$field['dob']; ?>
                                  <?php echo '<br><span class="label label-info">Email:</span> '.$field['email']; ?>
                                </div>
                        </div>
                </div>
              </div><!-- /.box -->
              
              <div class="box box-success collapsed-box">
                <div class="box-header">
                  <h3 class="box-title">Address and Contact Information</h3>
                  <!-- tools box -->
                  <div class="pull-right box-tools">
                    <button class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-plus"></i></button>
                  </div><!-- /. tools -->
                </div><!-- /.box-header -->
                	<div class="box-body pad">
                        <div class="col-md-5">
                        	<div>
                            <?php echo '<span class="label label-success">Address Line 1:</span> '.$field['addl1']; ?>
                            <?php echo '<br><span class="label label-success">Address Line 2:</span> '.$field['addl2']; ?>
                            <?php echo '<br><span class="label label-success">City:</span> '.$field['city']; ?>
                            <?php echo '<br><span class="label label-success">State/Region/Province:</span> '.$field['srp']; ?>
                            <?php echo '<br><span class="label label-success">Zip:</span> '.$field['zip']; ?>
                            <?php echo '<br><span class="label label-success">Country:</span> '.$field['cntry']; ?>
                            <?php echo '<br><span class="label label-success">Mobile:</span> '.$field['mobile']; ?>
                            <?php echo '<br><span class="label label-success">Home:</span> '.$field['home']; ?>
                            <?php echo '<br><span class="label label-success">Work:</span> '.$field['work']; ?>
                            </div>                                
                        </div>
                </div>
              </div><!-- /.box -->
              
              <div class="box box-warning collapsed-box">
                <div class="box-header">
                  <h3 class="box-title">Personal Information</h3>
                  <!-- tools box -->
                  <div class="pull-right box-tools">
                    <button class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-plus"></i></button>
                  </div><!-- /. tools -->
                </div><!-- /.box-header -->
                	<div class="box-body pad">
                        <div class="col-md-5">
                        	<div>
                            <?php echo '<span class="label label-warning">Place of Birth:</span> '.$field['pob']; ?>
                            <?php echo '<br><span class="label label-warning">SSN/NIS:</span>  '.$field['ssn-nis']; ?>
							<?php echo '<br><span class="label label-warning">BIR:</span>  '.$field['bir']; ?>
                            <?php echo '<br><span class="label label-warning">Type of Salary:</span> '.$field['tos']; ?>
                            <?php echo '<br><span class="label label-warning">Other Details:</span> '.$field['otherdetails']; ?>
                            <?php echo '<br><span class="label label-warning">Pay Rate:</span> '.$field['payrate']; ?>
                            <?php echo '<br><span class="label label-warning">Job Title:</span> '.$field['jobtitle']; ?>
                            <?php echo '<br><span class="label label-warning">Tertiary School Name:</span> '.$field['tertiaryschoolname']; ?>
                            <?php echo '<br><span class="label label-warning">Secondary School Name:</span> '.$field['secondaryschoolname']; ?>
                            <?php echo '<br><span class="label label-warning">Tertiary Level:</span> '.$field['tertiarylevel']; ?>
                            <?php echo '<br><span class="label label-warning">Secondary Level:</span> '.$field['secondarylevel']; ?>
                            </div>                               
                        </div>
                    </div>
                  </div><!-- /.box -->
              <div class="box box-alert collapsed-box">
                <div class="box-header">
                  <h3 class="box-title">Other Details</h3>
                  <!-- tools box -->
                  <div class="pull-right box-tools">
                    <button class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-plus"></i></button>
                  </div><!-- /. tools -->
                </div><!-- /.box-header -->
                <div class="box-body pad">
                	<?php echo '<br>'.$field['editorEmp']; ?>
                </div>
              </div><!-- /.box -->
                  </div><!-- /.tab-pane -->
                  
                  <div class="tab-pane" id="emergency">
			<div class="box box-danger">
                <div class="box-header">
                  <h3 class="box-title">Emergency Contacts</h3>
                  <!-- tools box -->
                  <div class="pull-right box-tools">
                    <button class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                  </div><!-- /. tools -->
                </div><!-- /.box-header -->
                <div class="box-body pad">
                        <div class="col-md-5">
                                <div>
                                	<?php echo '<span class="label label-danger">Full Name:</span> '.$field['fullnameec']; ?>
                                    <?php echo '<br><span class="label label-danger">Mobile:</span> '.$field['mobileec']; ?>
                                    <?php echo '<br><span class="label label-danger">Home:</span> '.$field['homeec']; ?>
                                    <?php echo '<br><span class="label label-danger">Work:</span> '.$field['workec']; ?>
                                    <?php echo '<br><span class="label label-danger">Address Line 1:</span> '.$field['addec']; ?>
                                    <?php echo '<br><span class="label label-danger">Address Line 2:</span> '.$field['addec2']; ?>
                                    <?php echo '<br><span class="label label-danger">Relationship:</span> '.$field['relationec']; ?>
                                </div>
                        </div>
                        <div class="col-sm-offset-1 col-md-5">
                        		<div>
                                	<?php echo '<span class="label label-danger">Full Name:</span> '.$field['fullnameec2']; ?>
                                    <?php echo '<br><span class="label label-danger">Mobile:</span> '.$field['mobileec2']; ?>
                                    <?php echo '<br><span class="label label-danger">Home:</span> '.$field['homeec2']; ?>
                                    <?php echo '<br><span class="label label-danger">Work:</span> '.$field['workec2']; ?>
                                    <?php echo '<br><span class="label label-danger">Address Line 1:</span> '.$field['addec22']; ?>
                                    <?php echo '<br><span class="label label-danger">Address Line 2:</span> '.$field['addec222']; ?>
                                    <?php echo '<br><span class="label label-danger">Relationship:</span> '.$field['relationec2']; ?>
                                </div>
                        </div>
                </div>
              </div><!-- /.box -->
             </div><!-- /.tab-pane -->
             <div class="tab-pane" id="options">
             <?php 
			 echo anchor( 'editemployee/'.$empid.'', '<b>Edit Employee</b>', array( 'class'=>'btn btn-info btn-flat'));?>
             <a class="btn btn-danger btn-flat" data-toggle="modal" data-target="#myModal_<?php echo $empid; ?>">Delete Employee</a>
					  <!-- Modal -->
                        <div class="modal fade" id="myModal_<?php echo $empid; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                          <div class="modal-dialog" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Deleting a user.</h4>
                              </div>
                              <div class="modal-body">
                                Are you sure you want to delete this user? Once deleted it cannot be undone.
                              </div>
                              <div class="modal-footer">
                               <?php echo anchor( 'deleteemployee/'.$empid.'', '<b>Delete</b>', array( 'class'=>'btn btn-danger btn-flat')); ?>
                               <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                              </div>
                            </div>
                          </div>
                        </div><!-- End modal -->
             </div><!-- /.tab-pane -->
                </div><!-- /.tab-content -->
              </div><!-- /.nav-tabs-custom -->
            </div><!-- /.col -->
          </div><!-- /.row -->

        </section><!-- /.content -->
         <?php
		}
		?>
      </div><!-- /.content-wrapper -->