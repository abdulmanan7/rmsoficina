    
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
        <?php 
			if ( !isset($message)){}
			else{ echo "<h3 style=\"color:green; text-align: center;\">".$message."</h3>"; }
	  	?>
          <h1>
            Employee List
          </h1>
          <!--<ul class="list">
            <li><a href="#">Sort by A-Z</a></li>
            <li><a href="#">Sort by Z-A</a></li>
            <li class="active">Employee List</li>
          </ul>-->
        </section>
     <?php
	$sql = "SELECT * FROM edata"; 
	$data = $this->db->query($sql);
	
	if($data->num_rows() == 0)
	{
		echo "<br><div class=\"callout callout-info\">
                 <h4>There are no employees added as yet.</h4>
                 <p>Please add some employees.</p>
              </div>";
	}	
	else
	{	
		echo "<section class=\"pull-right content-header\">";
		echo "<span class=\"label label-success\">".$data->num_rows()." employee/s returned</span></section><br><br>";
	  	$data = $data->result_array(); //create an array
		?>   
        <section class="content">
		<div class="box">
        <div class="box-body table-responsive">
        <table id="example1"  class="table table-hover">
        <thead><tr><th><< SORT ASC/DSC >></th></tr></thead>
        <tbody>
        <?php
		foreach ($data as $field)
		{?>
        <tr><td>
        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-md-3">

              <!-- Profile Image -->
              <div class="box box-primary">
                <div class="box-body box-profile">
                  
                  <?php 
				  if(strcmp($field['profilephoto'], "blank.png") == 0)
				  	echo img(array('src' => 'assets/images/blank.png','class'=>'profile-user-img img-responsive'));
                  else
                  	echo img(array('src' => "upload/e_kdjhawkdhawjkhd/".$field['profilephoto'],'class'=>'profile-user-img img-responsive')); ?>
                  <h3 class="profile-username text-center"><?php echo $field['fullname']; ?></h3>
                  <p class="text-muted text-center"><?php echo $field['jobtitle']; ?></p>

                  <ul class="list-group list-group-unbordered">
                    <li class="list-group-item">
                      <b>Email</b> <a class="pull-right"><?php echo $field['email']; ?></a>
                    </li>
                    <li class="list-group-item">
                      <b>Mobile</b> <a class="pull-right"><?php echo $field['mobile']; ?></a>
                    </li>
                    <li class="list-group-item">
                      <b>Date of Birth</b> <a class="pull-right"><?php echo $field['dob']; ?></a>
                    </li>
                  </ul>
                  <?php echo anchor( 'fullprofile/'.$field['eid'].'', '<b>View Full Profile</b>', array( 'class'=>'btn btn-primary btn-block')); ?>
                </div><!-- /.box-body -->
              </div><!-- /.box -->

              
            </div><!-- /.col -->
            <div class="col-md-9">
 <!-- Details Box -->
              <div class="box box-primary">
                <div class="box-header width-border">
                  <h3 class="box-title"><strong>Details</strong></h3>
                  <?php echo '<span>Employee ID#: '.$field['eid'].'</span>'; ?>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <strong><i class="fa fa-map-marker margin-r-5"></i>Address</strong>
                  <p class="text-muted">
                    <?php echo '<span class="label label-success">Address Line 1:</span> '.$field['addl1'].' <span class="label label-info">Address Line 2:</span> '.$field['addl2'].'<br>'; ?>
                    <?php echo '<span class="label label-success">City:</span> '.$field['city']; ?>
                  </p>

                  <hr>

                  <strong><i class="fa fa-ambulance margin-r-5"></i>Emergency Contact</strong>
                  <p class="text-muted"><?php echo '<span class="label label-success">Name:</span> '.$field['fullnameec'].' <span class="label label-info">Phone:</span> '.$field['mobileec'].' '.$field['homeec'].' '.$field['workec'].' <span class="label label-danger">Relation:</span> '.$field['relationec']; ?></p>
                  <p class="text-muted"><?php echo '<span class="label label-success">Name:</span> '.$field['fullnameec2'].' <span class="label label-info">Phone:</span> '.$field['mobileec2'].' '.$field['homeec2'].' '.$field['workec2'].' <span class="label label-danger">Relation:</span> '.$field['relationec2']; ?></p>
                  <hr>

                  <strong><i class="fa fa-file-text-o margin-r-5"></i> Other details</strong>
                  <p><?php echo $field['editorEmp']; ?></p>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
        </td></tr>
        <?php
		}?>
        </tbody>
        
        </table>
		</div>
		</div>
        </section>
        <?php
	}
		?>
      </div><!-- /.content-wrapper -->