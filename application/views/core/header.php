<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'XXXXXX', 'auto');
  ga('send', 'pageview');

</script>
<header class="main-header">
        <!-- Logo -->
        <?php echo anchor('index', '<span class="logo-mini"><b>S</b>APP</span><span class="logo-lg"><b>SBMSX</b>App</span>', array('class' => 'logo'));?>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <i class="fa fa-exchange"></i>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
			              <!-- Messages: style can be found in dropdown.less-->
              <li class="dropdown messages-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-question-circle"></i> Help Information
                </a>
                <ul class="dropdown-menu">
                  <li class="header">Select an option below</li>
                  <li>
                    <!-- inner menu: contains the actual data -->
                    <ul class="menu">
                      <li><!-- start message -->
                        <a href="http://techbaseltd.com/sbmsx-help-documentation/" target="_blank">
                          <div class="pull-left">
                            <?php echo img(array('src' => 'upload/help.png', 'alt' => 'Help Image', 'class' => 'user-image'));?>
                          </div>
                          <h4>
                            Help Document
                            <small></small>
                          </h4>
                          <p>How to use this application.</p>
                        </a>
                      </li><!-- end message -->
                      <li>
                        <a href="http://techbaseltd.com/feedback-form/" target="_blank">
                          <div class="pull-left">
                            <?php echo img(array('src' => 'upload/feedback.png', 'alt' => 'Feedback Image', 'class' => 'user-image'));?>
                          </div>
                          <h4>
                            Feedback Form
                            <small></small>
                          </h4>
                          <p>Suggestions, bug reports, errors, etc.</p>
                        </a>
                      </li>
                      <li>
                        <a href="http://techbaseltd.com/sbmsx-updates/" target="_blank">
                          <div class="pull-left">
                            <?php echo img(array('src' => 'upload/update.png', 'alt' => 'Feedback Image', 'class' => 'user-image'));?>
                          </div>
                          <h4>
                            Updates
                            <small></small>
                          </h4>
                          <p>New features added.</p>
                        </a>
                      </li>
                      <li>
                        <a href="http://techbaseltd.com/sbmsx-bug-fixes/" target="_blank">
                          <div class="pull-left">
                            <?php echo img(array('src' => 'upload/bug.png', 'alt' => 'Feedback Image', 'class' => 'user-image'));?>
                          </div>
                          <h4>
                            Bug Fixes
                            <small></small>
                          </h4>
                          <p>Small to critcal bug fixes.</p>
                        </a>
                      </li>
                    </ul>
                  </li>
                  <li class="footer"><a href="#">Scroll to see more.</a></li>
                </ul>
              </li>
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <?php echo img(array('src' => 'upload/e_kdjhawkdhawjkhd/' . $_SESSION['photo'], 'alt' => 'User Image', 'class' => 'user-image'));?>
                  <span class="hidden-xs"><?php echo $_SESSION['usrnm']?></span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <?php echo img(array('src' => 'upload/e_kdjhawkdhawjkhd/' . $_SESSION['photo'], 'alt' => 'User Image', 'class' => 'img-circle'));?>
                    <p>
                      <?php echo $_SESSION['usrnm'];?><br>
					  <?php echo $_SESSION['ut'];?>
                    </p>
                  </li>
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <?php echo anchor('editCoreuser/' . $_SESSION['userid'], 'Modificar', array('class' => 'btn btn-default btn-flat'));?>
                    </div>
                    <div class="pull-right">
                      <?php echo anchor('logout', 'Salir', array('class' => 'btn btn-default btn-flat'));?>
                    </div>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </nav>
      </header>