<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
        <?php 
			if ( !isset($message)){}
			else{ echo "<h3 style=\"color:green; text-align: center;\">".$message."</h3>"; }
	  	?>
          <h1>
            Import Clients
            <small>Please follow all guidelines.</small>
          </h1>
        </section>
        <!-- Main content -->
        <section class="content">
		<?php echo form_open_multipart('importcsvcontent'); ?>
		    <!-- Basic Information -->
        <div class="row">
            <div class="col-md-12">
              <div class="box box-info">
                <div class="box-header">
                  <h3 class="box-title">Upload Client Excel Sheet</h3>
                  <!-- tools box -->
                  <div class="pull-right box-tools">
                    <button class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                  </div><!-- /. tools -->
                </div><!-- /.box-header -->
                <div class="box-body pad">
                        <div class="col-md-4">
							<div class="form-group">
							  <label for="userfile">XLSX or CSV ONLY</label>
							  <input type="file" name="userfile" id="userfile" readonly="true" />
							  <p class="help-block"></p>
							</div>
							<button type="submit" value="upload" class="btn btn-block btn-primary btn-lg">Add Client Data</button>
							
                        </div>
						<div class="col-sm-offset-1 col-md-7">
						<h4>Excel formatting details.</h4>
						<?php echo img(array('src' => 'upload/example1.jpg','class'=>'img-responsive')); ?>
						</div>
                </div>			
<br>				
              </div><!-- /.box -->
            </div><!-- /.col-->
          </div><!-- ./row -->
		 </form>
        </section><!-- /.content -->
      </div>