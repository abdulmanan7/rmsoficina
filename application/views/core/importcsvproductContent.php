<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
        <?php 
			if ( !isset($message)){}
			else{ echo "<h3 style=\"color:green; text-align: center;\">".$message."</h3>"; }
	  	?>
          <h1>
            Add Product Data
            <small>Please follow all guidelines.</small>
          </h1>
        </section>
        <!-- Main content -->
        <section class="content">
		<?php echo form_open_multipart('importproddata'); ?>
		    <!-- Basic Information -->
        <div class="row">
            <div class="col-md-12">
              <div class="box box-success">
                <div class="box-header">
                  <h3 class="box-title">Upload Product Excel Sheet</h3>
                  <!-- tools box -->
                  <div class="pull-right box-tools">
                    <button class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                  </div><!-- /. tools -->
                </div><!-- /.box-header -->
                <div class="box-body pad">
                        <div class="col-md-4">
							<div class="form-group">
							  <label for="userfile">XLSX or CSV ONLY</label>
							  <input type="file" name="userfile" id="userfile" readonly="true" />
							  <p class="help-block"></p>
							</div>
							<button type="submit" value="upload" class="btn btn-block btn-primary btn-lg">Add Product Data</button>
							
                        </div>
                </div>
				<hr>
				<div class="box-body pad">
						<div class="col-md-12">
						<h4><strong>Excel formatting details.</strong></h4>
						<p>The headings are in bold and are on the same row. (42 Column Headers)</p>
					
						<?php echo img(array('src' => 'upload/example3.jpg','class'=>'img-responsive')); ?><br>
						</div>
                </div>	
<br>				
              </div><!-- /.box -->
            </div><!-- /.col-->
          </div><!-- ./row -->
		 </form>
        </section><!-- /.content -->
      </div>