﻿<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>SBMSX | Log in</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<link rel="icon" href="<?=base_url()?>/favicon.gif" type="image/gif">
    <!-- Bootstrap 3.3.5 -->
    <?php echo link_tag('assets/bootstrap/css/bootstrap.min.css');?>
    <!-- Font Awesome -->
    <?php echo link_tag('assets/bootstrap/font-awesome/css/font-awesome.min.css');?>
    <!-- Ionicons -->
    <?php echo link_tag('assets/bootstrap/ionicons/css/ionicons.min.css');?>
    <!-- Theme style -->
    <?php echo link_tag('assets/dist/css/AdminLTE.min.css');?>
    <!-- iCheck -->
    <?php echo link_tag('assets/plugins/iCheck/square/blue.css');?>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
<?php if ($this->session->flashdata('msg')) {
	$message = $this->session->flashdata('msg');
}
?>
  <body class="hold-transition login-page">
    <div class="login-box">
      <div class="login-logo">
        <?php echo anchor('index', '<b>SBMSX</b>App');?>
      </div><!-- /.login-logo -->
      <div class="login-box-body">
        <p class="login-box-msg">Log in to view your dashboard</p>
         <?php
if (isset($message)) {echo $message;}
?>

        <?php
$attributes = array('id' => 'techform');
echo form_open('checkuser', $attributes);
?>
          <div class="form-group has-feedback">
            <input style="text-transform: lowercase;" type="text" class="form-control" name="email" value="<?php echo set_value('email');?>"placeholder="Enter Email" >
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="password" class="form-control" name="pastech" value="<?php echo set_value('Password');?>" placeholder="Password" >
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <?php if (isset($security_code) && $security_code == true): ?>
      <div class="row">
      <div class="form-group has-feedback">
        <div class="col-xs-6"><?php echo "<div class='img-responsive'>" . $cap_img . "</div>";?></div>
        <div class="col-xs-6"><input type="text" class="form-control" id="captcha" name="captcha" value="" placeholder="Security Code"/></div>
      </div>
    </div>
          <?php endif?>
          <div class="row">
            <div class="col-xs-8">
              <div class="checkbox icheck">

              </div>
            </div><!-- /.col -->
            <div class="col-xs-4">
			<br>
              <button type="submit" class="btn btn-primary btn-block btn-md btn-flat">Ingresar</button>

            </div><!-- /.col -->




          </div>

          <div class="row">
            <div class="col-xs-8">
              <div class="checkbox icheck">

              </div>
            </div><!-- /.col -->
            <div class="col-xs-4">
      <br>
              <a href="register" class="btn btn-primary btn-block btn-md btn-flat">>>Registro</a>

            </div><!-- /.col -->




          </div>

        </form>

      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->

    <!-- jQuery 2.1.4 -->
    <script src="<?php echo base_url('assets/plugins/jQuery/jQuery-2.1.4.min.js');?>" type="text/javascript"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js');?>" type="text/javascript"></script>
    <!-- iCheck -->
    <script src="<?php echo base_url('assets/plugins/iCheck/icheck.min.js');?>" type="text/javascript"></script>
    <script>
      $(function () {
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });
      });
    </script>
  </body>
</html>