<?php
$title = !isset($title) ? $this->router->fetch_class() : $title;
$sub_title = !isset($sub_title) ? $this->router->fetch_method() : $sub_title;
if (strcmp($_SESSION['ut'], 'Staff') == 0) {
	?>
<!-- Staff Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <?php echo img(array('src' => 'upload/e_kdjhawkdhawjkhd/' . $_SESSION['photo'], 'alt' => 'User Image', 'class' => 'img-circle'));?>
            </div>
            <div class="pull-left info">
              <p><?php echo $_SESSION['fullname']?></p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li>
              <?php echo anchor('index', '<i class="fa fa-dashboard"></i><span>Dashboard</span>', array('class' => ''));?>
            </li>

          <li class="header">MAIN MODULES</li>
          <!-- Products Menu -->
            <li class="treeview">
              <a href="#">
                <i class="fa fa-barcode"></i>
                <span>Product Module</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><?php echo anchor('viewproducts', '<i class="fa fa-square-o"></i><span>View Products</span>', array('class' => ''));?></li>
                <li><?php echo anchor('addproduct', '<i class="fa fa-user-plus"></i><span>Add Product</span>', array('class' => ''));?></li>
                <li><?php echo anchor('barcodeForm', '<i class="fa fa-clipboard"></i><span>View/Create Labels</span>', array('class' => ''));?></li>
                <li><?php echo anchor('barcodeSingleForm', '<i class="fa fa-h-square"></i><span>View/Create Barcodes</span>', array('class' => ''));?></li>
                <li><?php echo anchor('addeditcategory', '<i class="fa fa-list-alt"></i><span>Add/Edit Category</span>', array('class' => ''));?></li>
              </ul>
            </li>
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>
<?php
} else if (strcmp($_SESSION['ut'], 'Product Manager') == 0) {
	?>

<!-- Product Manager Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <?php echo img(array('src' => 'upload/e_kdjhawkdhawjkhd/' . $_SESSION['photo'], 'alt' => 'User Image', 'class' => 'img-circle'));?>
            </div>
            <div class="pull-left info">
              <p><?php echo $_SESSION['fullname']?></p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li>
              <?php echo anchor('index', '<i class="fa fa-dashboard"></i><span>Dashboard</span>', array('class' => ''));?>
            </li>

          <li class="header">MAIN MODULES</li>
          <!-- Products Menu -->
            <li class="treeview">
              <a href="#">
                <i class="fa fa-barcode"></i>
                <span>Product Module</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><?php echo anchor('viewproducts', '<i class="fa fa-square-o"></i><span>View Products</span>', array('class' => ''));?></li>
                <li><?php echo anchor('addproduct', '<i class="fa fa-user-plus"></i><span>Add Product</span>', array('class' => ''));?></li>
                <li><?php echo anchor('barcodeForm', '<i class="fa fa-clipboard"></i><span>View/Create Labels</span>', array('class' => ''));?></li>
                <li><?php echo anchor('barcodeSingleForm', '<i class="fa fa-h-square"></i><span>View/Create Barcodes</span>', array('class' => ''));?></li>
                <li><?php echo anchor('addeditcategory', '<i class="fa fa-list-alt"></i><span>Add/Edit Category</span>', array('class' => ''));?></li>
				<li><?php echo anchor('csvprod', '<i class="fa fa-cloud-upload"></i><span>Import Products</span>', array('class' => ''));?></li>
              </ul>
            </li>
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>
<?php
} else if (strcmp($_SESSION['ut'], 'HR Manager') == 0) {
	?>
<!-- Admin Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <?php echo img(array('src' => 'upload/e_kdjhawkdhawjkhd/' . $_SESSION['photo'], 'alt' => 'User Image', 'class' => 'img-circle'));?>
            </div>
            <div class="pull-left info">
              <p><?php echo $_SESSION['fullname']?></p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li>
              <?php echo anchor('index', '<i class="fa fa-dashboard"></i><span>Dashboard</span>', array('class' => ''));?>
            </li>

          <li class="header">MAIN MODULES</li>

            <li class="header">HR MODULES</li>

            <!-- Employee Menu -->
            <li class="treeview">
              <a href="#">
                <i class="fa fa-puzzle-piece"></i>
                <span>Employee Module</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><?php echo anchor('empList', '<i class="fa fa-square-o"></i><span>View employees</span>', array('class' => ''));?></li>
                <li><?php echo anchor('addEmployee', '<i class="fa fa-user-plus"></i><span>Add employee</span>', array('class' => ''));?></li>
				<li><?php echo anchor('csvemp', '<i class="fa fa-cloud-upload"></i><span>Import employees</span>', array('class' => ''));?></li>
              </ul>
            </li>

            <!-- Client Menu -->
            <li class="treeview">
              <a href="#">
                <i class="fa fa-users"></i>
                <span>Client Module</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><?php echo anchor('viewclients', '<i class="fa fa-square-o"></i><span>View Clients</span>', array('class' => ''));?></li>
                <li><?php echo anchor('addclient', '<i class="fa fa-user-plus"></i><span>Add Client</span>', array('class' => ''));?></li>
				<li><?php echo anchor('importcsvclient', '<i class="fa fa-cloud-upload"></i><span>Import Clients</span>', array('class' => ''));?></li>
              </ul>
            </li>

          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>
<?php
} else {
	?>
<!-- Admin Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <?php echo img(array('src' => 'upload/e_kdjhawkdhawjkhd/' . $_SESSION['photo'], 'alt' => 'User Image', 'class' => 'img-circle'));?>
            </div>
            <div class="pull-left info">
              <p><?php echo $_SESSION['usrnm']?></p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li>
              <?php echo anchor('index', '<i class="fa fa-dashboard"></i><span>Dashboard</span>', array('class' => ''));?>
            </li>

          <li class="header">MODULO ADMINISTRACION</li>
          <!-- Products Menu -->
            <li class="treeview">
              <a href="#">
                <i class="fa fa-barcode"></i>
                <span>Modulo Contrato</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><?php echo anchor('viewproducts', '<i class="fa fa-square-o"></i><span>View Products</span>', array('class' => ''));?></li>
                <li><?php echo anchor('addproduct', '<i class="fa fa-user-plus"></i><span>Add Product</span>', array('class' => ''));?></li>
                <li><?php echo anchor('barcodeForm', '<i class="fa fa-clipboard"></i><span>View/Create Labels</span>', array('class' => ''));?></li>
                <li><?php echo anchor('barcodeSingleForm', '<i class="fa fa-h-square"></i><span>View/Create Barcodes</span>', array('class' => ''));?></li>
                <li><?php echo anchor('addeditcategory', '<i class="fa fa-list-alt"></i><span>Add/Edit Category</span>', array('class' => ''));?></li>
                <li><?php echo anchor('csvprod', '<i class="fa fa-cloud-upload"></i><span>Import Products</span>', array('class' => ''));?></li>
              </ul>
            </li>

            <!-- User menu -->
			 <li  <?=isset($title) ? $title == "CoreUser" ? "class='treeview active'" : "treeview" : "treeview"?>>
              <a href="#">
                <i class="fa fa-user"></i>
                <span>Core users</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li <?=isset($sub_title) ? $sub_title == "viewCoreUser" ? "class='active'" : "" : ""?>><?php echo anchor('viewCoreUsers', '<i class="fa fa-square-o"></i><span>View Users</span>', array('class' => 'avtive'));?></li>
                <li <?=isset($sub_title) ? $sub_title == "adduser" ? "class='active'" : "" : ""?>><?php echo anchor('addCoreUser', '<i class="fa fa-user-plus"></i><span>Add User</span>', array('class' => ''));?></li>
              </ul>
            </li><!-- End User menu -->

            <li class="treeview">
              <a href="#">
                <i class="fa fa-user-secret"></i>
                <span>Informes</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><?php echo anchor('viewUsers', '<i class="fa fa-square-o"></i><span>View Users</span>', array('class' => ''));?></li>
                <li><?php echo anchor('addUser', '<i class="fa fa-user-plus"></i><span>Add User</span>', array('class' => ''));?></li>
              </ul>
            </li><!-- End User menu -->

            <li class="header">AJUSTES</li>
            <!-- Client Menu -->
            <li class="treeview">
              <a href="#">
                <i class="fa fa-building-o"></i>
                <span>Mi Oficina</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><?php echo anchor('viewCompany', '<i class="fa fa-square-o"></i><span>Detalles Oficina</span>', array('class' => ''));?></li>
              </ul>
            </li>

          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>
<?php
}
?>
