﻿<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>SBMSX | Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="icon" href="<?=base_url()?>/favicon.gif" type="image/gif">
  <!-- Bootstrap 3.3.5 -->
  <?php echo link_tag('assets/bootstrap/css/bootstrap.min.css');?>
  <!-- Font Awesome -->
  <?php echo link_tag('assets/bootstrap/font-awesome/css/font-awesome.min.css');?>
  <!-- Ionicons -->
  <?php echo link_tag('assets/bootstrap/ionicons/css/ionicons.min.css');?>
  <!-- Theme style -->
  <?php echo link_tag('assets/dist/css/AdminLTE.min.css');?>
  <!-- iCheck -->
  <?php echo link_tag('assets/plugins/iCheck/square/blue.css');?>
  <style>
    .login-box, .register-box {
      margin: 3% auto;
    }
    .error{
      border: 1px solid red;
    }
    .error-desc{
      color: red;
    }
  </style>
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
      </head>
      <body class="hold-transition login-page">
        <div class="login-box">
          <div class="login-logo">
            <?php echo anchor('index', '<b>SBMSX</b>App');?>
          </div><!-- /.login-logo -->
          <div class="login-box-body">
            <p class="login-box-msg">Register with us</p>
            <?php
if (!isset($message)) {} else {echo $message;}
?>

            <?php
$attributes = array('id' => 'techform');
echo form_open('register', $attributes);
?>
            <div class="form-group has-feedback">
              <input style="text-transform: lowercase;" type="email" class="form-control" name="email" id="email" value="<?php echo set_value('email');?>"placeholder="Email" >
              <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
              <input type="password" class="form-control" name="pastech" id="password" value="<?php echo set_value('pastech');?>" placeholder="Password" >
              <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
              <input type="password" class="form-control" name="cpassword" id="cpassword" value="<?php echo set_value('cpassword');?>" placeholder="confirm password" >
              <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <!-- <div class="form-group has-feedback">
              <input type="text" class="form-control" name="number" id="number" value="<?php echo set_value('number');?>"placeholder="Number" >
              <span class="glyphicon glyphicon-phone-alt form-control-feedback"></span>
            </div> -->
            <!-- <div class="form-group has-feedback">
              <input type="text" class="form-control" name="ut" id="ut" value="<?php echo set_value('ut');?>"placeholder="ut" >
              <span class="glyphicon glyphicon- form-control-feedback"></span>
            </div> -->
            <div class="row">
              <div class="col-xs-4">
               <br>
               <button type="submit" id="btnSave" class="btn btn-primary btn-block btn-md btn-flat">Save</button>

             </div><!-- /.col -->




           </div>

           <div class="row">
            <div class="col-xs-8">
              <div class="checkbox icheck">

              </div>
            </div><!-- /.col -->
          </div>

        </form>

      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->

    <!-- jQuery 2.1.4 -->
    <script src="<?php echo base_url('assets/plugins/jQuery/jQuery-2.1.4.min.js');?>" type="text/javascript"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js');?>" type="text/javascript"></script>
    <!-- iCheck -->
    <script src="<?php echo base_url('assets/plugins/iCheck/icheck.min.js');?>" type="text/javascript"></script>
    <script>
      $(function () {
        $('body').on('click', '#btnSave', function(event) {
          var username  = $('#username');
          var password  = $('#password');
          var cpassword = $('#cpassword');
          var email     = $('#email');
          var number     = $('#number');
          // var ut        = $('#ut');
          var formClear = true;
          if (username.val() =="") {
            username.addClass('error').focus(function(event) {
              $(this).removeClass('error');
              $(this).nextAll('.error-desc').remove();
            });
            formClear = false;
            username.parents('div').append('<span class="error-desc">Nombre required</span>');
          }if(password.val() == ""){
            password.addClass('error').focus(function(event) {
              $(this).removeClass('error');
              $(this).nextAll('.error-desc').remove();
            });
            formClear = false;
            password.parents('div').append('<span class="error-desc">Password is required</span>');
          }if(email.val() == ""){
            email.addClass('error').focus(function(event) {
              $(this).removeClass('error');
              $(this).nextAll('.error-desc').remove();
            });
            formClear = false;
            email.parents('div').append('<span class="error-desc">Email is required</span>');
          }
          // if(ut.val() == ""){
          //   ut.addClass('error').focus(function(event) {
          //     $(this).removeClass('error');
          //     $(this).nextAll('.error-desc').remove();
          //   });
          //   formClear = false;
          //   ut.parents('div').append('<span class="error-desc">Ut is required</span>');
          // }
          // if(number.val() == ""){
          //   number.addClass('error').focus(function(event) {
          //     $(this).removeClass('error');
          //     $(this).nextAll('.error-desc').remove();
          //   });
          //   formClear = false;
          //   number.parents('div').append('<span class="error-desc">Number is required</span>');
          // }
          if(cpassword.val() == ""){
            cpassword.addClass('error').focus(function(event) {
              $(this).removeClass('error');
              $(this).nextAll('.error-desc').remove();
            });
            formClear = false;
            cpassword.parents('div').append('<span class="error-desc">confirm password is required</span>');
          }if(cpassword.val() != password.val()){
            cpassword.addClass('error').focus(function(event) {
              $(this).removeClass('error');
              $(this).nextAll('.error-desc').remove();
            });
            password.addClass('error').focus(function(event) {
              $(this).removeClass('error');
              $(this).nextAll('.error-desc').remove();
            });
            formClear = false;
            cpassword.parents('div').append('<span class="error-desc">password does\'t match</span>');
          }
          if (!formClear) {
            event.preventDefault();
          }else{
            formClear = true;
          }
        });
      });
    </script>
  </body>
  </html>