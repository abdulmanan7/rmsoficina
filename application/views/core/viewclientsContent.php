<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Client List
          </h1>
          <small></small>
        </section>
        <!-- Main content -->
        <section class="content">
        <?php 
		
		$sql = "SELECT * FROM cldata"; 
		$data = $this->db->query($sql);
		if($data->num_rows() == 0)
		{
			echo "<br><div class=\"callout callout-info\">
					 <h4>There are no clients added as yet.</h4>
					 <p>Please add some clients.</p>
				  </div>";
		}	
		else
		{
			if ( !isset($message)){}
			else{ echo "<h3 style=\"color:green; text-align: center;\">".$message."</h3>"; }
	  	?>
        <!-- Basic Information -->
        <div class="row">
            <div class="col-md-12">
               <div class="box">
                <div class="box-body table-responsive">
                  <table id="example1" class="table table-hover">
                  <thead>
                    <tr>
	                  <th><i class="fa fa-file-image-o"></i></th>
                      <th>Name</th>
                      <th>Occupation</th>
                      <th>Company Name</th>
                      <th>Email</th>
                      <th>Work Number 1</th>
                      <th>Work Number 1</th>
                      <th>Options</th>
                    </tr>
                   </thead>
                   <tbody>
                    <?php
					$sql = "SELECT * FROM cldata"; 
					$data = $this->db->query($sql);
					$data = $data->result_array();
					foreach ($data as $field)
					{
					?>
                    <tr>
                      <td><?php
					  		echo img(array('src' => "upload/cl_skeuhdflefjslfjseflksjef/".$field['photo'],'class'=>'profile-user-img img-responsive'));
					  ?></td>
                      <td><?php echo $field['fullname']; ?></td>
                      <td><?php echo $field['occupation']; ?></td>
                      <td><?php echo $field['employer']; ?></td>
                      <td><?php echo $field['email']; ?></td>
                      <td><?php echo $field['worknum']; ?></td>
                      <td><?php echo $field['worknum2']; ?></td>
                      <td>
					  <?php 
					  echo anchor( 'viewclient/'.$field['clId'].'', '<b>View</b>', array( 'class'=>'btn btn-success btn-flat'));?>
					  <?php 
					  echo anchor( 'editclient/'.$field['clId'].'', '<b>Edit</b>', array( 'class'=>'btn btn-info btn-flat'));?><a class="btn btn-danger btn-flat" data-toggle="modal" data-target="#myModal_<?php echo $field['clId']; ?>">Delete</a>
					  <!-- Modal -->
                        <div class="modal fade" id="myModal_<?php echo $field['clId']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                          <div class="modal-dialog" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Deleting a client.</h4>
                              </div>
                              <div class="modal-body">
                                Are you sure you want to delete this client? Once deleted it cannot be undone.
                              </div>
                              <div class="modal-footer">
                               <?php echo anchor( 'deleteclient/'.$field['clId'].'', '<b>Delete</b>', array( 'class'=>'btn btn-danger btn-flat')); ?>
                               <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                              </div>
                            </div>
                          </div>
                        </div><!-- End modal -->
					  </td>
                    </tr>
                    <?php
					}
					?>
                    </tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col-->
          </div><!-- ./row -->
          <?php
			}
          ?>
        </section><!-- /.content -->
      </div>