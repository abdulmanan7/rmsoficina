<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!doctype html>
<html lang="us">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>SBMSX | View Company</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<link rel="icon" href="<?=base_url()?>/favicon.gif" type="image/gif">
        <!-- Bootstrap 3.3.5 -->
    <?php echo link_tag('assets/bootstrap/css/bootstrap.min.css'); ?>
    <!-- Font Awesome -->
    <?php echo link_tag('assets/bootstrap/font-awesome/css/font-awesome.min.css'); ?>
    <!-- Ionicons -->
    <?php echo link_tag('assets/bootstrap/ionicons/css/ionicons.min.css'); ?>
        <!-- iCheck -->
    <?php echo link_tag('assets/plugins/iCheck/flat/blue.css'); ?>
    <!-- Date Picker -->
    <?php echo link_tag('assets/plugins/datepicker/datepicker3.css'); ?>
    <!-- Daterange picker -->
    <?php echo link_tag('assets/plugins/daterangepicker/daterangepicker-bs3.css'); ?>
    <!-- bootstrap wysihtml5 - text editor -->
    <?php echo link_tag('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css'); ?>
	<?php echo link_tag('assets/mycss.css'); ?>
    <?php echo link_tag('assets/plugins/datatables/dataTables.bootstrap.css'); ?>
    <!-- Theme style -->
    <?php echo link_tag('assets/dist/css/AdminLTE.min.css'); ?>
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <?php echo link_tag('assets/dist/css/skins/_all-skins.min.css'); ?>
    <style>
   table {border-collapse:collapse; table-layout:fixed; width:310px;}
   table td {border:solid 1px #fab; width:100px; word-wrap:break-word;}
   </style>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

      <!- Header -->
      <?php
			include("header.php");
	  ?>
      
      <!-- Left side column. contains the logo and sidebar -->
      <?php
			include("menu/leftMenuMain.php");
	  ?>
      <!-- Content Wrapper. Contains page content -->
      <?php
	  	include("viewcompanyContent.php");
	   ?>
      <!-- /.content-wrapper -->
      	  <?php
		include("footer.php");
	  ?>
    </div><!-- ./wrapper -->

    <!-- jQuery 2.1.4 -->
    <script src="<?php echo base_url('assets/plugins/jQuery/jQuery-2.1.4.min.js'); ?>" type="text/javascript"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="<?php echo base_url('assets/plugins/jQueryUI/jquery-ui.min.js'); ?>" type="text/javascript"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
      $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Bootstrap 3.3.5 -->
    <script src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js'); ?>" type="text/javascript"></script>
   	<!-- Data Tables -->
    <script src="<?php echo base_url('assets/plugins/datatables/jquery.dataTables.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/plugins/datatables/dataTables.bootstrap.min.js'); ?>"></script>
    
    <!-- AdminLTE App -->
    <script src="<?php echo base_url('assets/dist/js/app.min.js'); ?>" type="text/javascript"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="<?php echo base_url('assets/dist/js/pages/dashboard.js'); ?>" type="text/javascript"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?php echo base_url('assets/dist/js/demo.js'); ?>" type="text/javascript"></script>
       
    <script>
		$("#example1").DataTable({
        "paging":   true,
        "ordering": true,
        "info":     true,
		"bFilter":	true
    });
    </script>
  </body>
</html>
