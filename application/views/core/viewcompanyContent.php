<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Detalles Oficina
          </h1>
          <small></small>
        </section>
        <!-- Main content -->
        <section class="content">
        <!-- Basic Information -->
        <div class="row">
            <div class="col-md-12">
               
                 <?php
					$sql = "SELECT * FROM coremarca where email = '".$_SESSION['email']."'";
					$data = $this->db->query($sql);
					$comp_data = $data->row_array();
					?>
          <?php if ($comp_data || (isset($comp_data['email']) && strlen($comp_data['email'])>4)): ?>
            
              <!-- Widget: user widget style 1 -->
              <div class="box box-widget widget-user">
                <!-- Add the bg color to the header using any of the bg-* classes -->
                <div class="widget-user-header bg-aqua-active">
                  <h3 class="widget-user-username"><?php echo $comp_data['nombre']; ?></h3>
                  <h5 class="widget-user-desc"><?php echo $comp_data['rut']; ?></h5>
                  <h5 class="widget-user-desc"><?php echo $comp_data['email']; ?></h5>
                  <h5 class="widget-user-desc"><?php echo $comp_data['comuna']; ?></h5>
                </div>
                <div class="widget-user-image">
                  <?php
                  $img_avatar = ($comp_data['foto1'] == "" || $comp_data['foto1'] == NULL)?"blank.png":$comp_data['foto1'];
					  		echo img(array('src' => "upload/comp_awhdlawhdal/".$img_avatar,'class'=>'img-circle img-responsive'));
					  ?>
                </div>
                <div class="box-footer">
                  <div class="row">
                    <div class="col-sm-4 border-right">
                      <div class="description-block">
                        <h5 class="description-header">Dirección</h5>
                        <span class="description-text"><?php echo $comp_data['dirline1']; ?></span><br>
                        <span class="description-text"><?php echo $comp_data['dirline2']; ?></span><br><br>
                        
                        <h5 class="description-header">Ciudad</h5>
                        <span class="description-text"><?php echo $comp_data['ciudad']; ?></span><br><br>
                        
                      </div><!-- /.description-block -->
                    </div><!-- /.col -->
                    <div class="col-sm-4 border-right">
                      <div class="description-block">
                        <h5 class="description-header">Contact</h5>
                        <span class="description-text"><i class="fa fa-mobile"></i> <?php echo $comp_data['fonocelular']; ?></span><br>
                      	<span class="description-text"><i class="fa fa-home"></i> <?php echo $comp_data['fonooficina1']; ?></span><br>
                      	<span class="description-text"><i class="fa fa-suitcase"></i> <?php echo $comp_data['fonooficina2']; ?></span><br>
                      </div><!-- /.description-block -->
                    </div><!-- /.col -->
                    <div class="col-sm-4">
                      <div class="description-block">
                        <h5 class="description-header">Facebook</h5>
                        <span class="description-text"><i class="fa fa-facebook"></i> <?php echo $comp_data['facebook']; ?></span><br><br>
                        
                         <h5 class="description-header">Twitter</h5>
                        <span class="description-text"><i class="fa fa-twitter"></i> <?php echo $comp_data['twitter']; ?></span><br>
                        <h5 class="description-header">LinkedIn</h5>
                        <span class="description-text"><i class="fa fa-linkedin"></i> <?php echo $comp_data['linkedin']; ?></span><br>
                      </div><!-- /.description-block -->
                    </div><!-- /.col -->
                  </div><!-- /.row -->
                </div>
              </div><!-- /.widget-user -->
              <?= anchor( 'editcomp/'.$comp_data['id'], '<b>Editar</b>', array( 'class'=>'btn btn-info btn-flat'));?>
            </div><!-- /.col-->
          </div><!-- ./row -->
<?php else: ?>
  <div class="alert alert-info">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <strong>No Record found !</strong> please click add new to create company info....
  </div>
    <?= anchor( 'addCompany', '<b>Add New</b>', array( 'class'=>'btn btn-info btn-flat'));?>
<?php endif;?>

        </section><!-- /.content -->
      </div>