<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Product Inventory
            <small>Below are all the products in your inventory.</small>
          </h1>
          <small>&nbsp;Note: You can click on the top of the list to sort by the column name or you can search in the search box for an item.</small>
        </section>
        <!-- Main content -->
        <section class="content">
        <?php 
		$sql = "SELECT * FROM pdata"; 
		$data = $this->db->query($sql);
		if($data->num_rows() == 0)
		{
			echo "<br><div class=\"callout callout-info\">
					 <h4>There are no products added as yet.</h4>
					 <p>Please add some products.</p>
				  </div>";
		}	
		else
		{
			if ( !isset($message)){}
			else{ echo "<h3 style=\"color:green; text-align: center;\">".$message."</h3>"; }
	  	?>
        <!-- Basic Information -->
        <div class="row">
            <div class="col-md-12">
               <div class="box">
                <div class="box-body table-responsive">
                  <table id="example1" class="table table-hover">
                  <thead>
                    <tr>
	                  <th><i class="fa fa-file-image-o"></i></th>
                      <th>SKU</th>
                      <th>Product Name</th>
                      <th>Price</th>
                      <th>Sale Price</th>
                      <th>Stock Status</th>
                      <th>Quantity</th>
                      <th>Main Category</th>
                      <th>Options</th>
                    </tr>
                   </thead>
                   <tbody>
                    <?php
					$sql = "SELECT * FROM pdata"; 
					$data = $this->db->query($sql);
					$data = $data->result_array();
					foreach ($data as $field)
					{
					?>
                    <tr>
                      <td><?php
					  			if($field['file_name1'] != NULL) echo img(array('src' => "upload/p_dfgdfgdfgjkhd/".$field['file_name1'],'class'=>'profile-user-img img-responsive'));
					  			else if($field['file_name2'] != NULL) echo img(array('src' => "upload/p_dfgdfgdfgjkhd/".$field['file_name2'],'class'=>'profile-user-img img-responsive')); 
					  			else if($field['file_name3'] != NULL) echo img(array('src' => "upload/p_dfgdfgdfgjkhd/".$field['file_name3'],'class'=>'profile-user-img img-responsive')); 
								else echo img(array('src' => 'assets/images/blank.png','class'=>'profile-user-img img-responsive'));
					  ?></td>
                      <td><?php echo $field['sku']; ?></td>
                      <td><?php echo $field['prodname']; ?></td>
                      <td><?php echo $field['currency'].$field['price']; ?></td>
                      <td><?php echo $field['saleprice']; ?></td>
                      <td><?php 
					  		if(strcmp($field['stock'], "In Stock") ==0 ) echo '<span class="label label-success">'.$field['stock'].'</span>'; 
					  		else if(strcmp($field['stock'], "Out of Stock") ==0 ) echo '<span class="label label-danger">'.$field['stock'].'</span>'; 
					  ?></td>
                      <td><?php echo $field['stkqty']; ?></td>
                      <td><?php echo $field['cat']; ?></td>
                      <td>
					  <?php 
					  echo anchor( 'viewspecificprod/'.$field['prodId'].'', 'View', array( 'class'=>'btn btn-success btn-flat'));?><?php 
					  echo anchor( 'editprod/'.$field['prodId'].'', 'Edit', array( 'class'=>'btn btn-info btn-flat'));?><a class="btn btn-danger btn-flat" data-toggle="modal" data-target="#myModal_<?php echo $field['prodId']; ?>">Delete</a>
					  <!-- Modal -->
                        <div class="modal fade" id="myModal_<?php echo $field['prodId']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                          <div class="modal-dialog" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Deleting a product.</h4>
                              </div>
                              <div class="modal-body">
                                Are you sure you want to delete this product? Once deleted it cannot be undone.
                              </div>
                              <div class="modal-footer">
                               <?php echo anchor( 'deleteprod/'.$field['prodId'].'', '<b>Delete</b>', array( 'class'=>'btn btn-danger btn-flat')); ?>
                               <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                              </div>
                            </div>
                          </div>
                        </div><!-- End modal -->
					  </td>
                    </tr>
                    <?php
					}
					?>
                    </tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col-->
          </div><!-- ./row -->
          <?php
			}
		  ?>
        </section><!-- /.content -->
      </div>