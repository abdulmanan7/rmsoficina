      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Product Full Details
          </h1>
        </section>
		<?php
			$sql = "SELECT * FROM cldata Where clId=".$id; 
			$data = $this->db->query($sql);
		
       	 	$data = $data->result_array(); //create an array
		
			foreach ($data as $field)
		{?>
        <!-- Main content -->
        <section class="content">

          <div class="row">
            <div class="col-md-3">

<!-- Profile Image -->
              <div class="box box-primary">
                <div class="box-body box-profile">
                  
                  <?php 
				  echo img(array('src' => "upload/cl_skeuhdflefjslfjseflksjef/".$field['photo'],'class'=>'profile-user-img img-responsive'));
				?>
                  <h3 class="profile-username text-center"><?php echo $field['fullname']; ?></h3>
                  <p class="text-muted text-center"><?php echo $field['occupation']; ?></p>

                  <ul class="list-group list-group-unbordered">
                    <li class="list-group-item">
                      <b>Company</b> <a class="pull-right"><?php echo $field['employer']; ?></a>
                    </li>
                    <li class="list-group-item">
                      <b>Email</b> <a class="pull-right"><?php echo $field['email']; ?></a>
                    </li>
                    <li class="list-group-item">
                      <b>Work Number 2</b> <a class="pull-right"><?php echo $field['worknum']; ?></a>
                    </li>
                    <li class="list-group-item">
                      <b>Work Number 2</b> <a class="pull-right"><?php echo $field['worknum2']; ?></a>
                    </li>
                  </ul>
                </div><!-- /.box-body -->
              </div><!-- /.box -->

            </div><!-- /.col -->
            <div class="col-md-9">
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#Details" data-toggle="tab"><i class="fa fa-file-text-o"></i> Details</a></li>
                  <li><a href="#options" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
                </ul>
                <div class="tab-content">
                  <div class="active tab-pane" id="Details">              
              <div class="box box-warning">
                <div class="box-header">
                  <h3 class="box-title">Client Description</h3>
                  <!-- tools box -->
                  <div class="pull-right box-tools">
                    <button class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-plus"></i></button>
                  </div><!-- /. tools -->
                </div><!-- /.box-header -->
                	<div class="box-body pad">
                        <div class="col-md-5">
                        	<div>
                            <?php echo $field['editorEmp']; ?>
                            </div>                               
                        </div>
                    </div>
                  </div><!-- /.box -->
                  </div><!-- /.tab-pane -->
                  
              
             <div class="tab-pane" id="options">
             <?php 
			 echo anchor( 'editclient/'.$id.'', '<b>Edit Product</b>', array( 'class'=>'btn btn-info btn-flat'));?>
             <a class="btn btn-danger btn-flat" data-toggle="modal" data-target="#myModal_<?php echo $id; ?>">Delete Client</a>
					  <!-- Modal -->
                        <div class="modal fade" id="myModal_<?php echo $id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                          <div class="modal-dialog" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Deleting a client.</h4>
                              </div>
                              <div class="modal-body">
                                Are you sure you want to delete this client? Once deleted it cannot be undone.
                              </div>
                              <div class="modal-footer">
                               <?php echo anchor( 'deleteclient/'.$id.'', '<b>Delete</b>', array( 'class'=>'btn btn-danger btn-flat')); ?>
                               <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                              </div>
                            </div>
                          </div>
                        </div><!-- End modal -->
             </div><!-- /.tab-pane -->
                </div><!-- /.tab-content -->
              </div><!-- /.nav-tabs-custom -->
            </div><!-- /.col -->
          </div><!-- /.row -->

        </section><!-- /.content -->
         <?php
		}
		?>
      </div><!-- /.content-wrapper -->