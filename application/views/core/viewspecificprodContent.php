      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Product Full Details
          </h1>
        </section>
		<?php
			$sql = "SELECT * FROM pdata Where prodId=".$id; 
			$data = $this->db->query($sql);
		
       	 	$data = $data->result_array(); //create an array
		
			foreach ($data as $field)
		{?>
        <!-- Main content -->
        <section class="content">

          <div class="row">
            <div class="col-md-3">

<!-- Profile Image -->
              <div class="box box-primary">
                <div class="box-body box-profile">
                  
                  <?php 
				  if($field['file_name1'] == NULL && $field['file_name2'] == NULL && $field['file_name3'] == NULL)
				  	echo img(array('src' => 'assets/images/blank.png','class'=>'profile-user-img img-responsive'));
                  else
				  {
                  	if($field['file_name1'] != NULL)
						echo img(array('src' => "upload/p_dfgdfgdfgjkhd/".$field['file_name1'],'class'=>'profile-user-img img-responsive')); 
					else if($field['file_name2'] != NULL)
						echo img(array('src' => "upload/p_dfgdfgdfgjkhd/".$field['file_name2'],'class'=>'profile-user-img img-responsive'));
					else if( $field['file_name3'] != NULL)
						echo img(array('src' => "upload/p_dfgdfgdfgjkhd/".$field['file_name3'],'class'=>'profile-user-img img-responsive'));
				  }
				?>
                  <h3 class="profile-username text-center"><?php echo $field['prodname']; ?></h3>
                  <p class="text-muted text-center"><?php echo $field['sku']; ?></p>

                  <ul class="list-group list-group-unbordered">
                    <li class="list-group-item">
                      <b>Price</b> <a class="pull-right"><?php echo $field['currency'].''.$field['price']; ?></a>
                    </li>
                    <li class="list-group-item">
                      <b>Sale</b> <a class="pull-right"><?php echo $field['currency'].''.$field['saleprice']; ?></a>
                    </li>
                    <li class="list-group-item">
                      <b>Stock</b> <a class="pull-right"><?php echo $field['stock']; ?></a>
                    </li>
                  </ul>
                </div><!-- /.box-body -->
              </div><!-- /.box -->

            </div><!-- /.col -->
            <div class="col-md-9">
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#Details" data-toggle="tab"><i class="fa fa-file-text-o"></i> Details</a></li>
                  <li><a href="#options" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
                </ul>
                <div class="tab-content">
                  <div class="active tab-pane" id="Details">
			<div class="box box-info">
                <div class="box-header">
                  <h3 class="box-title">Basic Product Information</h3>
                  <!-- tools box -->
                  <div class="pull-right box-tools">
                    <button class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-plus"></i></button>
                  </div><!-- /. tools -->
                </div><!-- /.box-header -->
                <div class="box-body pad">
                        <div class="col-md-10">
                                <div>
                                  <?php echo '<span class="label label-info">Product Name:</span> '.$field['prodname']; ?>
                                  <?php echo '<br><span class="label label-info">SKU:</span> '.$field['sku']; ?>
                                  <?php echo '<br><span class="label label-info">Stock Status:</span> '.$field['stock']; ?>
                                  <?php echo '<br><span class="label label-info">Price:</span> '.$field['currency'].$field['price']; ?>
                                  <?php echo '<br><span class="label label-info">Stock Quantity:</span> '.$field['stkqty']; ?>
                                  <?php echo '<br><span class="label label-info">Category:</span> '.$field['cat'].' > '.$field['sub1'].' > '.$field['sub2'].' > '.$field['sub3'].' > '.$field['sub4']; ?>
                                </div>
                        </div>
                </div>
              </div><!-- /.box -->
              
              <div class="box box-warning">
                <div class="box-header">
                  <h3 class="box-title">Product Description</h3>
                  <!-- tools box -->
                  <div class="pull-right box-tools">
                    <button class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-plus"></i></button>
                  </div><!-- /. tools -->
                </div><!-- /.box-header -->
                	<div class="box-body pad">
                        <div class="col-md-5">
                        	<div>
                            <?php echo $field['editorEmp']; ?>
                            </div>                               
                        </div>
                    </div>
                  </div><!-- /.box -->
              <div class="box box-alert">
                <div class="box-header">
                  <h3 class="box-title">Images</h3>
                  <!-- tools box -->
                  <div class="pull-right box-tools">
                    <button class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-plus"></i></button>
                  </div><!-- /. tools -->
                </div><!-- /.box-header -->
                <div class="box-body pad">
                <div class='row margin-bottom'>
                        <div class='col-sm-4'>
                          <?php 
				  if($field['file_name1'] == NULL && $field['file_name2'] == NULL && $field['file_name3'] == NULL)
				  	echo img(array('src' => 'assets/images/blank.png','class'=>'profile-user-img img-responsive'));
                  else
				  {
                  	if($field['file_name1'] != NULL)
						echo img(array('src' => "upload/p_dfgdfgdfgjkhd/".$field['file_name1'],'class'=>'img-responsive'));
				  }
				?>
                        </div><!-- /.col -->
                        <div class='col-sm-4'>
                          <?php 
					if($field['file_name2'] != NULL)
						echo img(array('src' => "upload/p_dfgdfgdfgjkhd/".$field['file_name2'],'class'=>'img-responsive'));
				?>
                        </div><!-- /.col -->
                        <div class='col-sm-4'>
                          <?php 
					if($field['file_name3'] != NULL)
						echo img(array('src' => "upload/p_dfgdfgdfgjkhd/".$field['file_name3'],'class'=>'img-responsive'));
				?>
                        </div><!-- /.col -->
                      </div><!-- /.row -->
                </div>
              </div><!-- /.box -->
                  </div><!-- /.tab-pane -->
                  
              
             <div class="tab-pane" id="options">
             <?php 
			 echo anchor( 'editprod/'.$id.'', '<b>Edit Product</b>', array( 'class'=>'btn btn-info btn-flat'));?>
             <a class="btn btn-danger btn-flat" data-toggle="modal" data-target="#myModal_<?php echo $id; ?>">Delete Product</a>
					  <!-- Modal -->
                        <div class="modal fade" id="myModal_<?php echo $id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                          <div class="modal-dialog" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Deleting a product.</h4>
                              </div>
                              <div class="modal-body">
                                Are you sure you want to delete this product? Once deleted it cannot be undone.
                              </div>
                              <div class="modal-footer">
                               <?php echo anchor( 'deleteprod/'.$id.'', '<b>Delete</b>', array( 'class'=>'btn btn-danger btn-flat')); ?>
                               <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                              </div>
                            </div>
                          </div>
                        </div><!-- End modal -->
             </div><!-- /.tab-pane -->
                </div><!-- /.tab-content -->
              </div><!-- /.nav-tabs-custom -->
            </div><!-- /.col -->
          </div><!-- /.row -->

        </section><!-- /.content -->
         <?php
		}
		?>
      </div><!-- /.content-wrapper -->