<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Users
            <small>Below are users who can access the application.</small>
          </h1>
          <small>&nbsp;Note: You must leave one <b>Administrator</b> (other than the Super Admin) user active.</small>
        </section>
        <!-- Main content -->
        <section class="content">
        <?php 
			if ( !isset($message)){}
			else{ echo "<h3 style=\"color:green; text-align: center;\">".$message."</h3>"; }
	  	?>
        <!-- Basic Information -->
        <div class="row">
            <div class="col-md-12">
               <div class="box">
                <div class="box-body table-responsive no-padding">
                  <table class="table table-hover">
                  
                    <tr>
                      <th>Fullname</th>
                      <th>User Type</th>
                      <th>Email</th>
                      <th>Username</th>
                      <th>Option</th>
                    </tr>
                    <?php
					$sql = "SELECT * FROM appuser"; 
					$data = $this->db->query($sql);
					$data = $data->result_array();
					foreach ($data as $field)
					{
					?>
                    <tr>
                      <td><?php echo $field['fullname']; ?></td>
                      <td><?php 
					  		if(strcmp($field['ut'], "Administrator") ==0 ) echo '<span class="label label-success">'.$field['ut'].'</span>'; 
					  		else if(strcmp($field['ut'], "Staff") ==0 ) echo '<span class="label label-danger">'.$field['ut'].'</span>';
							else if(strcmp($field['ut'], "Product Manager") ==0 ) echo '<span class="label bg-teal">'.$field['ut'].'</span>';
							else if(strcmp($field['ut'], "HR Manager") ==0 ) echo '<span class="label bg-orange">'.$field['ut'].'</span>';
							else echo '<span class="label label-maroon">'.$field['ut'].'</span>';
							
					  ?></td>
                      <td><?php echo $field['email']; ?></td>
                      <td><?php echo $field['usrnm']; ?></td>
                      <td><?php
					  echo anchor( 'edituser/'.$field['userid'].'', '<b>Edit</b>', array( 'class'=>'btn btn-info btn-flat'));?>
                      <?php
					  if(strcmp($field['usrnm'],'superadmin') != 0)
					  {
					  ?>
                      <a class="btn btn-danger btn-flat" data-toggle="modal" data-target="#myModal_<?php echo $field['userid']; ?>">Delete</a>
					  <!-- Modal -->
                        <div class="modal fade" id="myModal_<?php echo $field['userid']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                          <div class="modal-dialog" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Deleting a user.</h4>
                              </div>
                              <div class="modal-body">
                                Are you sure you want to delete this user? Once deleted it cannot be undone.
                              </div>
                              <div class="modal-footer">
                               <?php echo anchor( 'deleteuser/'.$field['userid'].'', '<b>Delete</b>', array( 'class'=>'btn btn-danger btn-flat')); ?>
                               <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                              </div>
                            </div>
                          </div>
                        </div><!-- End modal -->
                        <?php
						}
						?>
					  </td>
                    </tr>
                    <?php
					}
					?>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col-->
          </div><!-- ./row -->
        </section><!-- /.content -->
      </div>