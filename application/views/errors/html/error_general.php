<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Error</title>
<style type="text/css">

::selection { background-color: #E13300; color: white; }
::-moz-selection { background-color: #E13300; color: white; }

body {
	background-color: #d2d6de;
	margin-top: 80px;
	font: 13px/20px normal Helvetica, Arial, sans-serif;
	color: #4F5155;
}

h1 {
	color: red;
	background-color: transparent;
	font-size: 19px;
	font-weight: normal;
	margin: 0 0 14px 0;
}

#container{
		text-align: center;
}

ul{ list-style: none }
</style>
</head>
<body>
	<div id="container">
		<h1>You cannot do that! Go back to login page.</h1>
		<?php echo $message; ?>
		<?php echo '<p>For your safety you have been logged out of the application'; ?>
		<?php echo '<br><p>Possible errors:<ul><li>- You resubmitted form data twice.</li><li>- You have entered data directly into the url. Please do not do that.</li><li>- Security detected a possible break-in attempt.</li></ul><p>'; ?>
		<?php 	if(isset($_SERVER['HTTP_HOST']))
					//unset($_SERVER['HTTP_HOST']);   
		?>
		<?php $link = 'http://'.$_SERVER['HTTP_HOST'].'/sbms/'; ?>
		<a href="<?php echo $link; ?>">BACK TO LOGIN PAGE</a>
	</div>
</body>
</html>