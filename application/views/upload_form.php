<html>
<head>
<script type = "text/javascript" src = "//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.1/jquery.js"></script>
<script type = "text/javascript" src = "//cdnjs.cloudflare.com/ajax/libs/jquery.form/3.51/jquery.form.js"></script>
<title>Upload Form</title>
<style>
    ul {
    font-size: 0 !important; /* remove physical spaces between items */
    text-align: justify;
    text-justify: distribute-all-lines; /* distribute items in IE */
    list-style-type: none;
    margin: 0;
    padding: 0;
}

/* fully justify all items in browsers other than IE */
ul:after {
    content: "";
    display: inline-block;
    width: 100%;
}

ul li {
    text-align: left; /* customize to suit */
    vertical-align: top; /* can customize to suit */
    display: inline-block;
    margin-bottom: 1em; /* optional, customize to suit */
    margin-left: 4px; /* optional, customize to suit */
}
    .avatar{
        width: 75px;
    }
</style>
</head>
<body>
    <!-- AJAX Response will be outputted on this DIV container -->
    <div class = "upload-image-messages"></div>

    <div class = "col-md-6">
        <!-- Generate the form using form helper function: form_open_multipart(); -->
        <?php echo form_open_multipart('upload/do_upload', array('class' => 'upload-image-form'));?>
            <input type="file" multiple = "multiple" class = "form-control" onchange="do_upload();" name="uploadfile[]" size="20" /><br />
            <input type="submit" name = "submit" value="Upload" class = "btn btn-primary" />
        </form>

        <script>
        jQuery(document).ready(function($) {
            var options = {
                beforeSend: function(){
                    // Replace this with your loading gif image
                    $(".upload-image-messages").html('<p><img src = "<?php echo base_url()?>images/loading.gif" class = "loader" /></p>');
                },
                complete: function(response){
                    // Output AJAX response to the div container
                    console.log(response);
                    $(".upload-image-messages").html($.parseHTML(response.responseText));
                    $('html, body').animate({scrollTop: $(".upload-image-messages").offset().top-100}, 150);

                }
            };
            // Submit the form
            $(".upload-image-form").ajaxForm(options);

            return false;

        });
        function do_upload(){
            // setTimeout(function() {
            // $('.upload-image-form').submit();
            // }, 1000);
            return ;
        }
        </script>
    </div>
</body>
</html>